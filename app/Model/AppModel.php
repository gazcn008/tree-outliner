<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');
App::uses('TrUtils', 'Lib');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

	public function l($msg, $data = null, $type = LOG_DEBUG, $scope = NULL) {
//		if ($data === null) {
//			$data = &$msg;
//			unset($msg);
//			$msg = null;
//		}
//		if ($msg != null) {
//			$this->log(array('msg' => &$msg, 'data' => &$data), $type, $scope);
//		} else {
//			$this->log($data, $type, 'bla');
//		}
	}

	public function le($msg, $data = null, $scope = NULL) {
		$this->l($msg, $data, LOG_ERR, $scope);
	}

}
