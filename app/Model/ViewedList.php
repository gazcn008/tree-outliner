<?php
class ViewedList extends AppModel {
	function saveStatus($listId, $userId, $itemStatus) {
		$statusRec = $this->find('first', array(
				'conditions' => array (
						'user_id' => $userId,
						'list_id' => $listId 
				) 
		) );
		
		$statusRec['ViewedList']['list_id'] = $listId;
		$statusRec['ViewedList']['user_id'] = $userId;
		$statusRec['ViewedList']['item_status'] = $itemStatus;
		
		$this->save($statusRec);
	}
}