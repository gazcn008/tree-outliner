<?php

/**
 * @property Item $Item
 * @property ItemList $ItemList
 * @property ViewedList $ViewedList
 * @property User $User
 * @property IndexedItem $IndexedItem
 * @property Tag $Tag
 * @property ItemTag $ItemTag
 * @property MyObject $MyObject
 *
 * @author lzheng
 *
 */
class AjaxController extends AppController {
	public $uses = array('Item', 'ItemList', 'IndexedItem', 'ViewedList', 'User', 'MyObject');
	var $user;

	public function ajax($flag, $data = array()) {
		$this->set('flag', $flag);
		$this->set('data', $data);
		$this->render('ajaxreturn', '');
	}

    public function need_auth() {
        $this->ajax('need-auth');
    }

    public function object_sync() {
        $req = json_decode($this->request->data['ops'], true);
        $inUse = $this->request->data['use'];
        $tokens = $this->request->data['tokens'];
        if (!$tokens) $tokens = array();

        $ids = array();

        foreach($req as $op) {
            $ids[] = $op['id'];
        }

        $records = $this->MyObject->getObjects($ids);

        foreach($req as $op){
            $rec = $records[$op['id']];
            if ($op['method'] == 'get') {
                if ($rec) {
                    if ($tokens[$rec['_token_']]) $rec['_owned_'] = true;
                    unset($rec['_token_']);
                    $result[] = array(
                        'id' => $op['id'],
                        'raw' => $rec,
                    );
                } else {
                    $result[] = array(
                        'id' => $op['id'],
                        'raw' => false,
                    );
                }
            } else {
                if ($rec && ($rec['readonly'] || !$rec['_public_edit_'] && !$tokens[$rec['_token_']])) {
                    $result[] = array(
                        'id' => $op['id'],
                        'raw' => false,
                    );
                } else {
                    if (!$rec) $op['raw']['_token_'] = $inUse;
                    else unset($op['raw']['_token_']);

                    $this->MyObject->saveObject($op['raw']);
                    $result[] = array('id' => $op['id'], 'raw' => $op['raw']);
                }

            }
        }
        $this->ajax('ok', $result);
    }

    function search_object() {
        $req = json_decode($this->request->data['req'], true);

        $rsp = $this->MyObject->search($req['name'], $req['space'], $req['offset'], $req['step'], $req['tokens']);

        $this->ajax('ok', $rsp);
    }

    function beforeFilter() {
        $action = $this->action;

        if ($action != 'need_auth' && $action != 'object_sync' && $action != 'search_object') {
            $this->user = $user = $this->Session->read('user');
            if (!$user || !$user['id']) {
                $this->redirect('need_auth');
            }
        }
    }

    var $CON_randRangeKey = 'rand_range_info';

    public function get_review_items () {
        $req = json_decode($this->request->data['req'], true);
        $user = $this->user;

        $this->l($user);
        $time = $this->_datetime();

        $randRangeInfo = $this->Session->read($this->CON_randRangeKey);
        $res = $this->Item->getReviewItems(10, $user['id'], $req['tags'], array(
            'now' => $req['now'],
            'randRangeInfo' => $randRangeInfo,
        ));

        $this->Session->write($this->CON_randRangeKey, $res['randRangeInfo']);


        $this->ajax('ok', array('items' => &$res['items'],
            'arounds' => &$res['arounds'],
            'count' => &$res['count'],
            'lists' => &$res['lists']));
    }

    public function review() {
        $itemId = $this->request->data['itemId'];
        $nextReview = $this->request->data['nextReview'];
        $reviewType = $this->request->data['reviewType'];
        $now = $this->request->data['now'];
        $isNewReview = $this->request->data['isNewReview'];

        $this->Item->review($itemId, $reviewType, $nextReview, $this->user['id'], $now, $isNewReview != 'false');
        $this->ajax('ok');
    }

    public function get_indexed_items() {
        $req = json_decode($this->request->data['req'], true);
        $user = $this->user;

        $res = $this->Item->getIndexedItems(20, $user['id']);

        $this->ajax('ok', $res);
    }

    public function index_item() {
        $itemId = $this->request->data['itemId'];
        $now = $this->request->data['now'];
        $removed = $this->request->data['removed'];

        $this->Item->indexItems(array(
                array(
                    'id' => $itemId,
                    'removed' => $removed == 'true',
                )
            ),
            $this->user['id'],
            $this->_getRangeInfo(),
			$now);

        $this->ajax('ok');
    }

    public function _getRangeInfo() {
        $range = $this->Session->read($this->CON_randRangeKey);
        if (!$range) {
            $this->Item->getRandRangeInfo($this->user['id']);
            $this->Session->write($this->CON_randRangeKey, $range);
        }
        return $range;
    }

    function _datetime() {
        return date('Y-m-d H:i:s');
    }

	public function hello() {
		$this->ajax('ok', $this->request->data);
	}

	public function remove_list() {
		$listId = $this->request->data['listId'];
		$list = $this->ItemList->findById($listId);

		if ($list['ItemList']['owner_id'] == $this->user['id']) {
			$this->ItemList->removeList($listId);
			$this->ajax('ok');
		}
		else {
			$this->ajax('fail');
		}
	}

    public function checkPermission(&$list, &$user) {

        /*
         * Share types : null, 'public', 'read-public'
         * permission types : 'write', 'read', 'none'
         */
        if ($list['share_type'] == 'public' || $list['list_type'] == 'tutorial'
        	|| $list['owner_id'] == $user['id']) {
            // this is a public list, the user can ... do whatever he wants
            return 'write';
        } else if ($list['share_type'] == 'readonly') {
			return 'read';
		} else {
			return 'none';
		}
    }

    public function recent_removed() {
    	$req = $this->request->data;

    	$removed = $this->ItemList->getRecentRemoved(
    		$req['listId'], $req['lastTimeAnchor']
		);

    	$this->ajax('ok', $removed);
    }

    public function recent_updated() {
		$req = $this->request->data;

		$removed = $this->ItemList->getRecentUpdated(
			$req['listId'], $req['lastTimeAnchor']
		);

		$this->ajax('ok', $removed);
	}

	public function revert_removed() {
		$req = $this->request->data;

		$this->ItemList->revertRemoved($req['listId'], $req['removedAt']);
		$this->ItemList->dirtyList($req['listId'], $this->_time());

		$this->ajax('ok');
	}

	public function item_status() {
		$data = $this->request->data;

		$itemRec = $this->Item->findById($data['itemId']);

		$this->ajax('ok', $itemRec['Item']);
	}

	public function sync() {

		$timestamp = $this->_time();
		$req = json_decode($this->request->data['req'], true);
		$user = $this->Session->read('user');

		$listDirty = false;
		$hasConflict = false;

		// check the current status of list updates before update the current update
		$list = $this->ItemList->getListInfo($req['listId']);

		$permission = $this->checkPermission($list, $user);
        if ($list['list_type'] == 'tutorial' || $permission == 'none') {
            $this->ajax('ok', array('synced' => $timestamp, 'patch' => null));
            return;
        }

		if ($req['itemStatus']) {
			$this->ViewedList->saveStatus($req['listId'], $user['id'], $req['itemStatus']);
		}

        // get new dicts
        $newDicts = array();
        if ($req['newDicts']) {
            foreach ($req['newDicts'] as $listId) {
                $newDicts[$listId] = $this->ItemList->getList($listId, $this->user['id'], true);
            }
        }

        $dicts = array();
        if ($req['dicts']) {
            foreach ($req['dicts'] as $listId) {
                $dicts[$listId] = $this->Item->loadUpdatePatch($req['lastSynced'],
                    $listId, false, false, true);
            }
        }

		if ($req['patch'] && $permission != 'read') {
			if ($list['synced'] <= $req['lastSynced'] || $list['synced'] > $timestamp) {
				$this->Item->syncAt($req['patch'], $timestamp,
						$req['listId'], $req['userHash'], $req['today'],
                    array(
                        'userId' => $user['id'],
                        'randRangeInfo' => $this->Session->read($this->CON_randRangeKey),
                        'now' => $req['now'],
                    ));
				$listDirty = true;
			} else {
				$hasConflict = true;
			}
		}

		if ($list['synced'] > $req['lastSynced'] && $list['synced'] < $timestamp) {
			$patch = $this->Item->loadUpdatePatch($req['lastSynced'],
                $req['listId'], $req['userHash']);
		}

		// write to list synced flag
		if ($listDirty) {
			$this->ItemList->dirtyList($req['listId'], $timestamp);
			$updated = date('Y-m-d H:i:s');
		}
		else {
			$updated = '';
		}

		if ($permission == 'read') {
			$flag = 'readonly';
		}
		else if ($hasConflict) {
			$flag = 'conflict';
		}
		else {
			$flag = 'ok';
		}

		$this->ajax($flag, array(
			'synced' => $timestamp,
			'patch' => &$patch,
            'newDicts' => $newDicts,
            'dicts' => $dicts,
            'updated' => $updated
		));
	}

    public function complete_quest() {
        $questId = $this->request->data['questId'];
        $user = $this->Session->read('user');
        $userRec = $this->User->findById($user['id']);

        $quests = explode('|', $userRec['User']['tutorial_progress']);
        if ($user['id'] && !in_array($questId, $quests)) {
            $quests[] = $questId;
            $questsStr = implode('|', $quests);
            $this->User->save(array(
                'User' => array(
                    'id' => $user['id'],
                    'tutorial_progress' => $questsStr,
                )
            ));

            $user['quests'] = $questsStr;
            $this->Session->write('user', $user);
        }

		$log = $this->User->getDataSource()->getLog(false, false);
		$this->l('sql for list', $log);

        $this->ajax('ok');
    }

    function tutorial() {
        $list = $this->ItemList->find('first', array(
            'conditions' => array('list_type' => 'tutorial')
        ));

        $user = $this->Session->read('user');
        if (!$user['quests']) {
            $user['quests'] = 'visited';
            $this->Session->write('user', $user);
        }

        $this->ajax('ok', $list['ItemList']['id']);
    }

    public function move_items() {
        $req = json_decode($this->request->data['req'], true);
        $time = $this->_time();

        $sourceList = $this->ItemList->getListInfo($req['listId']);

        if ($sourceList['list_type'] == 'tutorial') {
            $this->ajax('ok', array(
                'synced' => $time,
                'patch' => array()
            ));
            return;
        }

        $this->ItemList->moveItems($req['listId'], $time, $req['children'], $req['items'], array(
            'parentItem' => $req['parentItem'],
        ));

        $patch = $this->Item->loadUpdatePatch($req['lastSynced'],
            $req['listId'], null);

        $this->ajax('ok', array(
            'synced' => $time,
            'patch' => &$patch
        ));
    }

    public function add_back_link() {
        $listId = $this->request->data['listId'];
        $backLinkId = $this->request->data['backLink'];
        $backLinkTitle = $this->request->data['backLinkTitle'];
        $backLinkText = $this->request->data['backLinkText'];
        $time = $this->_time();

        $this->Item->addBackLink($listId, $backLinkId, $backLinkTitle, $time, $backLinkText);

        $this->ItemList->dirtyList($listId, $time);

        $this->ajax('ok');
    }

    public function upload_files() {
    	$files = $_FILES;
    	$fileNames = array();

		$folder = WWW_ROOT . '/uploads';
		if (!file_exists($folder)) {
			mkdir($folder, 0755, true);
		}

		foreach ($files as $file) {
			if ($file['error'] === UPLOAD_ERR_OK) {
				$info = pathinfo($file['name']);
				$uuidWithExtname = uniqid(substr(MD5(microtime().uniqid()), 0, 5)) . '.' . $info['extension'];
				$fileNames[] = array('name' => $file['name'], 'url' => "/uploads/$uuidWithExtname");
				move_uploaded_file($file['tmp_name'], "$folder/$uuidWithExtname");
			}
		}

//		foreach ($files['error'] as $id => $err) {
//			if ($err == UPLOAD_ERR_OK) {
//				$fn = $files['name'][$id];
//
////				move_uploaded_file(
////					$files['tmp_name'][$id],
////					'uploads/' . $fn
////				);
//				$fileNames[] = $files['name'];
//			}
//		}

		$this->ajax('ok', $fileNames);
    }

    public function create_list() {
        $user = $this->Session->read('user');
        $children = $this->request->data['children'];
        $items = $this->request->data['items'];
        $time = $this->_time();
        $title = $this->request->data['title'];
        $backLinkId = $this->request->data['listId'];
        $backLinkTitle = $this->request->data['listTitle'];


        // check the current status of list updates before update the current update
        $sourceList = $this->ItemList->getListInfo($backLinkId);

        $list = $this->ItemList->createList($user['id'], $time, $title);
        $itemId = $this->Item->addBackLink($list['id'], $backLinkId, $backLinkTitle, $time);

        if ($sourceList['list_type'] == 'tutorial') {
            $this->ajax('ok', array(
                'listId' => $list['id']
            ));
            return;
        }

        $this->ItemList->moveItems($list['id'], $time, $children, $items, array(
            'parentItem' => $itemId,
        ));

        $this->ajax('ok', array(
            listId => $list['id']
        ));
    }

	public function search_list() {
		$term = $this->request->data['term'];
		$user = $this->user;

		$result = $this->ItemList->search($term, $user['id']);
		$this->ajax('ok', $result);
	}

	public function _time() {
		return floor(microtime(true) * 1000);
	}

	public function saveListTitle() {
		$id = $this->request->data['listId'];
		$title = $this->request->data['title'];

		$rec = $this->ItemList->findById($id);
//		$this->l('save list title: get by id', $rec);
		if ($rec) {
			$rec['ItemList']['title'] = $title;
			$this->ItemList->save($rec);
		}
		$this->ajax('ok');
	}

	public function saveImage() {
		$imageData = $this->request->data['image'];
		$type = $this->request->data['type'];
		if (!preg_match('/^\w+$/', $type)) {
			$this->ajax('invalidType');
			return;
		}

		$imageData = substr($imageData, strpos($imageData, ','));

		$imageName = uniqid() . substr(MD5(microtime().uniqid()), 0, 5) . ".$type";
//		$this->l('image path', App::path('webroot'));
		$folder = WWW_ROOT . '/uploads';
		if (!file_exists($folder)) {
			mkdir($folder, 0755, true);
		}
		file_put_contents("$folder/$imageName", base64_decode($imageData));
		$this->ajax('ok', "/uploads/$imageName");
	}

	public function checkListUpdate() {
		$user = $this->user;

		$id = $this->request->data['list'];
		$updated = $this->request->data['updated'];

		$listInfo = $this->ItemList->getListInfo($id);

		if ($id != -1) {
//		print_r($listInfo);
//		print_r($updated);
			if ($listInfo['updated'] <= $updated && !!$updated) {
				$this->ajax('notChanged');
			}
			else {
				$this->ajax('changed', array('updated' => $listInfo['updated']));
			}
		}
		else {
			$this->ajax('fail');
		}
	}

	public function favDoc() {
		$user = $this->user;

		$listId = $this->request->data['list'];
		$isFav = $this->request->data['isFav'];

		$this->ItemList->favDoc($user['id'], $listId, $isFav);

		$this->ajax('ok');
	}

	public function getList() {
		$user = $this->user;
        $time = $this->_time();

		$id = $this->request->query['list'];

		$this->response->cache('-1 minutes', '+1 years');

		if ($id != -1) {
            // check the current status of list updates before update the current update
            $listInfo = $this->ItemList->getListInfo($id);
            $permission = $this->checkPermission($listInfo, $user);

            if ($permission == 'none') {
                $this->ajax('noPermission', array('updated' => $listInfo['updated']));
                return;
            }

			$list = $this->ItemList->getList($id, $user['id']);
		} else {
			$list = $this->ItemList->createList($user['id'], $time);
		}

        $username = $user['user'];
        $list['username'] = $username;

		$maxId = $this->Item->maxId();

		$this->ajax('ok', array(
			'list' => &$list,
			'synced' => $time,
			'maxId' => $maxId,
			'userSeq' => intval($user['id']),
		));
	}

    public function set_permission() {
        $listId = $this->request->data['listId'];
        $shareType = $this->request->data['shareType'];

        $listInfo = $this->ItemList->getListInfo($listId);
        if ($listInfo['owner_id'] == $this->user['id']) {
            $listInfo['share_type'] = $shareType;
            unset($listInfo['updated']);

            $this->ItemList->save($listInfo);
            $this->ajax('ok');
        } else {
            $this->ajax('fail');
        }
    }

    // reminder module : indexed items
    public function searchIndexedItems() {
           $user = $this->user;
           $term = $this->request->data['term'];

            $time = $this->_datetime();

            $randRangeInfo = $this->Session->read($this->CON_randRangeKey);
            $res = $this->Item->searchIndexedItems($user['id'], $term);

            $this->ajax('ok', $res);
		}

	public function setIndexedItem() {
		$user = $this->user;
		$data = $this->request->data;

		$this->Item->setIndexedItem($user['id'], $data);

		$this->ajax('ok');
	}

	public function checkUpdate() {
		$now = date('Y-m-d H:i:s', strtotime('-15 minutes', time()));
		$userId = $this->user['id'];

		$clientNow = $this->request->data['now'];

		$rangeStart = $clientNow - 60 * 60 * 1000;
		$rangeEnd = $clientNow + 30 * 60 * 1000;

		$this->ItemList->contain('ViewedList');
		$list = $this->ItemList->find('all', array(
			'field' => array('ItemList.title'),
			'conditions' => array(
				'AND' => array(
					"ItemList.removed is null",
					"ItemList.updated > '$now'",
					"ViewedList.user_id = $userId",
				)
			),
			'limit' => 11
		));

		$items = $this->Item->find('all', array(
			'field' => array('Item.content'),
			'conditions' => array(
				'AND' => array(
					"IndexedItem.next_review > $rangeStart",
					"IndexedItem.next_review < $rangeEnd",
					"IndexedItem.user_id = $userId",
				)
			),
			'limit' => 11
		));

		$this->ajax('ok', array(
			'lists' => array_map(function ($list) {
				return array($list['ItemList']['title']);
			}, $list),
			'items' => array_map(function ($item) {
				return array($item['Item']['content']);
			}, $items)
		));
	}
}
