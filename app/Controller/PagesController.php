<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 *
 * @property User $User
 */
class PagesController extends AppController {
	public $helpers = array (
			'Html',
			'App'
	);

	public $uses = array(
		'User', 'ObjectConfig'
	);

	private $pv = array ();
	public function beforeRender() {
		$this->pv['appVersion'] = Configure::read('app-version');
		$this->set('appVersion', Configure::read('app-version'));
		$this->set ( 'pv', $this->pv );
	}

	private static $CON = array(
		'cssFocusPage' => 'aui-layout aui-theme-default aui-page-focused aui-page-focused-small',
	);

	private $user = null;

	function beforeFilter() {
		$action = $this->action;

		$this->user = $user = $this->Session->read('user');
        $this->pv['quests'] = $user['quests'];
		if (!$user && !in_array($action, array(
				'login', 'logout', 'register', 'popup', 'objects'
			))) {
				$this->Session->write('url-before-login', $this->request->here);
				$this->redirect(array('action' => 'login'));
			}

	}


    var $CON_objectSeqKey = 'object_seq';
    var $CON_maxSeq = 2100000000;
    function objects() {
        $res = $this->ObjectConfig->findById(1);
        if (!$res['ObjectConfig']) {
            $conf = array('seq' => 0);
        } else {
            $conf = json_decode($res['ObjectConfig']['config'], true);
        }
        if ($conf['seq'] > $this->CON_maxSeq) {$conf['seq'] = 1;}
        else $conf['seq'] ++;

        $hash = base_convert($conf['seq'], 10, 36);
        $this->pv['seq'] = "{$hash}";
        $this->pv['hi'] = $this->CON_maxSeq;
        $this->pv['lo'] = $conf['seq'] > $this->CON_maxSeq;
        $this->pv['bl'] = $res;

        $this->ObjectConfig->save(array(
            'ObjectConfig' => array(
                'id' => 1,
                'config' => json_encode($conf),
            )
        ));
        $this->render('objects', '');
    }

    function googleCallback() {

    }

	function login() {
		$this->set('css', self::$CON['cssFocusPage']);
		if ($this->Session->read('reg-success')) {
			$this->Session->delete('reg-success');
			$this->pv['success'][] = array('message' => 'New user has been registered successfully.');
		}

		if ($this->request->is('post')) {
			$user = $this->request->data['user'];
			$pass = $this->request->data['pass'];

			$result = $this->User->find('first', array(
				'conditions' => array(
				'username' => $user,
				'pass' => $pass
			)));

			$this->pv['user'] = $user;
			$this->pv['pass'] = $pass;

			if (empty($result['User']) || empty($result['User']['pass'])) {
				$this->pv['errors'][] = array('message' => 'User cannot be found or password is incorrect.');
				$this->Session->delete('user');
			} else {
				$this->Session->write('user', array(
						'id' => $result['User']['id'],
						'user' => $user,
                    'quests' => $result['User']['tutorial_progress']
				));
			}

            $this->_previousUrl();
		} else {
            $userInfo = $this->Session->read('user');
            if ($userInfo) {
                $this->_previousUrl();
                return;
            }

            $gmail = $this->Session->read('google-email');
            $this->Session->delete('google-email');
            if ($gmail) {
                // if the user has logged in
                $result = $this->User->find('first', array(
                    'conditions' => array(
                        'username' => $gmail,
                    )));
                if (empty($result)) {
                    $this->User->save(array(
                        'User' => array(
                            'username' => $gmail
                        )
                    ));
                    $id = $this->User->id;
                } else {
                    $id = $result['User']['id'];
                }
                $this->Session->write('user', array(
                    'id' => $id,
                    'user' => $gmail,
                    'quests' => $result['User']['tutorial_progress']
                ));
                $this->_previousUrl();
            } else {

				/*
                $client = new Google_Client();
                $client->setClientId(CLIENT_ID);
                $client->setClientSecret(ClIENT_SECRET);
                $client->setRedirectUri(REDIRECT_URI);
                $client->setScopes('email');

                // if the request is from OAuth callback
                $code = $this->request->query('code');
                if ($code) {
                    $client->authenticate($code);
                    if ($client->getAccessToken()) {
                        $_SESSION['access_token'] = $client->getAccessToken();
                        $token_data = $client->verifyIdToken()->getAttributes();
                        $email = $token_data['payload']['email'];
                        if ($email) {
                            $this->Session->write('google-email', $email);
                        }
                    }
                    $this->redirect('/login');
                } else {
                    $this->set(authUrl, $client->createAuthUrl());
                }
                */
            }
        }
	}

    function main() {

    }

    function review() {

        $styles = array(
            'banner-1.jpg' => array(
                'main' => '#ffc033',
                'mainText' => '#fff',
                'second' => '#FFCC1E',
                'secondText' => '#fff',
            ),

            'banner-blue.jpg' => array(
                'main' => '#1e5eb6',
                'mainText' => '#fff',
                'second' => '#207fda',
                'secondText' => '#fff',
            ),

            'banner-black.jpg' => array(
                'main' => '#5a5a5a',
                'mainText' => '#fff',
                'second' => '#7c7c7c',
                'secondText' => '#fff',
            ),

            'banner-colorful.jpg' => array(
                'main' => '#111',
                'mainText' => '#fff',
                'second' => '#333',
                'secondText' => '#fff',
            ),

        );
        $bannersImages = array_keys($styles);

        $this->set('title_for_layout', 'Item review stream');
        $this->set('bannerImage', $image = $bannersImages[rand(0, count($bannersImages) - 1)]);

        $colors = $styles[$image];
        $extraStyles = "
            #page .settings {background-color: ${colors['main']}; color:  ${colors['mainText']}}
            #page footer {background-color: ${colors['main']}; color:  ${colors['mainText']}}
            #page footer .button {background-color: ${colors['main']}; color:  ${colors['mainText']}}
            #page footer .expanded {background-color: ${colors['second']}; color:  ${colors['secondText']}}
            #page footer .expanded .button {background-color: ${colors['second']}; color:  ${colors['secondText']}}
            ";

        $this->set('extraStyles', $extraStyles);

        if (substr($this->request->here, 0, 7) != '/review') {
            $this->redirect('/review');
        }
    }

    function _previousUrl() {
        $previousUrl = $this->Session->read('url-before-login');
        if ($previousUrl) {
            $this->Session->delete('url-before-login');
            $this->redirect($previousUrl);
        } else {
            $this->redirect('/app');
        }
    }

	function logout() {
		$this->Session->delete('user');
		$this->redirect("/login");
	}

    function qcon_survey() {
        $this->render('qcon_survey', '');
    }

    function qcon_submit() {
        $this->l($this->request->data);
        $this->redirect('qcon_survey');
    }

	function register() {
		$this->set('css', self::$CON['cssFocusPage']);
		if ($this->request->is('post')) {
			$user = $this->request->data['reg-user'];
			$pass = $this->request->data['reg-pass'];
			$confirm = $this->request->data['reg-confirm'];

			if (!preg_match('/^\w+$/', $user)) {
				$this->pv['errors'][] = array('message' => 'Username should be numeric only.');
			} else if (!$pass)  {
				$this->pv['errors'][] = array('message' => 'Password should not be empty.');
			} else if ($pass != $confirm) {
				$this->pv['errors'][] = array('message' => 'Password and password confirm do not match.');
			} else {
				$result = $this->User->find ( 'first', array (
						'conditions' => array (
								'username' => $user
						)
				) );

				if ($result) {
					$this->pv['errors'][] = array('message' => 'Username are already registered by someone.');
				} else {
					$this->User->save ( array(
							'User' => array (
									'username' => $user,
									'pass' => $pass
							)
					) );
					$this->Session->write('reg-success', true);
					$this->redirect("/login");
				}
			}

		}
	}

	function register_success() {

	}

	function app() {
		if (substr($this->request->here, 0, 4) != '/app') {
			$this->redirect('/app' . $this->request->here);
		}
	}

	function popup() {
		$this->set('isPopup', true);
	}
}
