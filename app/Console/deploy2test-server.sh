app=~/public_ftp
target=~/public_html/app

backups=$app/backups
deployDir=$app/incoming/deploy
deployZip=$app/incoming/deploy.zip
uploads=$target/webroot/uploads

if [ -f $deployZip ]
then
	echo "deploy file found, start to deploy"
	pushd "$app/incoming"
	
	echo "remove deploy dir if exists"
	rm -rf $deployDir
	
	echo "unzip deploy zip to deploy dir"
	unzip $deployZip > deploy.log

    zip "$backups/`date +"%Y_%m_%d_%H_%M"`_uploads_before.zip" $app/uploads/*
    rm -rf $app/uploads
	mv $uploads $app/uploads
    zip "$backups/`date +"%Y_%m_%d_%H_%M"`_uploads_after.zip" $app/uploads/*

	echo "removing *"
	rm -rf $target
	
	echo "moving deploy folder to target app folder"
	mv $deployDir $target
	
	echo "chmod - all file"
	chmod -R 755 $target
	
	mkdir $target/tmp
	mkdir $target/tmp/cache
	mkdir $target/tmp/cache/persistent
	mkdir $target/tmp/cache/models
	mkdir $target/tmp/logs
	chmod -R 777 $target/tmp

	mkdir $target/webroot/uploads
	
	mv $deployZip "$backups/`date +"%Y_%m_%d_%H_%M"`_deploy.zip"
	rm -rf $uploads
	mv $app/uploads $uploads
	
else
	echo "deploy file not found"
fi 