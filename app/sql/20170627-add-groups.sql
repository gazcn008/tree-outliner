--
-- 现有Table `tr_users` 扩展：
-- 个人信息 添加 nickname, email, descr
--
ALTER TABLE `tr_users`
  ADD `nickname` varchar(120) DEFAULT NULL,
  ADD `email` varchar(45) DEFAULT NULL,
  ADD `descr` varchar(200) DEFAULT NULL;

--
-- 新增Table组群 `tr_groups`
--
CREATE TABLE `tr_groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(120) DEFAULT NULL,
  `group_descr` varchar(200) DEFAULT NULL,
  `created_by` int(11) NOT NULL,  
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `removed` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- `created_by` is referencing `id` in the `tr_users` table
--

ALTER TABLE `tr_groups`
  ADD FOREIGN KEY (`created_by`) REFERENCES `tr_users` (`id`);

--
-- Indexes for table `tr_groups`
--
ALTER TABLE `tr_groups`
  ADD PRIMARY KEY (`id`);

--
-- 新增Table组群-个人 `tr_groups_to_users`
--

CREATE TABLE `tr_groups_to_users` (
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_admin` boolean DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 添加foreign keys
--
ALTER TABLE `tr_groups_to_users`
  ADD FOREIGN KEY (`user_id`) REFERENCES `tr_users` (`id`);

--
-- 新增Table组群-列表 `tr_groups_to_item_lists`
--

CREATE TABLE `tr_groups_to_item_lists` (
  `group_id` int(11) NOT NULL,
  `list_id` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- 添加foreign keys
--

ALTER TABLE `tr_groups_to_item_lists`
  ADD FOREIGN KEY (`group_id`) REFERENCES `tr_groups` (`id`),
  ADD FOREIGN KEY (`list_id`) REFERENCES `tr_item_lists` (`id`);


