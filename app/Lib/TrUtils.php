<?php


class TrUtils {
	static function pick(&$source, $keys, $defauls = array()) {
		$target = array();
        foreach ($keys as $t => $s) {
            if (is_int($t)) {
                $t = $s;
            }
            if (isset($source[$s])) {
                $target[$t] = $source[$s];
            } else if (isset($defaults[$t])) {
                $target[$t] = $defaults[$t];
            }
        }
		return $target;
	}

    static function transfer(&$source, &$target, $keys, $isForce = false, $defaults = array()) {
        foreach ($keys as $t => $s) {
            if (is_int($t)) {
                $t = $s;
            }
            if (isset($source[$s])) {
                if (isset($target[$t]) && !$isForce) {
                    throw new Exception("The target key '$t' already exists!");
                }
                $target[$t] = $source[$s];
            } else if (isset($defaults[$t])) {
                $target[$t] = $defaults[$t];
            }
        }
    }
}