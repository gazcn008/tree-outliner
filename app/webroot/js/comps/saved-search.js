define(['backbone'], function (Backbone) {

	var seq = 1;

	// __m30: __m32: class: saved search
	var SavedSearch = Backbone.View.extend({
		events: {
			'click .saved-search-title > span': 'refresh',
			'click .sort': 'sort',
			'click .remove': 'remove',
			'click .top': 'top',
			'delay-refresh': 'delayRefresh'
		},
		className: 'saved-search',
		initialize: function (opts) {
			var t = this;
			_.extend(t, opts);

			t.term = t.search.term;
			t.sortType = 'time';
			t.searchId = 'saved-search-' + seq++;
			t.delayRefresh = _.delayFn(t.searchId + '-refresh', function () {
				t.refresh();
			}, 50);
		},

		top: function () {
			var t = this;
			t.g.list.$searchBoxWrapper.after(this.$el);
			t.g.list.removeSearch(this.search);
			t.g.list.status.searches.push(this.search);
		},

		sort: function () {
			var t = this;

			t.sortType = t.sortType === 'time' ? 'order' : 'time';
			t.refresh();
		},

		refresh: function () {
			var t = this;
			var count = t.g.list.searchItems(t.term, {
				$savedSearch: t.$el,
				sortType: t.sortType,
				skipHover: true
			});

			t.$('.saved-search-title > span').text(
				(count > 0 ? '(' + count + ') ' : '')
				+ t.search.term
			);

			t.$('.saved-search-title > .sort').html(
				t.sortType === 'order' ? '时间排序' : '位置排序'
			)
		},

		remove: function () {
			this.$el.remove();
			this.g.list.removeSearch(this.search);
			this.g = null;
			_.cancelDelayFn(this.searchId);
		},

		render: function () {
			var t = this;
			var html = t.g.tpls.savedSearch({
				term: t.term,
				sortType: t.sortType
			});
			t.$el.html(html);

			t.g.setupSelectableList(null, t.$('.result-div ul'), t.$('.result-div'), 20);

			return t.$el;
		}
	});

	return SavedSearch;
});
