define(['ajax', 'jquery', 'utils', 'lodash', 'text!tpls/common.tpl',
		'backbone', 'mousetrap', 'keymap', 'noty', 'vex-loader',
		'vex', 'help-content', 'md5', 'item',
		'item-list', 'syncer', 'key-manager', 'header-dropdown', 'aui-dropdown2',
		'spinner', 'window-messenger', 'item-processor', 'marked', 'pickmeup',
		'moment'],
	function (ajax, $, utils, _, commonTplStr,
			  Backbone, Mousetrap, keymap, noty, dialog,
			  vex, helpContent, md5, Item,
			  ItemList, Syncer, KeyManager, HeaderDropdown, _dropdown2,
			  _spinner, messenger, itemProcessor, marked, pickmeup,
			  moment,
			  _undefined) {

		if (!('Notification' in window)) {
			return null;
		}

		var CHECK_KEY = 'notification_check';
		var UPDATE_INTERVAL = 10 * 60 * 1000;

		window.onunload = function () {
			notificationUtils.hide();
		}

		var notificationUtils = {
			notification: null,

			hide: function () {
				var t = this;

				if (t.notification) {
					var prevNotification = t.notification;
					prevNotification.onshow = function () {
						prevNotification.close();
					};
					prevNotification.close();
					t.notification = null;
				}
			},

			initMenuItem: function ($menuLink) {
				var t = this;
				var check = utils.getItem(CHECK_KEY);

				$menuLink.parent().show();
				var isOpen = check.nextCheck - new Date().getTime() <= UPDATE_INTERVAL;

				t.refreshMenuItem(isOpen, $menuLink);
				$menuLink.on('click', function () {
					utils.setItem(CHECK_KEY, {
						nextCheck: new Date().getTime() + (isOpen ? 24 * 60 * 60 * 1000 : 0)
					});

					isOpen = !isOpen;

					t.refreshMenuItem(isOpen, $menuLink);
				});
			},

			refreshMenuItem: function (isOpen, $menuLink) {
				if (isOpen) {
					$menuLink.html('暂时关闭提醒 (1 天)');
				}
				else {
					$menuLink.html('打开提醒 (每 15 分钟)');
				}
			},

			notify: function (title, opts, fadeInSecond) {
				fadeInSecond = fadeInSecond || 12;

				var t = this;
				if (Notification.permission === 'denied') {
					// 还是得偶尔提醒用户打开提示功能
					if (Math.random() < 0.08) {
						alert('提醒功能被禁用, 如果你想获取消息的主动提醒功能, 请打开本网站的消息提示功能权限 ;)');
					}
					return;
				}

				var showNotification = function () {
					if (title) {
						t.hide();
						var currNotification = t.notification = new Notification(title, opts);
						currNotification.onclick = function () {
							window.focus();
							t.hide();
						};

						if (fadeInSecond > 0) {
							setTimeout(function () {
								if (currNotification === t.notification) {
									currNotification.close();
								}
							}, fadeInSecond * 1000);
						}
					}
				};

				if (Notification.permission !== 'granted') {
					Notification.requestPermission(function () {
						showNotification();
					});
				} else {
					showNotification();
				}
			},

			check: function () {
				var checkInfo = utils.getItem(CHECK_KEY, {});

				if (!checkInfo.nextCheck || checkInfo.nextCheck < new Date().getTime()) {
					checkInfo.nextCheck = new Date().getTime() + UPDATE_INTERVAL;
					utils.setItem(CHECK_KEY, checkInfo);

					ajax.post('checkUpdate', {
						now: new Date().getTime()
					}, {
						ok: function (result) {
							var message = '';
							var listLen = result.lists.length;
							var itemLen = result.items.length;

							if (listLen) {
								var listMessage = (listLen > 10 ? '10+' : listLen) + ' 个列表有更新 : ' +
									result.lists.slice(0, 5)
										.map(function(text) { return text.slice(0, 20).split(/\s+/g, ' ');})
										.join(', ');

								message = listMessage.slice(0, 100) + (listMessage.length > 100 ? '...' : '');
							}


							if (itemLen) {
								var itemMessage = (itemLen > 10 ? '10+' : itemLen) + ' 个提醒需要关注 : ' +
									result.items.slice(0, 5)
										.map(function(text) { return text.slice(0, 20).split(/\s+/g, ' ');})
										.join('\n');

								message += (message ? '\n\n' : '')
									+ itemMessage.slice(0, 100) + (itemMessage.length > 100 ? '...' : '');
							}

							if (listLen || itemLen) {
								notificationUtils.notify('树状文档有更新 (每 10 分钟)', {
									body: message
								})
							}
						}
					});
				}
			}
		};

		// utils.setItem(CHECK_KEY, {nextCheck: 0});
		setInterval(
			notificationUtils.check,
			UPDATE_INTERVAL
		);

		return notificationUtils;
	});
