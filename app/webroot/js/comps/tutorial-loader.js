define(['ajax', 'jquery', 'utils', 'lodash', 'text!tpls/common.tpl',
		'backbone', 'mousetrap', 'keymap', 'noty', 'vex-loader',
		'vex', 'help-content', 'md5', 'item',
		'item-list', 'syncer', 'key-manager', 'header-dropdown', 'aui-dropdown2',
		'spinner', 'window-messenger', 'item-processor', 'marked', 'tutorial'],
	function (ajax, $, utils, _, commonTplStr,
			  Backbone, Mousetrap, keymap, noty, dialog,
			  vex, helpContent, md5, Item,
			  ItemList, Syncer, KeyManager, HeaderDropdown, _dropdown2,
			  _spinner, messenger, itemProcessor, marked, tutorial,
			  _undefined) {

		return {
			play: function (g) {
				var root = itemProcessor.getSmartLlist();
				console.log(root);

				tutorial.init(g);

				if (!root.context.transcript) {
					alert('这个文档不是一个可播放的文档, 请设置 #transcript= ');
					return;
				}

				var getUrl = function (node) {
					var url = '';
					if (node.children[0]) {
						var content = node.children[0].content;
						var match = /\((.*?)\)/.exec(content);
						if (match) {
							url = match[1];
							return url;
						}
					}
					console.error(node, '找不到相应的 url');
					return '/img/file-not-found'
				};

				var hasToken = function (node, fullKey) {
					return node.tokens.some(function (token) {
						return token.fullKey === fullKey
					});
				};

				var avatars = {};

				root.context.transcript.children.forEach(function (step) {
					switch (step.tokens[0] && step.tokens[0].fullKey) {
						case 'scene':
							tutorial.scene(getUrl(step));
							break;
						case 'play-music':
							tutorial.playMusic(getUrl(step));
							break;
						case 'avatar':
							var name = step.tokens[1].fullKey;
							avatars[name] = tutorial.avatar(getUrl(step), hasToken(step, 'avatar.left'), name);
							break;
						// case 'mute':
						// 	tutorial.mute();
						// 	break;
						case 'stop-music':
							tutorial.stopMusic();
							break;
						case 'image':
							tutorial.image(
								getUrl(step),
								step.tokens[1] && step.tokens[1].value ? ~~step.tokens[1].value : _undefined
							);
							break;
						case 'hide-image':
							tutorial.hideImage();
							break;
						default:
							if (!step.tokens[0]) {
								break;
							}

							var firstToken = step.tokens[0].fullKey;
							if (avatars.hasOwnProperty(firstToken)) {
								var image = null;
								if (step.image) {
									avatars[firstToken].image(getUrl(step.image), step.image.tokens[0] && step.image.tokens[0].value);
								}

								avatars[firstToken].say(
									marked(
										step.tokens.filter(function (token) {
											return token.type === 'text'
										}).map(function (token) {
											return token.value;
										}).join('\n')
									),
									hasToken(step, 'avatar.left') ? true
										: hasToken(step, 'avatar.right') ? false : _undefined,
									image
								);
							}
							break;
					}
				});
			}
		};
	});
