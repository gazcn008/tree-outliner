define(['ajax', 'jquery', 'utils', 'lodash', 'text!tpls/common.tpl',
		'backbone', 'mousetrap', 'keymap', 'noty', 'vex-loader',
		'vex', 'help-content', 'md5', 'item',
		'item-list', 'syncer', 'key-manager', 'header-dropdown', 'aui-dropdown2',
		'spinner', 'window-messenger', 'item-processor', 'marked'],
	function (ajax, $, utils, _, commonTplStr,
			  Backbone, Mousetrap, keymap, noty, dialog,
			  vex, helpContent, md5, Item,
			  ItemList, Syncer, KeyManager, HeaderDropdown, _dropdown2,
			  _spinner, messenger, itemProcessor, marked,
			  _undefined) {

		var Markdown = Backbone.View.extend({
			events: {},

			initialize: function (opts) {
				var t = this;
				_.extend(t, opts);

				t.initEvents();

				t.$raw = t.$el.find('.raw > textarea');
				t.$render = t.$el.find('.render');

				$('#sidebar, #content').hide();

				t.refresh();

				t.$el.attr('data-view', 'render');
			},

			initEvents: function () {
				var t = this;

				t.$el.on('click', '.toggle-raw', function () {
					var view = t.$el.attr('data-view');
					t.$el.attr('data-view', view === 'render' ? 'raw' : 'render');
				})
			},

			refresh: function () {
				var t = this;

				var list = itemProcessor.getFlattenList();

				var markdownRaw = list.join('\n\n');
				var markdownHtml = marked(markdownRaw);

				t.$raw.val(markdownRaw);
				t.$render.html(markdownHtml);
			}
		});

		return Markdown;
	});
