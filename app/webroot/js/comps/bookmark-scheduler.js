define(['ajax', 'jquery', 'utils', 'lodash', 'text!tpls/common.tpl',
		'backbone', 'mousetrap', 'keymap', 'noty', 'vex-loader',
		'vex', 'help-content', 'md5', 'item',
		'item-list', 'syncer', 'key-manager', 'header-dropdown', 'aui-dropdown2',
		'spinner', 'window-messenger', 'item-processor', 'marked', 'pickmeup',
		'moment'],
	function (ajax, $, utils, _, commonTplStr,
			  Backbone, Mousetrap, keymap, noty, dialog,
			  vex, helpContent, md5, Item,
			  ItemList, Syncer, KeyManager, HeaderDropdown, _dropdown2,
			  _spinner, messenger, itemProcessor, marked, pickmeup,
			  moment,
			  _undefined) {


		pickmeup.defaults.locales.cn = {
			days: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
			daysShort: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
			daysMin: ['日', '一', '二', '三', '四', '五', '六'],
			months: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
			monthsShort: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月']
		};

		var Scheduler = Backbone.View.extend({
			events: {},

			initialize: function (opts) {
				var t = this;
				_.extend(t, opts);

				t.$title = t.$('.item-content');

				t.$nextReview = t.$('.next-review .time');

				t.calendarElem = t.$('.calendar').get(0);
				t.calendar = pickmeup(t.calendarElem, {
					flat: true,
					locale: 'cn'
				});

				t.initEvents();
			},

			initEvents: function () {
				var t = this;

				t.calendarElem
					.addEventListener('pickmeup-change', function (e) {
						t.item.next_review = +moment(e.detail.date.getTime()).hour(8);
						t.item.review_type = '';

						t.refresh();

						t.touchNextReview();
					});

				t.$el
					.on('click', '[data-interval]', function () {
						var $t = $(this);
						var interval = $t.attr('data-interval');

						var m = /([\d.]+)(\w)/.exec(interval);

						var newNextReview = moment().add(+m[1], m[2] === 'n' ? 'M' : m[2]);
						if (/[dwny]/.exec(m[2])) {
							newNextReview.hour(8).minute(0);
						}

						t.item.review_type = interval;
						t.item.next_review = +newNextReview;

						t.refresh();
						t.touchNextReview();
					})
					.on('click', '[data-hour]', function () {
						var $t = $(this);
						var hour = ~~$t.attr('data-hour');

						var minute = t.nextReview.format('mm').slice(0, 1);
						t.item.next_review = +t.nextReview.hour(hour).minute(minute + '0');

						t.refresh();
						t.touchNextReview();
					})
					.on('click', '[data-minute]', function () {
						var $t = $(this);
						var minute = ~~$t.attr('data-minute');

						t.item.next_review = +t.nextReview.minute(minute);

						t.refresh();
						t.touchNextReview();
					})
					.on('click', '[data-type]', function () {
						var $t = $(this);
						var type = $t.attr('data-type');

						t.item.time_type = type;

						t.dirtyItem();
						t.refresh();
					});
			},

			dirtyItem: function () {
				var t = this;
				t.item.dirty = true;

				t.onDirty && t.onDirty()
			},

			touchNextReview: function () {
				var t = this;

				t.item.touched = true;
				t.dirtyItem();

				if (t.$trigger) {
					t.$trigger.trigger('touched');
				}

				if (window.top.onItemTouched) {
					window.top.onItemTouched({
						itemId: t.item.id,
						item: t.item
					});
				}
			},

			trySaveItem: function () {
				var t = this;
				var item = t.item;

				if (item && !item.saving && item.dirty) {
					if (!item.next_review) {
						item.next_review = new Date().getTime();
					}

					if (item.time_type && item.touched) {
						var delta = +item.next_review - new Date().getTime();
						item.next_review = +item.next_review - delta / 4 * Math.random();
					}

					item.saving = true;

					var requestData = _.pick(_.extend({}, item), [
						'next_review', 'id', '',
						'time_type', 'review_type'
					]);
					requestData.now = new Date().getTime();
					requestData.review_times = item.review_times ? +item.review_times + 1 : 1;

					ajax.post('setIndexedItem', requestData,
						{
							ok: function () {
								item.dirty = false;
							},
							last: function () {
								item.saving = false;
							}
						}
					);
				}
			},

			/**
			 *
			 * @param item    : {}
			 *    id: string
			 *    content: string
			 *    list_title: string
			 *    next_review: string    // timestamp in millis
			 *    review_type: string    // '4h', '8h', ...
			 *    time_type: string        // 'exact', 'random'
			 *
			 * @param opts    : {}
			 *    $trigger: jQueryInstance    // scheduler 打开的 trigger
			 *    onDirty: function
			 *    top:        number
			 *    left:    number
			 */
			show: function (item, opts) {
				var t = this;

				opts = opts || {};
				t.trySaveItem();
				t.removePreviousEvents();

				t.onDirty = opts.onDirty;
				t.item = item;

				var HEADER_HEIGHT = 40;
				var MARGIN = 20;
				var SCHEDULER_WIDTH = t.$el.outerWidth();
				t.top = opts.top || $(window).scrollTop() + HEADER_HEIGHT + MARGIN;

				var left = opts.left || t.g.mousePos.x - SCHEDULER_WIDTH / 2;
				var widnowLeftEdge = $(window).scrollLeft() + $(window).width();
				var splitterWidth = parseInt(t.g.list.$splitter.css('left')) + 20;
				if (left < splitterWidth) {
					left = splitterWidth;
				}
				if (left + SCHEDULER_WIDTH > widnowLeftEdge) {
					left = widnowLeftEdge - SCHEDULER_WIDTH - MARGIN;
				}
				t.left = left;

				t.$trigger = opts.$trigger;

				t.$el
					.css({top: t.top, left: t.left})
					.show();

				t.initHidingEvents();
				t.refresh();
			},

			debounceShow: _.debounce(function() {
				this.show.apply(this, arguments);
			}, 200),

			initHidingEvents: function () {
				var t = this;

				var bindHideEvents = function ($elem) {
					$elem
						.on('mouseenter.hide-scheduler', function () {
							t.debounceShow.cancel();
							t.debounceHide.cancel();
						})
						.on('mouseleave.hide-scheduler', function () {
							t.debounceHide();
						});
				};

				if (t.$trigger) {
					bindHideEvents(t.$trigger);
				}

				bindHideEvents(t.$el);
				t.$el
					.on('click.hide-scheduler', function (e) {
						e.stopPropagation();
					});
				$(document).on('click.hide-scheduler', function (e, data) {
					if (!data || !data.hideMenu) {
						t.hide();
					}
				});
			},

			removePreviousEvents: function () {
				var t = this;

				t.$el.off('.hide-scheduler');
				$(document).off('.hide-scheduler');

				t.debounceHide.cancel();
				if (t.$trigger) {
					t.$trigger.off('.hide-scheduler');
					t.$trigger = null;
				}
			},
			debounceHide: _.debounce(function () {
				this.hide();
			}, 400),
			hide: function () {
				var t = this;

				t.trySaveItem();
				t.removePreviousEvents();

				t.$el.hide();
			},

			getAttrSel: function (key, value) {
				if (arguments.length == 1) {
					return '[' + key + ']';
				}
				else {
					return '[' + key + '="' + value + '"]';
				}
			},

			selectByDataAttr: function (key, value) {
				var t = this;

				t.$(t.getAttrSel('data-' + key)).removeClass('selected');
				t.$(t.getAttrSel('data-' + key, value)).addClass('selected');
			},

			refresh: function () {
				var t = this;
				var item = t.item;

				var top = t.top;
				var left = t.left;

				t.$title.text(item.content);

				t.selectByDataAttr(
					'type',
					item.time_type === 'random' ? 'random' : 'exact'
				);

				var n = item.next_review ? new Date(+item.next_review) : new Date();

				var nextReview = t.nextReview = moment(n);

				t.refreshInput(nextReview);
				t.refreshNextReviewText(nextReview);
				t.refreshInterval(item.review_type);
			},

			refreshInterval: function (reviewType) {
				var t = this;

				t.selectByDataAttr('interval', reviewType);
			},

			refreshInput: function (nextReview) {
				var t = this;

				t.calendar.set_date(nextReview.toDate());
				t.selectByDataAttr('hour', nextReview.format('HH'));
				t.selectByDataAttr('minute', nextReview.format('mm').slice(0, 1) + '0');
			},

			refreshNextReviewText: function (nextReview) {
				var t = this;

				t.$nextReview.html(nextReview.format('YYYY-MM-DD HH:mm'));
			}
		});

		return Scheduler;
	});
