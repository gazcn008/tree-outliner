define(['lodash'], function (_) {

	var itemProcessor = {

		$mainList: null,
		$title: null,

		doInit: function () {
			if (!this.$mainList) {
				this.$mainList = $('#main-list');
				this.$title = $('#list-title');
			}
		},

		getRawList: function ($node) {
			var t = this;
			var $children;

			t.doInit();

			var node = {children: []}

			if (!$node) {
				$children = t.$mainList.children();

				node.content = t.$title.find('.content').html();
			}
			else {
				var itemView = $node.data('view');

				node.content = itemView.item.content;

				$children = $node.children('.children').children();
			}

			$children.each(function () {
				var $child = $(this);

				node.children.push(t.getRawList($child));
			});

			return node;
		},

		rgxItem: /#\{(\w+)(?:\|([^\}]*))?\}#/g,
		rgxListItem: /@\{(\w+)?\|(\w+)?\|([^\}]+)\}@/g,
		rgxImage: /(^|\s|<|>)!(\S+)!(?=\s|$|<|>)/g,
		rgxTag: /(^|\s|\n|\t)\#(?:([^#{!！\s:：]+)[.。])?([^#{!！\s:：]+)(?:[\:：][^\s\!\:：！]*(?:[\:：][^\!\s\:：！]+)?)?(([\!！][^\s\!！]+)*)/g,
		rgxTagDef: /^(\#(?:([^\{\!！\s\:：]+)[\.。])?([^\{\!！\s\:：]+)(?:[\:：][^\s\!\:：！]*(?:[\:：][^\!\s\:：！]+)?)?)((?:[\!！][^\s\!！]+)*) *[\=＝]/,

		processSmartList: function (node, root, titleLevel) {
			var t = this;

			if (!titleLevel) {
				node.content = '# ' + node.content;
				titleLevel = 2;

				root = node;
				root.context = {};
			}
			else {
				if (node.content.indexOf('# ') === 0) {
					node.content = _.repeat('#', titleLevel) + ' ' + node.content.slice(2);
					titleLevel++;
				}
			}

			node.context = root.context;
			node.root = root;

			var c = node.content;

			var c = c
				.replace(t.rgxItem, function (whole, id, text) {
					text = text || '';
					text = text.split('#').join('&#35;');

					return '[' + text + '](' + location.origin + '/app/item/' + id + ')';
				})
				.replace(t.rgxListItem, function (whole, id, itemId, listText) {
					listText = listText || '';
					listText = listText.split('#').join('&#35;');

					return '[' + listText + '](' + location.origin + '/app/list/' + id + ')';
				})
				.replace(t.rgxImage, '$1 ![image]($2)')

			// 处理定义
			var isDef = false;
			c = c.replace(t.rgxTagDef, function (whole, tagDef, space, tag, _tmp, index, text) {
				tagDef = (space ? space + '.' : '') + tag;

				root.context[tagDef] = node;

				node.space = space || '';
				node.key = tag;
				node.fullKey = tagDef;
				node.value = text.slice(index + whole.length);

				isDef = true;

				return '`' + tagDef + '`';
			});

			var prevTagIndex = 0;
			var tokens = [];
			var currContent = c;
			c = c.replace(t.rgxTag, function (whole, prefix, space, tag, _tmp, _tmp2, index, text) {

				var space = space || '';
				var key = tag;
				var fullKey = (space ? space + '.' : '') + tag;

				tokens.push({
					type: 'text',
					value: _.trim(text.slice(prevTagIndex, index) + prefix)
				});
				tokens.push({
					type: 'tag',
					space: space,
					key: key,
					fullKey: fullKey
				});
				prevTagIndex = index + whole.length;

				return '`' + fullKey + '`';
			});

			tokens.push({
				type: 'text',
				value: _.trim(currContent.slice(prevTagIndex))
			});

			var isProp = false, propKey;
			if (tokens.length >= 3) {
				if (!tokens[0].value && tokens[2].value.substr(0, 1) === ':') {
					tokens[2].value = _.trim(tokens[2].value.substr(1));
					propKey = tokens[1].fullKey;
					isProp = true;
				}
			}

			tokens = tokens.filter(function(token) {
				if (token.type === 'text') {
					token.value = _.trim(token.value);
					return !!token.value;
				}
				return true;
			});

			node.content = c;

			node.tokens = isProp ? tokens.slice(1) : tokens;

			node.children = node.children.filter(function (child) {
				var nodeType = t.processSmartList(child, root, titleLevel);
				if (!nodeType.type) {
					return true;
				}
				else if (nodeType.type === 'prop') {
					node[nodeType.key] = child;
				}
			});

			return isProp ? {type: 'prop', key: propKey} :
				isDef ? {type: 'def'} : {};
		},

		getSmartLlist: function() {
			var t = this;

			var root = t.getRawList();
			t.processSmartList(root);

			return root;
		},

		getFlattenList: function () {
			var t = this;

			var root = t.getRawList();
			t.processSmartList(root);

			console.log(root);

			return t.flattenList(root);
		},

		flattenList: function (node, list, titleLevel) {
			var t = this;

			if (!list) {
				list = [node.content];
			}
			else {
				list.push(node.content);
			}

			node.children.forEach(function (child) {
				t.flattenList(child, list, titleLevel);
			});

			return list;
		}
	};

	return itemProcessor;

});
