define(['backbone', 'utils', 'underscore', 'jquery'],
    function (Backbone, utils, _, $) {

        /**
         * Two types of things here -
         * tab
         * popup
         *
         * There could be a number of tabs
         * But only one popup is allowed
         *
         *
         */

        var infoKey = 'InfoKey_jfwsdi2',
            CON_polling = 1800,
            getInfo = function () {
                return utils.getItem(infoKey, {
                    seq: 0,
                    tabs: [],

                    // if action.hash = undefined, it means the action is taken by the master
                    actions: [],
                    popup: null
                })
            },
            setInfo = function (info) {
                utils.setItem(infoKey, info);
            },
            getWindowHash = function () {
                var info = getInfo();

                info.seq++;
                info.seq = info.seq > 10000000 ? 1 : info.seq;
                var hash = info.seq + '_' + Math.floor(Math.random() * Math.pow(36, 6)).toString(36);
                setInfo(info);

                return hash;
            },
            myHash = getWindowHash(),
            iAmMaster = false,
            inited = false;


        var Messenger = _.extend({}, Backbone.Events, {
            _timer: null,
            _hashToList: [],
            /**
             *
             * @param type {string} 'list' | 'popup' | 'review'
             * @param listId
             * @param isMaster
             */
            setup: function (type, listId, isMaster) {
                var t = this;

                if (!inited) {
                    $(window).on('beforeunload', function() {
                        var info = getInfo();
                        for (var i = info.tabs.length; i-- > 0;) {
                            var tab = info.tabs[i];
                            if (tab.hash == myHash) {
                                info.tabs.splice(i, 1);
                            }
                        }
                        setInfo(info);
                    });
                    inited = true;
                }

                clearInterval(t._timer);
                t._timer = setInterval(function () {
                        var info = getInfo();
                        var now = new Date().getTime();

                        t._hashToList = [];

                        if (type == 'tab') {
                            var master = null, tabsAlive = {};
                            for (var i = info.tabs.length; i-- > 0;) {
                                var tab = info.tabs[i];

                                if (tab.heart < now - CON_polling * 2.5) {
                                    // regarded as dead, remove it
                                    info.tabs.splice(i, 1);
                                    delete t._hashToList[tab.hash];
                                } else {
                                    if (!master || tab.hash > master.hash) {
                                        master = tab;
                                    }
                                    if (tab.hash == myHash) {
                                        info.tabs.splice(i, 1);
                                    }
                                    tabsAlive[tab.hash] = 1;
                                    t._hashToList[tab.hash] = tab.listId;
                                }
                            }

                            if (!info.tabs.length || master.hash == myHash && !iAmMaster) {
                                iAmMaster = true;
                                // promote this tab as master tab
                                t.setup(type, listId, true);
                            } else if (master.hash != myHash && iAmMaster) {
                                iAmMaster = false;
                                t.setup(type, listId, false);
                            }

                            t.processActions(info.actions, tabsAlive);

                            var newTabInfo = t.createInfo(type, listId);
                            info.tabs.push(newTabInfo);
                            t._hashToList[myHash] = listId;
                            setInfo(info);
                        } else {
                            var popupInfo = t.createInfo(type, listId);
                            if (!info.popup || popupInfo.hash > info.popup.hash) {
                                if (info.popup) {
                                    t.setAction(info.actions, info.popup.hash, 'close');
                                }
                                t.processActions(info.actions);
                            }
                        }

                        console.log('hash to list id : ', t._hashToList, ' isMaster: ', iAmMaster, isMaster);
                    },
                    // master will process info quicker
                    isMaster ? CON_polling / 2 : CON_polling)
            },
            setAction: function(hash, type, params, actions) {
                if (actions) {
                    actions.push({
                        hash: hash,
                        type: type,
                        params: params
                    })
                } else {
                    var info = getInfo();
                    info.actions.push({
                        hash: hash,
                        type: type,
                        params: params
                    });
                    setInfo(info);
                }
            },
            /**
             *
             * todo: also consinder lazying loading (when hover) and update the list id for items
             * @param listId id of the list.
             * @param itemId optional, id of the item.
             */
            gotoListItem: function(listId, itemId) {
                var listHash, t = this;
                _.some(t._hashToList, function(list, hash) {
                    if (list == listId) {
                        listHash = hash;
                        return true;
                    }
                });

                if (listHash) {
                    t.setAction(listHash, 'goto-list-item', {
                        itemId : itemId
                    });
                } else {
                    window.open('/app/list/' + listId + (itemId ? '/' + itemId : ''));
                }
            },
            processActions: function(actions, tabsAlive) {
                var t = this;
                tabsAlive = tabsAlive || {};

                for (var i = actions.length; i-- > 0 ;) {
                    var action = actions[i];
                    if (iAmMaster && !tabsAlive[action.hash]) {
                        // dead tab & i am master !
                        t.processAction(actions, i);
                    } else if (iAmMaster && !action.hash) {
                        // take care of master actions
                        t.processAction(actions, i);
                    } else if (myHash == action.hash) {
                        // or my own actions
                        t.processAction(actions, i);
                    }
                }
            },
            processAction: function(actions, idx) {
                var t = this, action = actions[idx];
                t.trigger(action.type, action.params);
                actions.splice(idx, 1);
            },
            createInfo: function (type, listId) {
                var tab = {
                    hash: myHash,
                    heart: new Date().getTime(),
                    type: type,
                    listId: listId
                }
                return tab;
            }
        });

        return Messenger;
    });