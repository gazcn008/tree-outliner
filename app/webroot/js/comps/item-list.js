define(['ajax', 'jquery', 'utils', 'underscore', 'text!tpls/common.tpl',
		'backbone', 'mousetrap', 'keymap', 'noty', 'vex-loader',
		'vex', 'help-content', 'md5', 'draggable', 'item',
		'saved-search', 'moment'],
	function (ajax, $, utils, _, commonTplStr,
			  Backbone, Mousetrap, keymap, noty, dialog,
			  vex, helpContent, md5, Draggable, Item,
			  SavedSearch, moment,
			  _undefined) {

		// __logic: __m21: * ItemList
		var ItemList = Backbone.View.extend({

			events: {
				'click li > .item': 'clickOnItem'
//            ,
//            'mousemove li > .item': 'hoverOnItem',
//            'mouseleave li > .item': 'stopHoverTimer'
			},

			// __logic: list.init require to bind this view to the list ul element
			initialize: function (opts) {
				var t = this;
				_.extend(t, opts);
				if (!t.list) return;

				t.itemViews = {};
				t.$title = $('#list-title');
				t.$titleEdit = $('#list-title-edit');

				t.$searchResults = $('#sidebar .result-div ul');
				t.$searchBox = $('#sidebar .search-box input');
				t.$searchBoxWrapper = $('#sidebar .search-box')
				t.$savedSearchBottomAnchor = $('#sidebar .saved-search-bottom-anchor')
				t.$resultContainer = $('#sidebar .result-div')
				t.$body = $('body');

				t.$page = $('#page');

				t.$hiddenZone = $('#hidden-zone');

				// 提供给全局调用
				window.itemList = t;

				// __m32: logic: init status
				t.status = {items: {}};

				t.urlHash = md5(location.origin + '/app/list/' + t.list.id);

				try {
					var status = JSON.parse(t.list.itemStatus);
				} catch (e) {
					status = null;
				}
				if (!status) {
					status = {}
				}
				t.list.itemStatus = status;

				if (!(status.searches instanceof Array)) {
					t.status.searches = [];
				} else {
					t.status.searches = status.searches;
				}

				// __m9: the var
				t.$helpBox = $('#help-box .search-box input');
				t.$helpContent = $('#help-box');

				// __m26: var: $sidebar & $content & $splitter
				t.$sidebar = $('#sidebar');
				t.$sidebarContent = $('#sidebar .content-scrollable');
				t.$content = $('#content');
				t.$splitter = $('#splitter');
				t.$header = $('.header');

				t.$remindListResult = $('#sidebar [data-tab-content="remind"] .result-list');
				t.$remindListSearch = $('#sidebar [data-tab-content="remind"] input.search');
				t.$removedList = $('#sidebar [data-tab-content="remove"]');
				t.$updatedList = $('#sidebar [data-tab-content="update"]');

				// __m12: var - $contextMenu
				t.$contextMenu = $('#context-menu-trigger');

				t.$tagInput = $('#tag-input');

				t.isWithinIframe = window !== window.top;

				t.$subContent = $('#sub-content');
				t.$subContent.hide();

				t.$closeSubContent = $('#close-sub-content');

				t.$listSearchBox = $('#sidebar .search-list input');
				t.$listSearchResult = $('#sidebar .search-list .list-result');

				for (var i = t._tags.length; i-- > 0;) {
					if (typeof t._tags[i] == 'string') {
						t._tags.splice(i, 1, {text: t._tags[i], alias: []});
					}
				}

				t.createDefDict(t.list.id, t.list.title, false);
				t.refreshCachedTags();
				t.render();

				t.initEvents();

				t.initListSearch();

			},

			initListSearch: function () {
				var t = this;
				t.refreshListSearch('');

				t.$listSearchBox
					.on('keydown', function (e) {
						var $curr = t.$listSearchResult.find('.hover');
						if (e.keyCode == keymap.up) {
							var $prev = $curr.prev();
							if ($prev.length) {
								$curr.removeClass('hover');
								$prev.addClass('hover');
							}
							e.preventDefault();
						}
						else if (e.keyCode == keymap.down) {
							var $next = $curr.next();
							if ($next.length) {
								$curr.removeClass('hover');
								$next.addClass('hover');
							}
							e.preventDefault();
						}
						else if (e.keyCode == keymap.enter) {
							if ($curr.length) {
								$curr[0].click();
							}
							e.stopPropagation();
						}
					})
					.on('keyup', function (e) {
						if (
							e.keyCode != keymap.up
							&& e.keyCode != keymap.down
							&& e.keyCode != keymap.enter
						) {
							t.refreshListSearch(e.target.value);
						}
					});

				t.$listSearchResult
					.on('click', '.list-item .button', function (e) {
						var listId = $(this).closest('.list-item').attr('data-list-id');
						t.openSubContent(null, null, listId);

						e.stopPropagation();
						e.preventDefault();
					});
			},

			refreshListSearch: _.debounce(function (searchTerm) {
				var t = this;

				ajax.post('search_list', {
					term: searchTerm
				}, {
					ok: function (res) {
						if (res && res.length) {
							var result = t.g.tpls.sidebarListResult(res);
							t.$listSearchResult.html(result);
							t.$listSearchResult.find('.list-item[data-list-id]').eq(0).addClass('hover');
						}
						else {
							t.$listSearchResult.html(t.g.tpls.sidebarNoListFound());
						}
					}
				})
			}, 180),

			initSearches: function () {
				var t = this;

				if (!t.status.searches || !t.status.searches.length) {
					t.status.searches = [{"term": "#question.open:r2"}, {"term": "#task.todo:r2"}];
				}

				_.each(t.status.searches, function (search) {
					var searchInstance = new SavedSearch({
						search: search,
						g: t.g
					});
					t.$searchBoxWrapper.after(searchInstance.render());
					searchInstance.refresh();
				});


			},

			resetList: function (items) {
				this.applyPatch(items, false, true);
			},

			openSubContent: function (href, itemId, listId, openType) {
				var t = this;
				var found = false;

				var origin = location.origin;
				if (!href && !itemId && listId) {
					href = origin + '/app/list/' + listId;
				}
				var subContentWindow = $('#sub-content')[0].contentWindow;

				if (href && href.indexOf(origin) !== -1) {
					var rest = href.replace(origin, '');

					var match = /app(?:\/list\/)?(\w+)(?:\/(\w+))?/.exec(rest);

					if (match) {
						found = true;

						var listId = match[1];

						if (!match[2]) {
							if (t.isWithinIframe) {
								if (listId === window.listId) {
									window.document.body.scrollTop = 0;
								}
								else {
									location.href = href;
								}
							} else {
								if (!t.$page.hasClass('sub-content-opened')) {
									clearTimeout(hideSubContent);
									t.$subContent.show();
									setTimeout(function () {
										t.$page.addClass('sub-content-opened');
									}, 20);
									t.$splitter.trigger('minimize');
								}

								if (listId && listId === subContentWindow.listId) {
									subContentWindow.document.body.scrollTop = 0;
								}
								else {
									t.$subContent.attr('src', href);
								}
							}
						}
						else {
							itemId = match[2];
						}
					}
				}

				if (!found && !itemId) {
					return false;
				}

				if (itemId) {
					ajax.post('item_status', {
						itemId: itemId
					}, {
						ok: function (res) {
							if (listId && res.list_id !== listId) {
								// 提示 list id 发生了变动
							}

							listId = res.list_id;

							if (openType === 'new') {
								window.open('/app/list/' + listId + '/' + itemId);
							}
							else if (t.isWithinIframe || openType === 'current') {
								if (listId === window.listId) {
									t.hoverOnItemByItemId(itemId);
								}
								else {
									location.href = '/app/list/' + listId + '/' + itemId;
								}
							}
							else {
								if (!t.$page.hasClass('sub-content-opened')) {
									clearTimeout(hideSubContent);
									t.$subContent.show();
									setTimeout(function () {
										t.$page.addClass('sub-content-opened');
									}, 20)

									if (openType !== 'for-indexed-item') {
										t.$splitter.trigger('minimize');
									}
								}

								if (listId && listId === subContentWindow.listId) {
									subContentWindow.itemList.hoverOnItemByItemId(itemId);
								}
								else {
									t.$subContent.attr('src', '/app/list/' + listId + '/' + itemId);
								}
							}
						}
					});
				}

				var hideSubContent = null;
				window.closeSubContent = function () {
					t.$page.removeClass('sub-content-opened sub-content-focus');
					clearTimeout(hideSubContent);

					hideSubContent = setTimeout(function () {
						t.$subContent.hide();
					}, 500);
					t.$splitter.trigger('resume');
				}
				return true;
			},

			refreshCachedTags: function () {
				var t = this;
				var cachedTags = utils.getItem('tag-order');
				var min = new Date();
				min.setDate(min.getDate() - 3);
				var minTime = min.getTime();

				if (cachedTags) {
					var cachedTagList = [], hasOutdated = false;
					_.each(cachedTags, function (v, k) {
						if (v > 5000 && v < minTime) {
							hasOutdated = true;
							return;
						}
						cachedTagList.push({text: k.substr(1), time: v});
					})
					cachedTagList.sort(function (a, b) {
						if (a.time > b.time) return -1;
						if (a.time < b.time) return 1;
						return 0;
					});
					var CON_maxCache = 550;
					t._cachedTags = cachedTagList.slice(0, CON_maxCache);

					if (t._cachedTags.length >= CON_maxCache || hasOutdated) {
						console.log('cached tags refreshed');
						var newCached = {};
						_.each(t._cachedTags, function (tag) {
							newCached[tag.text] = tag.time;
						});
						utils.setItem('tag-order', newCached);
					}
				} else {
					t._cachedTags = [];
				}
			},

			// __m21: the tags for the list
			// we shall parse and group them by the first dot
			// preset tags for test
			_tags: [
				'task.todo:r2', 'task.in-progress:b2', 'task.done:g2',
				'task.pending:br2', 'task.canceled:gr2',
				'question.open:r2', 'question.closed:g2', 'question.canceled:gr2',
				'type.info:b2', 'type.link:b2', 'type.image:b2',
				'priority.high:r2', 'priority.medium:y2', 'priority.low:gr2',
				'bookmark:b3'
			],
			searchTags: function (term) {
				var t = this,
					tags = t.getFilteredTags(term);
				t.renderTagInput(tags, t.$tagInput.find('.tags'), _undefined, {
					term: term
				});
			},

			// __m29: _m21: method: renderTagInput
			renderTagInput: function (tags, $tags, tagText, opts) {
				var t = this;
				if (!$tags) return;
				opts = opts || {};

				var $defs = $tags.find('.tag-defs').empty();
				$tags.find('[tag-text]').remove();
				$defs.off('.tag-def-events')
					.on('mousewheel.tag-def-events DOMMouseScroll.tag-def-events', function (e) {
						var e0 = e.originalEvent,
							delta = e0.wheelDelta || -e0.detail;

						this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
						e.preventDefault();
					})
					.on('click.tag-def-events', 'li', function () {
						var $a = $(this).find('> a');
						var listId = $a.data('list-id'),
							itemId = $a.data('item-id');

						if (listId == t.list.id) {
							t.openSubContent(null, itemId);
						}
					})
					.on('click.tag-def-events', function (e) {
						e.stopImmediatePropagation();
					})

				var showDef = false;
				if (tagText) {
					var tagTextWithHash = '#' + tagText;
					_.each(t.defDicts, function (dict) {
						if (dict.defs[tagTextWithHash]) {
							var $tagDef;
							_.each(dict.defs[tagTextWithHash], function (tagDef) {
								if (!$tagDef) $tagDef = $('<ul/>');
								var $link = $('<a/>').append(tagDef.def)
									.data('item-id', tagDef.itemId)
									.data('list-id', dict.listId);
								var $li = $('<li/>').append($link);
								if (dict.listId != t.list.id) {
									var link = location.origin + '/app/list/'
										+ dict.listId;
									$link.attr('href', link + '/' + tagDef.itemId)
										.attr('target', md5(link));
								}
								$li.find('.tag-def-label').remove();
								$tagDef.append($li);
							});
							if ($tagDef) {
								$defs.append(t.g.tpls.defTitle({
									text: 'Def in ' + dict.listTitle,
									isSelf: dict.listId == t.list.id
								}));
								$defs.append($tagDef);
								showDef = true;
							}
						}
					})
				}

				$defs.toggleClass('hidden', !showDef);

				tags = _.map(tags.slice(0, 12), function (res) {
					return '#' + res
				});
				var html = tags.join(' ');
				$tags.append(utils.replaceTags(html));
				utils.parseTags($tags);

				var idx = 0;

				if (opts.term) {
					var termChs = opts.term.toLowerCase().split(''), chLen = termChs.length,
						execute = 0;

					var highlightKeyword = function (group, content) {

						var glen = group.length, clen = content.length, idx = 0,
							getNextCh = function () {
								if (idx < glen) return group[idx++];
								else return content[idx++ - glen];
							},
							writeNextCh = function (ch) {
								if (idx <= glen) newGroup += ch;
								else newContent += ch;
							};
						var newGroup = '', newContent = '';
						for (var i = 0; i < chLen; i++) {
							var ch = termChs[i];
							while (true) {
								var cch = getNextCh();
								if (!cch) return false;
								if (execute++ > 1000) return false;
								if (ch == cch.toLowerCase()) {
									writeNextCh('<ins>' + cch + '</ins>');
									break;
								} else {
									writeNextCh(cch);
								}
							}
						}

						if (idx < glen) {
							newGroup += group.substr(idx);
							newContent = content;
						} else {
							newContent += content.substr(idx - glen);
						}

						return {
							group: newGroup,
							content: newContent
						}
					}
				}

				$tags.find('.remove-tag').nextAll('.aui-lozenge').each(function () {
					var $t = $(this);
					$t.prepend('<span class="shortcut">;' + t.shortcuts[idx] + '</span>')
						.attr('data-shortcut', t.shortcuts[idx]);
					if (opts.term) {
						var $g = $t.find('.g'), $c = $t.find('.c');
						var search = highlightKeyword($g.html(), $c.html());
						if (search) {
							$g.html(search.group);
							$c.html(search.content);
						}
					}
					idx++;
					if (idx >= t.shortcuts.length) return false;
				});
			},
			shortcuts: ['a', 's', 'd', 'f', 'g', 'e', '1', '2', '3', '4'],

			// __m21: method: findNextTag
			findNextTag: function (tagText) {
				var prts = tagText.split('.');
				var group = prts.shift() + '.', content = prts.join('.');
				var t = this;
				var first, found = false, result;

				_.some(t._tags, function (tag) {
					if (tag.text.substr(0, group.length) == group) {
						if (found) {
							result = tag;
							return true;
						}
						if (!first) first = tag;
					}
					if (tag.text == tagText) found = true;
				});

				return result ? result.text : first.text;
			},

			// __m21: __m24: method: filterTagsByGroup
			filterTagsByGroup: function (tagText) {
				var prts = tagText.split('.');
				var group = prts.shift() + '.', content = prts.join('.');
				var t = this, tags = [];

				var tagsInGroup = _.map(t._tags, function (tag) {
					return tag;
				});
				tagsInGroup.sort(function (a, b) {
					if (a.text > b.text) return 1;
					if (a.text < b.text) return -1;
					return 0;
				});

				_.each(tagsInGroup, function (tag) {
					if (tag.text.substr(0, group.length) == group) {
						tags.push(tag.text);
					}
				});

				t.renderTagInput(tags, t.$tagInput.find('.tags'), tagText);
			},

			// __m24: method: replace tag to
			replaceTagTo: function (oldTag, newTag) {
				var t = this;
				_.each(t.itemViews, function (view) {
					var content = view.item.content;
					if (content.indexOf(oldTag) != -1) {
						view.item.content = view.item.content.split(oldTag).join(newTag);
						view.hide$tagInput();
						view.refreshBody();
						t.detectItemTags(view, view.item.content);
						t.detectDef(view.item.def, view);
						t.detectDict(view.item);
						view.dirty();
					}
				});
			},

			sortTags: _.throttle(function () {
				var t = this;
//                    console.log('_tags - ', t._tags);
				var tagOrder = utils.getItem('tag-order');
				if (tagOrder && typeof tagOrder == 'object') {
					t._tags.sort(function (a, b) {
						var at = tagOrder['#' + a.text], bt = tagOrder['#' + b.text];
						at = at || 0;
						bt = bt || 0;
						if (at < bt) return 1;
						else if (at > bt) return -1;
						else return 0;
					});
				}
//                    console.log('_tags - ', t._tags);
			}, 500, {trailing: true}),

			/**
			 *
			 * @param term
			 * @param opts: {}
			 *  fromTagArea: boolean
			 * @returns {Array}
			 */
			getFilteredTags: function (term, opts) {
				var results = [], cached = [], dictTags = [], t = this, num = 0;
				opts = opts || {};

				var rgxTermText = term.toLowerCase().split('').map(function (ch) {
					switch (ch) {
						case '[':
						case '-':
						case '\\':
						case '/':
						case '^':
						case '$':
						case '*':
						case '+':
						case '?':
						case '.':
						case '(':
						case ')':
						case '|':
						case '[':
						case ']':
						case '{':
						case '}':
							return '\\' + ch;
						default:
							return ch;
					}
				})
					.join('.*');
				var rgxTerm = new RegExp(rgxTermText);
				var rgxBeginTerm = new RegExp('^' + rgxTermText);

				var num = 0;
				var matched = {};

				var match = function (tag) {
					if (tag.text.indexOf(':') == -1) tag.text = tag.text + ':';
					if (matched[tag.text]) return;
					matched[tag.text] = 1;
					var m;
					var tagText = tag.text.toLowerCase();
					if (m = rgxTerm.exec(tagText)) {
						if (rgxBeginTerm.exec(tagText)) {
							return {text: tag.text, len: m[0].length, begin: 1};
						} else {
							return {text: tag.text, len: m[0].length, begin: 0};
						}
						num++;
					}
				}

				var tryFindTag = function (tag, list) {
					if (!opts.fromTagArea && num > 15 || num > 50) return true;
					var result = match(tag)
					if (result) {
						list.push(result);
						num++;
					}
				}

				_.some(t._tags, function (tag) {
					return tryFindTag(tag, results)
				});
				if (!opts.fromTagArea) {
					_.some(t.defDicts, function (dict) {
						return _.some(dict.defs, function (def, tag) {
							return tryFindTag({text: tag.substr(1)}, dictTags)
						})
					});

					_.some(t._cachedTags, function (tag) {
						return tryFindTag(tag, cached)
					});
				}

				var sortResults = function (list) {
					list.sort(function (a, b) {
						if (a.begin > b.begin) return -1;
						if (a.begin < b.begin) return 1;

//                            if (a.len < b.len) return -1;
//                            if (a.len > b.len) return 1;
						return 0;
					});
				}
				sortResults(results);
				sortResults(cached);

				return results.map(function (tag) {
					return tag.text;
				})
					.concat(dictTags.map(function (tag) {
						return tag.text;
					}))
					.concat(cached.map(function (tag) {
						return tag.text;
					}));
			},

			setTitle: function (title) {
				var t = this;
				t.$title.find('.content').text(title);
				ajax.post('saveListTitle', {
					title: title,
					listId: t.list.id
				}, {
					ok: function () {
						document.title = title;
					}
				});
			},

			dumpSelection: function (views) {
				var t = this;
				var lists = utils.getItem('list-data'),
					newLists = {};
				if (t.list.list_type == 'tutorial') return;
				if (views.length) {
					var num = 0;
					for (var key in lists) {
						if (num++ > 10) break;
						newLists[key] = lists[key];
					}

					var selection = t.getSelectionData(null, views);
					newLists[t.list.id] = {
						children: selection.children,
						items: selection.items,
						clone: views[0].getExportData(views)
					}

					t.dumped = true;

					utils.setItem('list-data', newLists);
				}

			},

			checkIfMoved: function () {
				var t = this;
				var lists = utils.getItem('list-data');

				var list = lists[t.list.id];
				if (list) {
					if (list.moved) {
						var views = t.g.getSelectedItems();
						_.each(views, function (v) {
							v.remove(true);
						})
					}
					delete lists[t.list.id];
					utils.setItem('list-data', lists);
					t.syncer.debounceSync();
				}
				t.dumped = false;
			},

			/**
			 *
			 * @param opts
			 *    getTitle
			 *    getMeta
			 *    actions
			 *    items
			 *    itemCanClick
			 *    $container
			 *    message: string
			 */
			refreshLightList: function (opts) {
				var t = this;

				opts = opts || {};

				opts.$container.html('加载中...');
				var prevBlock = null;
				var blocks = [];

				opts.items.forEach(function (item) {
					if (!prevBlock || prevBlock.title != opts.getTitle(item)) {
						var block = prevBlock = {
							title: opts.getTitle(item),
							actions: opts.actions,
							items: [],
							itemCanClick: opts.itemCanClick,
							meta: opts.getMeta && opts.getMeta(item)
						};
						blocks.push(block);
					}
					else {
						block = prevBlock;
					}

					block.items.push(item);
				});

				opts.$container.html(t.g.tpls.lightList({
					blocks: blocks,
					renderItem: opts.renderItem,
					message: opts.message
				}))
			},

			reloadRemindList: function (term, skipLoadingMessage) {
				var t = this;

				if (!skipLoadingMessage) {
					t.$remindListResult.html('加载中...');
				}

				ajax.post('searchIndexedItems', {
					term: term || ''
				}, {
					ok: function (res) {
						var getNextReview = function (item) {
							var cls = '';
							if (item.next_review) {
								var n = new Date(+item.next_review);
								if (n.getTime() > new Date().getTime()) {
									cls = 'in-future'
								}
								var text = moment(new Date(+item.next_review)).fromNow();
							}
							else {
								text = '未设置时间'
							}

							var datetime = moment(n).format('YYYY-MM-DD HH:mm');
							return '<span title="' + datetime
								+ '" class="' + cls + '">' + text + ' (' + datetime + ') </span>';
						};

						t.refreshLightList({
							items: res,
							getTitle: function (item) {
								return getNextReview(item);
							},
							getMeta: function (item) {
								return item.id;
							},
							renderItem: function (item) {
								return t.g.tpls.lightListIndexedItem(item);
							},
							itemCanClick: true,
							message: '没有到期提醒',
							$container: t.$remindListResult
						})

						t.indexedItems = res.reduce(function (result, item) {
							result[item.id] = item;
							return result;
						}, {});
					}
				})
			},

			reloadRemoveList: function () {
				var t = this;

				t.$removedList.html('加载中...');
				ajax.post('recent_removed', {
					listId: t.list.id
				}, {
					ok: function (res) {
						t.refreshLightList({
							items: res,
							getTitle: function (item) {
								return item.removed;
							},
							getMeta: function (item) {
								return item.removed;
							},
							actions: [{
								text: '撤销',
								key: 'revert-remove'
							}],
							message: '没有最近删除记录',
							$container: t.$removedList
						})
					}
				});
			},

			reloadUpdatedList: function () {
				var t = this;

				ajax.post('recent_updated', {
					listId: t.list.id
				}, {
					ok: function (res) {

						t.refreshLightList({
							items: res,
							getTitle: function (item) {
								var datetime = /\d+-\d+-\d+ \d+:\d+/.exec(item.updated)[0];
								return '<span title="' + datetime
									+ '">' + moment(item.updated).fromNow() + ' (' + datetime + ')</span>';
							},
							itemCanClick: true,
							message: '没有最近更新记录',
							$container: t.$updatedList
						})
					}
				})
			},

			initTitleEvent: function () {
				var t = this;

				var toggleTitleEdit = function (edit, canceled) {
					if (edit) {
						Mousetrap.pause();
						t.$titleEdit.show()
							.find('input').val(t.$title.find('.content').html())
							.select();
						t.$title.hide();
					} else {
						t.$titleEdit.hide();
						t.$title.show();
						if (!canceled) {
							var title = t.$titleEdit.find('input').val();
							t.$title.find('.content').text(title);
							ajax.post('saveListTitle', {
								title: title,
								listId: t.list.id
							}, {
								ok: function () {
									document.title = title;
									t.list.title = title;
								}
							});
						}
						setTimeout(function () {
							Mousetrap.unpause();
						}, 50);
					}
				}

				t.$title.on('click', function () {
					toggleTitleEdit(true);
				});

				t.$titleEdit.on('click', '.save', function () {
					toggleTitleEdit(false, false);
				})
					.on('click', '.cancel', function () {
						toggleTitleEdit(false, true);
					})
					.on('keydown', 'input', function (e) {
						if (e.keyCode == keymap.enter) {
							toggleTitleEdit(false, false);
						} else if (e.keyCode == keymap.escape) {
							toggleTitleEdit(false, true);
						}
					});
			},

			initSaveSearchEvent: function () {
				var t = this;

				// __m30: __m32: event: save search
				t.$searchBoxWrapper.on('click', '.save-search', function () {
					// check not empty
					var val = $.trim(t.$searchBox.val());
					if (!val) {
						noty({
							text: 'The search should not be empty.',
							layout: 'bottomRight',
							timeout: 1200,
							type: 'warning'
						})
						return;
					}

					var newSearch = {
						term: val
					};
					var savedSearch = new SavedSearch({
						search: newSearch,
						g: t.g
					});
					t.$searchBoxWrapper.after(savedSearch.render());
					t.$searchBox.val('').trigger('keydown');

					t.status.searches.push(newSearch);
					t._searchSaved = true;
					t.syncer.debounceSync();

					// new saved search
					savedSearch.refresh();
				});

			},

			initSearchBoxEvents: function () {

				var t = this;

				// will read the commands
				// __m8: __m27: event: search box keydown
				var searchBoxDebounce = _.debounce(function (opts) {
					var $t = t.$searchBox, val = $t.val();
					opts = opts || {};

					t.searchItems(val, {
						skipHover: opts.skipHover
					});
				}, 500);

				t.$searchBox.keydown(function (e) {
					if (e.keyCode == keymap.escape) {
						$(this).val('');
						$(this).blur();
					} else if (e.keyCode == keymap.enter) {
						if (e.ctrlKey || e.altKey || e.metaKey) {
							$(this).next().click();
							$(this).val('');
						} else {
							$(this).blur();
						}
						e.stopPropagation();
					} else {
						searchBoxDebounce();
					}
				})
					.on('focus', function () {
						t.$searchBox.data('old-search-term', false)
						searchBoxDebounce({
							skipHover: true
						})
					})
					.on('tag-selected', function () {
						t.$searchBox.data('old-search-term', false);
						searchBoxDebounce({
							skipHover: true
						})
					}).on('refresh-search', function () {
					$(this).data('old-search-term', false);
					searchBoxDebounce({
						skipHover: true
					})
				});
			},


			// __logic: list.initEvents
			initEvents: function () {
				var t = this;

				t.initTitleEvent();

				t.initSaveSearchEvent();
				t.initSearchBoxEvents();

				t.g.setupSelectableList(t.$searchBox,
					t.$searchResults, t.$resultContainer,
					20, {
						id: 'search-items'
					});


				$(window).on('beforeunload', function () {
					if (t.dumped) {
						var lists = utils.getItem('list-data');
						delete lists[t.list.id];
						utils.setItem('list-data', lists);
						t.dumped = false;
					}

					if (t.syncer.needSync) {
						t.syncer.sync(true);
						return 'There is some update need to be synced. Please wait for the change to be saved successfully';
						// } else if (!t.isFocus && utils.checkRedirect(t.urlHash)) {
						// 	return "Do you want to refresh the page?";
					} else {
						utils.beforeUnload();
					}
				}).on('blur', function () {
					t.isFocus = false;
					t.dumpSelection(t.g.getSelectedItems());
					if (t.isWithinIframe) {
						window.top.itemList.$page.removeClass('sub-content-focus');
					}
				}).on('focus', function () {
					t.isFocus = true;
					t.checkIfMoved();
					t.refreshCachedTags();
					if (t.isWithinIframe && $(window.top.document).find('#page').hasClass('sub-content-opened')) {
						window.top.itemList.$page.addClass('sub-content-focus');
					}
				});

				if (document.hasFocus()) {
					if (t.isWithinIframe && $(window.top.document).find('#page').hasClass('sub-content-opened')) {
						window.top.itemList.$page.addClass('sub-content-focus');
					}
				}


				$(document).on('click', function () {
					t.isFocus = true;
				});

				// __m8: execute item click / hover
				t.$sidebar.on('result-hover', '.result-div li', function (e, opts) {
					// __m30: event: result-hover
					var $t = $(this), v = $t.data('view');
					opts = opts || {};
					v && v.trigger('item-hover', true, {
						preview: true
					});
				});

				t.initSidebarTabsAndLightLists();

				// __m26: event: setup draggable for the splitter
				var MINIMIZED_WIDTH = 280;
				var DEFAULT_WIDTH = 320;

				var previousX = DEFAULT_WIDTH;
				var currentX = previousX;

				var isDragging = false;
				var splitter;

				var $searchContent = $('#sidebar .sidebar-content')

				splitter = Draggable.create("#splitter",
					{
						type: "x",
						edgeResistance: 0.65,
						bounds: "#page",
						cursor: 'col-resize',
						throwProps: true,
						onDragStart: function (e) {
							t.$body.addClass('splitter-moving');
							isDragging = true;
						},
						onDragEnd: function (e) {
							var x = e.pageX;
							x = x >= 1 ? x : 1;
							previousX = x;
							setTimeout(function () {
								isDragging = false;
							}, 20);
							setSplitter(x);
						}
					})[0];

				var isMinimized = false;
				var setSplitter = function (x) {
					t.$body.removeClass('splitter-moving');
					t.$sidebar.css('width', x);
					t.$content.css('margin-left', x + 2);

					t.$splitter.css({
						transform: '',
						left: x
					});

					if (isMinimized && x > 40) {
						if ($searchContent.is(':visible')) {
							$searchContent.hide();
							setTimeout(function () {
								$searchContent.show();
							});
						}
						isMinimized = false;
					}
					if (x < 40) {
						isMinimized = true;
					}

					// @todo: hacky way of setting internal properties
					splitter.target._gsTransform = null;

					currentX = x;
				};

				setSplitter(currentX);

				t.$splitter.on('click', function () {
					if (isDragging) return;
					if (currentX <= MINIMIZED_WIDTH) {
						previousX = DEFAULT_WIDTH;
					}

					setSplitter(currentX <= MINIMIZED_WIDTH ? previousX : 1);
				});

				// 给 sub content 使用
				t.$splitter.on('minimize', function () {
					setSplitter(1);
				});

				t.$splitter.on('resume', function () {
					if (currentX < MINIMIZED_WIDTH) {
						setSplitter(DEFAULT_WIDTH);
					}
				});

				// __m21: event: click on tag in input
				t.$content.on('click', '#tag-input [tag-text]', function () {
					var $item = $(this).closest('li'), itemView = $item.data('view');
					var $text = $item.find('textarea');
					var $body = $item.find('> .item > .body');
					var tagText = $(this).attr('tag-text');

					if ($text.is(':visible')) {
						itemView.selectTag($text, tagText);
						itemView.hide$tagInput();
					} else {
						// check the nth tag, and replace, and refresh
						var $targetTag = $body.find('[tag-text].tag-hovered'),
							targetTag = $targetTag.attr('tag-text');
						var $sameTags = $body.find('[tag-text="' + targetTag + '"]');
						itemView.toggleTagTo(targetTag, $sameTags.index($targetTag), tagText);
					}

				})
				// __m21: event: removing tag
					.on('click', '#tag-input .remove-tag', function () {
						var $item = $(this).closest('li'), itemView = $item.data('view');
						var $body = $item.find('> .item > .body');
						var $text = $item.find('textarea');

						if ($text.is(':visible')) {
							itemView.removeTagAtPos($text);
							itemView.hide$tagInput();
						} else {
							// check the nth tag, and replace, and refresh
							var $targetTag = $body.find('[tag-text].tag-hovered'),
								targetTag = $targetTag.attr('tag-text');
							var $sameTags = $item.find('> .item > .body [tag-text="' + targetTag + '"]');
							itemView.toggleTagTo(targetTag, $sameTags.index($targetTag), '');
						}
					})
					.on('click', '#tag-input .replace-tag', function () {
						// __m24: event: replace tag
						var $item = $(this).closest('li');
						var $body = $item.find('> .item > .body');

						var $targetTag = $body.find('[tag-text].tag-hovered'),
							targetTag = '#' + $targetTag.attr('tag-text');
						var replaceTo = prompt('Please type in the tag that you would like to replace with',
							targetTag);

						if (!replaceTo) {
							return;
						}
						if (replaceTo.substr(0, 1) !== '#') {
							replaceTo = '#' + replaceTo;
						}

						replaceTo = $.trim(replaceTo);
						if (replaceTo && replaceTo != targetTag) {
							if (replaceTo.indexOf(':') == -1) replaceTo += ':';

							if (replaceTo.indexOf(' ') != -1) {
								alert('A tag should not contain space');
							} else {
								t.g.list.replaceTagTo(targetTag, replaceTo);
							}
						}
					});

				var filterByTag = function (tagText, isAppend, e) {
					var searchTerm = t.$searchBox.val();

					if (isAppend) {
						e && e.preventDefault();
						if (searchTerm.indexOf(tagText) == -1) {
							searchTerm += ' ' + tagText;
						} else {
							searchTerm = searchTerm.split(tagText).join('');
						}
					} else {
						searchTerm = tagText;
					}

					t.$searchBox.val(searchTerm)
						.focus()
						.trigger('tag-selected');
				}

				t.$closeSubContent.on('click', function () {
					window.top.closeSubContent();
				});

				if (!t.isWithinIframe) {
					t.$subContent
						.on('mouseenter', function (e) {
							$(this).focus();
							e.stopPropagation();
							console.log('enter')
						});
					t.$content.on('mouseenter', function () {
						t.$subContent.blur();
					});
				}

				t.$body.on('click', '.body span.aui-lozenge', function (e) {
					filterByTag('#' + $(this).attr('tag-text'),
						e.ctrlKey || e.metaKey || e.altKey, e);
				});
			},

			initSidebarTabsAndLightLists: function () {
				var t = this;

				// sidebar tab toggling
				t.$sidebar.on('click', '[data-tab]', function () {
					var $t = $(this);

					var selectedTab = $t.attr('data-tab');
					t.$sidebar.attr('data-tab-selected', selectedTab);
					t.$sidebar.find('.sidebar-content').css('display', false);

					if (selectedTab === 'remove') {
						t.reloadRemoveList();
					}
					else if (selectedTab === 'update') {
						t.reloadUpdatedList();
					}
					else if (selectedTab === 'remind') {
						t.reloadRemindList();
					}
				}).on('click', '[data-can-click="true"] .light-item', function () {
					// 点击跳转至 Item
					var $t = $(this);
					var itemId = $t.attr('data-item-id');

					var $item = t.$content.find('[item-id="' + itemId + '"]');

					t.hoverOnItem($item, true, {
						preview: true
					});
				}).on('click', '.actions [data-key="revert-remove"]', function () {
					if ($(this).closest('.reverted').length) {
						return;
					}
					var $block = $(this).closest('[data-meta]');
					$block.addClass('reverted');

					var meta = $block.attr('data-meta');

					ajax.post('revert_removed', {
						listId: t.list.id,
						removedAt: meta
					}, {
						ok: function (res) {
							var $revertedInfo = dialog.open({
								message: '删除数据已撤销, 请刷新页面查看具体效果?',
								buttons: [
									{
										text: 'OK',
										type: 'button',
										className: 'primary-button',
										click: function () {
											vex.close($revertedInfo.data('vex').id);
										}
									}]
							})
						}
					});
				}).on('click', '.light-indexed-item', function (e) {
					var $item = $(this);
					var itemId = $item.attr('data-item-id')

					t.openSubContent(null, itemId, null, 'for-indexed-item');
					e.stopPropagation();
				}).on('click', '.light-indexed-item .main-window', function (e) {
					e.stopPropagation();

					var $item = $(this).closest('[data-item-id]');
					var itemId = $item.attr('data-item-id')

					window.closeSubContent();
					t.openSubContent(null, itemId, null, 'current');
				}).on('mouseenter', '.light-indexed-item', function () {
					var $t = $(this);
					var itemId = $t.attr('data-item-id');

					var indexedItem = t.indexedItems[itemId];

					t.g.scheduler.debounceShow(indexedItem, {
						$trigger: $t
					});
				}).on('touched', '.light-indexed-item', function () {
					var $t = $(this);

					$t.addClass('touched');
				}).on('keyup', '[data-tab-content="remind"] input.search', _.debounce(function (e) {
					t.reloadRemindList(e.target.value, true);
				}, 500))

				window.onItemTouched = function (data) {
					if (data.itemId && t.indexedItems) {
						t.$sidebar.find('[data-tab-content="remind"] [data-item-id="' + data.itemId + '"]')
							.addClass('touched');

						t.indexedItems[data.itemId] = data.item;
					}
				}

			},

			// __m32: method: removeSearch
			removeSearch: function (search) {
				var t = this, idx = t.status.searches.indexOf(search);
				if (idx != -1) t.status.searches.splice(idx, 1);
				t._searchSaved = true;
				t.syncer.debounceSync();
			},

			// __m9: filter helps
			filterHelps: function (term) {
				var t = this;
				term = $.trim(term).toLowerCase();
				var rgxLabel = /(\s+)@([^@]+)@/g;
				t.$helpContent.find('.help').each(function () {
					var $help = $(this).addClass('hidden');
					$help.find('.help-item').each(function () {
						var $t = $(this), content = $t.data('content');
						$t.removeClass('hidden');

						if (content.toLowerCase().indexOf(term) != -1) {
							$help.removeClass('hidden');
							if (term) {
								var content = content.replace(new RegExp('(' + term + ')', 'gi'), '<span class="highlight">$1</span>');
							}
							content = content.replace(rgxLabel, '$1<span class="aui-label">$2</span>');
							$t.html(content);
						} else {
							$t.addClass('hidden');
						}
					})
				});
			},

			hideResults: function () {
				var t = this;
				t.$body.removeClass('is-searching');
				t.$resultContainer.hide();
			},

			refreshSavedSearches: function () {
				this.$sidebarContent
					.find('.saved-search')
					.trigger('delay-refresh');
				this.$sidebar.find('.search-box input')
					.trigger('refresh-search');
			},

			// __m19: spentTimeToItem()
			spentTimeToItem: function (spent, itemId) {
				var $t = $('[item-id=' + itemId + ']'),
					v = $t.data('view');
				if (v) {
					v.spentTime(spent);
				}
			},

			destroy: function () {
				var t = this;
				$('#sidebar-content').off();
				t.$searchBox.off();
				$(window).off();
				t.$title.off();
				t.$titleEdit.off();
			},

			// __m8: __m17: __m30: search items
			/**
			 *
			 * @param searchTerm
			 * @param opts
			 *  'skipHover': boolean
			 *  '$saveSearch': jquery
			 */
			searchItems: function (searchTerm, opts) {
				var t = this, views = t.itemViews;
				opts = opts || {};

				if (!opts.$savedSearch) {
					var oldSearchTerm = t.$searchBox.data('old-search-term');
					if (!searchTerm || !$.trim(searchTerm)) {
						t.$body.removeClass('is-searching');
						t.$resultContainer.hide();
						return;
					}
					if (oldSearchTerm == searchTerm && t.$searchResults.is(':visible')) {
						return;
					}
					t.$searchBox.data('old-search-term', searchTerm);
				}


				var terms = _.map(searchTerm.split('>'), function (t) {
					return $.trim(t);
				});

				var results = [];
				var _search = function ($ul, terms, index) {
					// to be sure that the search won't crash the app due to too much data
					if (results.length > 500) return;

					var term = terms[index];
					var $lis = $ul.children();
					var termPrts = _.map(term.split(/\s+/), function (prt) {
						return prt && prt.toLowerCase();
					});

					$lis.each(function () {
						var $li = $(this), v = $li.data('view');
						var termHitIndice = {};

						if (v.item.content) {
							var lower = v.item.content.toLowerCase();
							var notFound = _.some(termPrts, function (termPrt) {
								var prevIndex = termHitIndice[termPrt];
								if (prevIndex >= 0) {
									prevIndex++;
								} else {
									prevIndex = 0;
								}
								var nextIndex = lower.indexOf(termPrt, prevIndex);
								if (nextIndex == -1) return true;
								termHitIndice[termPrt] = nextIndex;
							});

							if (!notFound) {
								if (index == terms.length - 1) {
									results.push($li);
									_search($li.children('.children'), terms, index);
								} else {
									_search($li.children('.children'), terms, index + 1);
								}
							} else {
								_search($li.children('.children'), terms, index);
							}
						}
					});
				}

				_search(t.$el, terms, 0);

				if (opts.$savedSearch) {
					var $results = opts.$savedSearch.find('.result-div ul').empty();
				} else {
					$results = t.$searchResults.empty();
					t.$resultContainer.show();
				}

				var isFirst = true;
				if (results.length) {
					if (opts.sortType !== 'order') {
						results.sort(function ($a, $b) {
							var aItem = $a.data('view').item,
								bItem = $b.data('view').item;

							if (aItem.tempUpdated && !bItem.tempUpdated) {
								return -1;
							} else if (!aItem.tempUpdated && bItem.tempUpdated) {
								return 1;
							} else if (aItem.tempUpdated && bItem.tempUpdated) {
								return bItem.tempUpdated - aItem.tempUpdated;
							} else if (aItem.updated > bItem.updated) {
								return -1;
							} else if (aItem.updated < bItem.updated) {
								return 1;
							} else {
								return 0;
							}
						});
					}

					_.each(results, function ($li) {
						var $content = $li.children('.item').clone();
						var view = $li.data('view');
						$content.children('.edit').remove();
						var $resultLi = $('<li/>').html($content)
							.data('view', view)
							.attr('task-status', $li.attr('task-status'))
							.attr('class', $li.attr('class'))
							.toggleClass('selected', isFirst);
						$resultLi.find('#tag-input,#context-menu-trigger').remove();
						$results.append($resultLi);
						if (isFirst && !opts.skipHover) {
							$resultLi.trigger('result-hover');
						}
						isFirst = false;
					});
				} else {
					$results.append(t.g.tpls.emptyItem());
				}

				return results.length;
			},

			searchResultVisibility: function ($item) {
				var t = this, view = $item.data('view');

				var needExpanded = false;
				view.$('> .children > li').each(function () {
					var $child = $(this);
					if (t.searchResultVisibility($child)
						|| $child.is('[search-hit=true]')) {
						needExpanded = true;
					}
				});

				view.onToggleExpanded(null, needExpanded);
				$item.attr('search-expanded', needExpanded);
				return needExpanded;
			},

			seeParent: function (id) {
				var t = this;
				var v = t.itemViews[id];
				if (v) {
					v.$('>.item').addClass('flash flash-background');
					v.scrollIntoView();
				}
			},

			unseeParent: function (id) {
				var t = this, v = t.itemViews[id];
				if (v) {
					v.$('>.item').removeClass('flash-background');
					setTimeout(function () {
						v.$('>.item').removeClass('flash');
					}, 200);
				}
			},

			// __logic: getChanges
			getChanges: function () {
				var t = this;
				var changes = [];
				_.each(t.itemViews, function (itemView) {
					if (itemView.item.isNew == true) return;

					if (itemView.$el.children('.item.edit-mode').length) return;

					var patch = t.getItemChange(itemView.item, itemView.old,
						t.keys.content, t.keys.core, true
					);

					if (patch) {
						if (patch.indexed || itemView.item.indexed && patch.content) {
							patch.tags = utils.getTags(itemView.item.content);
							patch.indexed = true;
						}

						changes.push({
							patch: patch,
							view: itemView
						});
					}
				});
				return changes;
			},
			keys: {
				core: ['id', 'id_seq'],
				content: ['task_status', 'content', 'seq', 'parent_item_id', 'ref_item_id',
					'removed', 'remove_attempt', 'is_hidden', 'task_points',
					'task_spent', 'def', 'indexed'],

				// normal conflict keys
				item: ['task_status', 'content', 'ref_item_id',
					'removed', 'remove_attempt', 'is_hidden', 'task_points',
					'user_hash', 'updated', 'task_spent', 'def', 'indexed', 'tags'],

				// essential conflict keys
				structure: ['seq', 'parent_item_id']
			},

			getItemChange: function (newItem, oldItem, keys, includes, checkForceSave) {
				var hasChange = false, itemPatch = {};
				keys.forEach(function (k) {
					if (newItem[k] != oldItem[k] ||
						(newItem[k] !== oldItem[k] &&
							(typeof newItem[k] == 'undefined' ||
							typeof oldItem[k] == 'undefined')
						)) {
						itemPatch[k] = newItem[k];
						hasChange = true;
					}
				});
				includes && includes.forEach(function (k) {
					itemPatch[k] = oldItem[k];
				});
				if (hasChange || (checkForceSave && newItem.forceSave)) return itemPatch;
			},

			// __logic: __m32: method: getStatusChanges
			_statusSavingAttempt: 0,
			getStatusChanges: function (force) {
				var t = this;

				if (force || t._searchSaved || t._statusDirty && t._statusSavingAttempt > -1) {
					t._statusSavingAttempt = 0;
					return JSON.stringify(t.status);
				} else {
					return _undefined;
				}
				t._statusSavingAttempt++;
			},

			smartTogglingList: _.debounce(function () {
				var t = this;

				var count = 10;
				var $prevs = $('#main-list > li')

				while (count > 0 && $prevs.length) {
					$prevs.each(function () {
						if (count) {
							$(this).data('view').onToggleExpanded(null, true);
							count--;
						}
					});
					$prevs = $prevs.find('> .children > li');
				}

			}, 200),

			render: function () {
				var t = this;
				t.$el.empty();
				if (t.list.items.length) {
					var parsedItems = t.parseItems(t.list.items);
					t.sortItems(parsedItems);
					if (parsedItems.length) {

						var firstVisit = true;
						// __m32: logic: check the itemViews & vacuum the status
						if (t.list.itemStatus && t.list.itemStatus.items) {
							var status = t.list.itemStatus.items;

							for (var itemKey in status) {
								// renderItem 中对有引用到的 item, 删掉这一个属性
								status[itemKey].toRemove = true;
								firstVisit = false;
							}
						}

						if (firstVisit) {
							t.smartTogglingList();
						}

						parsedItems.orphans.forEach(function (item) {
							t.renderItem(t.$el, item, status);
						});
						parsedItems.forEach(function (item) {
							t.renderItem(t.$el, item, status);
							t._statusDirty = true;
						});

						// 主动清除没被访问到的 status 节点
						if (status) {
							var keysToRemove = [];

							for (var itemKey in status) {
								if (status[itemKey].toRemove) {
									keysToRemove.push(itemKey);
								}
							}

							keysToRemove.forEach(function (itemKey) {
								delete status[itemKey]
							});
							if (keysToRemove.length) {
								t.statusDirty();
							}
						}
					} else {
						t.renderEmptyList();
					}
				} else {
					t.renderEmptyList();
				}

				// __m18: logic - scroll to item
				t.$title.find('.content').text(t.list.title);
				$(window).scrollTop(1);
				t.hoverOnItem(t.$focusedItem || t.$el.children('li').eq(0), true, {
					permaLink: true
				});

				// __m9: render the help
				var rgxLabel = /(\s+)@([^@]+)@/g;
				var $helpWrapper = $('#help-box .help-content');
				_.each(helpContent, function (help) {
					var $help = $('<div class="help"></div>');
					$('<h2/>')
						.html(help.title)
						.data('content', help.title)
						.appendTo($help);

					_.each(help.content, function (item) {
						$('<div class="help-item"/>')
							.html(item.text.replace(rgxLabel, '$1<span class="aui-label">$2</span>'))
							.data('content', item.text)
							.data('cmd', item.cmd)
							.appendTo($help);
					});

					$helpWrapper.append($help);
				});

				t.postRender();
				console.log(t.defDicts);
			},

			postRender: function () {
				this.renderLearningFields();
			},

			isLearningMode: true,

			renderLearningFields: function () {
				$('.field').each(function () {
					if (Math.random() > 0.3) {
						$(this).addClass('hidden-field')
					}
				});
			},

			showLearningFields: function () {
				$('.field').removeClass('hidden-field');
			},

			// __logic: list.applyPatch
			/**
			 *
			 * @param patch
			 * @param dirty
			 * @param force {boolean} - force update, regardless the conflict.
			 *  also, if in force, you will be forced to remove item that is not visited.
			 */
			applyPatch: function (patch, dirty, force) {
				var t = this;

				t.parseItemDataAndGetIndex(patch);
				t.applyPatchNew(patch, force);
				var parsedItems = t.checkPatchConflicts(force);
				t.sortItems(parsedItems);

				// __m22: dirty set as true
				parsedItems.forEach(function (item) {
					t.renderItem(t.$el, item);
				});

				_.each(t.itemViews, function (view) {
					if (view.item.removed) {
						view.$el.remove();
					} else {
						if (view.item.remove_attempt) {
							view.$el.addClass('remove-attempt');
						}
					}
				});

				_.each(t.itemViews, function (view) {
					view.checkChildren();
				})

				t.g.focusOnEditor();

				// 确保 sync 之后能 hover 到目标 item
				if (t.$focusedItem) {
					t.hoverOnItem(t.$focusedItem, true);
				}
			},

			/**
			 *
			 * @param patch
			 * @param force see applyPatch's force
			 */
			applyPatchNew: function (patch, force) {
				var t = this, views = t.itemViews;
				for (var i = patch.length; i-- > 0;) {
					var item = patch[i];
					t.parseData(item);
					var v = views[item.id];
					if (!v) {
						v = t.getItem(item);
						if (force) v.conflict = item;
					} else {
						v.conflict = item;
					}
				}
			},

			/**
			 *
			 * @param force see applyPatch's force
			 * @returns {*}
			 */
			checkPatchConflicts: function (force) {
				var t = this;
				var views = t.itemViews;

				_.each(views, function (v) {
					if (v.conflict) {
						if (force) {
							itemData = v.conflict;
							itemData.removed = false;
							v.$el.data('view', v);
							v.g = t.g;
							v.visited = true;
						} else {
							var itemData = t.getItemChange(v.conflict, v.old, t.keys.item);
						}
						_.extend(v.item, itemData);
						_.extend(v.old, itemData);

						var structureData = t.getItemChange(v.conflict, v.old, t.keys.structure);
						if (structureData) {
							if (v.conflict.parent_item_id != v.item.parent_item_id) {
								if (t.isConflictMergable(v.conflict)) {
									v.item.parent_item_id = v.conflict.parent_item_id;
									v.item.seq = v.conflict.seq;
								} else {
									v.old.parent_item_id = v.conflict.parent_item_id;
									v.old.seq = v.conflict.seq;
								}
							} else {
								v.item.seq = v.conflict.seq;
							}
						}
						delete v.conflict;
					}

				});

				var parsedItems = t.parseItems(_.map(views, function (v) {
					return v.item
				}));

				var viewsToRemove = [];
				_.each(views, function (v) {
					if (force && !v.visited) {
						viewsToRemove.push(v);
						v.item.removed = 0;
					} else {
					}
				});

				if (force) {
					_.each(viewsToRemove, function (v) {
						v.remove();
					});
				}

				return parsedItems;
			},

			isConflictMergable: function (conflict, visited) {
				var t = this, views = t.itemViews;

				if (!conflict.id) return false;

				if (!visited) {
					visited = {};
					visited[conflict.id] = 1;
				} else {
					if (conflict.id in visited) {
						return false;
					}
				}

				if (!conflict.parent_item_id) return true;
				var parent = views[conflict.parent_item_id];
				if (!parent) return false;
				if (parent.conflict) {
					return t.isConflictMergable(parent.conflict, visited);
				} else {
					return t.isConflictMergable(parent.item, visited);
				}
			},

			rgxDict: /^dict\:\s*@\{([^\|]+)\|[^\|]*\|([^\}\|]+)\}@/,
			detectDict: function (item) {
				var t = this;
				var m = t.rgxDict.exec(item.content);
				if (m) {
					t.createDefDict(m[1], m[2], item.id);
				} else {
					t.removeDict(item.id);
				}
			},

			detectDef: function (def, view) {
				var t = this;
				if (def) {
					t.addDef(def, view.item, t.defDicts[t.list.id], view.$('> .item > .body').html());
				} else {
					t.removeDef(view.item.id, t.list.id);
				}
			},

			removeDict: function (refItem) {
				var t = this;
				if (refItem) {
					_.some(t.defDicts, function (dict, key) {
						var idx = dict.refItems.indexOf(refItem);
						if (idx != -1) {
							dict.refItems.splice(idx, 1);
							if (!dict.refItems.length) {
								delete t.defDicts[key];
							}
							return true;
						}
					});
				}
			},

			removeDef: function (itemId, listId) {
				var t = this,
					dictDefs = t.defDicts[listId || t.list.id];
				if (dictDefs.index[itemId]) {
					delete dictDefs.defs[dictDefs.index[itemId]][itemId];
					delete dictDefs.index[itemId];
				}
			},

			detectItemTags: function (view, content) {
				var t = this;

				var tagInfo = utils.detectTags(content);
				var newTags = tagInfo.tags;

				if (tagInfo.def) {
					view.item.content = tagInfo.content;
				}

				if (view.item.def != tagInfo.def) {
					view.item.def = tagInfo.def;

					if (!tagInfo.def) {
						// to remove the defs
					}
					view.dirty();
				}


				// __m23: logic: need to detect the alias as well.
				_.each(newTags, function (newTag) {
					var found = _.some(t._tags, function (tag) {
						if (tag == newTag.text || tag.text == newTag.text) {
							if (newTag.alias.length) {
								_.each(newTag.alias, function (alias) {
									if (tag.alias.indexOf(alias) == -1) {
										tag.alias.push(alias);
									}
								})
							}
							return true;
						}
					});

					if (!found) {
						t._tags.push(newTag);
					}
				});
			},

			// __m34: def dicts, '_' is the current list
			/**
			 * def of the dicts: {}
			 *  $listId: #dictEntry
			 *
			 * #dictEntry: {}
			 *  refItems: []
			 *      $_: string -- the item id that defines the dict
			 *  listId: string
			 *  listTitle: string
			 *  defs: {}
			 *      $tagText: {}
			 *          defText: string
			 *          itemId: string
			 */
			defDicts: {},
			createDefDict: function (listId, listTitle, refItem) {
				var t = this;
				t.removeDict(refItem)

				if (!this.defDicts[listId]) {
					if (refItem) {
						t.syncer.debounceQuickSync();
					}
					this.defDicts[listId] = {
						isSelf: !refItem,
						isNew: true,
						refItems: [refItem],
						listId: listId,
						listTitle: listTitle,
						defs: {},
						index: {}
					}
				} else {
					this.defDicts[listId].refItems.push(refItem);
				}
			},

			addDef: function (def, item, dictDefs, html) {
				var t = this, itemId = item.id;
				t.removeDef(itemId);
				t.removeDict(itemId);

				if (!dictDefs.defs[def]) dictDefs.defs[def] = {};
				dictDefs.defs[def][itemId] = {
					itemId: itemId,
					def: html
				};
				dictDefs.index[itemId] = def;
			},

			// __logic: __m11: __m21: list.renderItem
			renderItem: function ($list, item, status) {
				var t = this;
				if (item.removed) return;
				var itemView = t.itemViews[item.id];

				if (status && status[item.id] && status[item.id].expanded) {
					t.status.items[item.id] = {expanded: true};
				}

				if (status && status[item.id]) {
					delete status[item.id].toRemove;
				}

				if (itemView) {
					itemView.refresh();
				} else {
					itemView = t.getItem(item);
				}
				$list.append(itemView.$el);

				item.taskChildren = 0;
				item.taskSpent = 0;
				item.taskChildrenSpent = 0;

				t.detectItemTags(itemView, item.content);
				t.detectDef(item.def, itemView);
				t.detectDict(item);

				if (!item.content) {
					var isEmpty = true;
				}
				if (item.children && item.children.length) {
					t.sortItems(item.children);
					item.children.forEach(function (child) {
						if (!t.renderItem(itemView.$('> .children'), child, status)) {
							isEmpty = false;
						}

						item.taskChildren += child.taskTotal;
						item.taskChildrenSpent += child.taskSpent;
					})
				}

				// if content is empty, we remove it. because it's not user friendly to show them.
				// but, we don't remove any item that is being edited.
				if (item.isOrphan || isEmpty && !itemView.$el.children('.item.edit-mode').length) {
					itemView.remove();
				}

				if (item.isDirty) {
					itemView.old = {
						id: item.id,
						id_seq: item.id_seq
					}

					delete item.isDirty;
				}
				return isEmpty;
			},

			getDicts: function (getNewDicts) {
				var res = [];
				_.each(this.defDicts, function (dict) {
					if (dict.isSelf) return;
					if (dict.isNew == getNewDicts) {
						res.push(dict.listId);
					}
				});
				return res;
			},

			setDictSynced: function (listId, dictList) {
				var t = this, dict = this.defDicts[listId];
				if (dict) {
					dict.isNew = false;
					dict.itemSynced = {};

					_.each(dictList.items, function (item) {
						var html = Item.getStandaloneBody(item.content).html();
						t.addDef(item.def, item, t.defDicts[listId], html);
						dict.itemSynced[item.id] = item.synced;
					});
				}
			},

			syncDict: function (dictLists) {
				var t = this;
				_.each(dictLists, function (list, listId) {
					if (list.length) {
						var dict = t.defDicts[listId];
						if (dict) {
							_.each(list, function (item) {
								var synced = dict.itemSynced[item.id];
								if (!synced || synced < item.synced) {
									if (item.removed) {
										t.removeDef(item.id, listId);
										delete dict.itemSynced[item.id];
									} else {
										t.addDef(item.def, item, dict, Item.getStandaloneBody(item.content).html());
										dict.itemSynced[item.id] = item.synced;
									}
								}
							});
						}
					}
				});
			},

			// __logic: list.renderEmptyList
			// __m21: need to create init items for tags
			renderEmptyList: function () {

				var t = this;
				var item = t.getItem({
					isNew: true,
					list_id: t.list.id,
					user_hash: t.g.userHash,
					task_points: 0,
					task_spent: 0,
					permanent: true
				});
				t.$el.html(item.$el);
			},

			// __logic: list.getParent
			getParent: function (itemView) {
				var $list = itemView.$el.closest('.children, .items');
				var parentView = $list.parent().data('view');

				if (parentView) {
					var parent = parentView.item;
				}
				return {
					$: $list,
					view: parentView,
					data: parent
				}
			},


			// __logic: list.statusDirty
			statusDirty: function (value) {
				var t = this;
				if (typeof value == 'boolean') {
					t._statusDirty = value;
				} else {
					t._statusDirty = true;
				}
			},

			// __logic: list.toggleItems
			toggleItems: function ($childrenContainer, expanded) {
				var $ch = $childrenContainer.children('li');
				if ($ch.length) {
					var first = $ch.eq(0).data('view');
					if (typeof expanded != 'boolean')
						expanded = !first.status.expanded;
					$ch.each(function () {
						var $t = $(this), v = $t.data('view');
						if (v) {
							v.onToggleExpanded(null, expanded);
						}
					});
				}
			},

			cleanItemIndex: function () {
				var t = this, toRemoved = [];
				_.each(t.itemViews, function (view) {
					if (view.item.removed) {
						toRemoved.push(view.item.id);
					}
				});

				_.each(toRemoved, function (id) {
					delete t.itemViews[id];
				});
			},

			// __logic: list.removeItemFromIndex
			removeItemFromIndex: function (view) {
				if (view.$el) view.$el.remove();
				delete this.itemViews[view.item.id];
			},

			scrollToItem: function (view) {
				view.scrollIntoView();
			},


			// __m8: item expand/collapse handlers
			previewExpandings: [],
			previewExpand: function ($itemLi) {
				var t = this;
				t.previewCollapse();

				$itemLi.parents('li').each(function () {
					var $t = $(this), v = $t.data('view');
					if (v) {
						if (!v.isExpanded()) {
							t.previewExpandings.push(v);
							v.onToggleExpanded(null, true);
						}
					}
				});
			},
			previewCollapse: function () {
				var t = this;
				_.each(t.previewExpandings, function (v) {
					v.onToggleExpanded(null, false);
				});
				t.previewExpandings = [];
			},

			// __logic: __m18: list.getItem
			getItem: function (itemData) {
				var t = this;
				var itemView = new Item({
					g: t.g,
					item: itemData,
					// __m10: status for the item
					status: t.status.items[itemData.id]
				});

				if (itemData.id == t.focusedItem) {
					t.$focusedItem = itemView.$el;
					delete t.focusedItem;
				}

				var createNew = function () {
					return {
						isNew: true,
						task_points: 0,
						task_spent: 0,
						user_hash: t.g.userHash,
						list_id: t.list.id
					};
				}

				itemView

				// __m28: event: split item
					.on('split-item', function () {
						var $textarea = itemView.$('.edit textarea');
						var textarea = $textarea.get(0);

						var pos = textarea.selectionEnd;
						var val = $textarea.val(),
							pre = val.substr(0, pos),
							post = val.substr(pos);

						if (pre && post) {
							itemView.trigger('new-item-prev', {
								content: pre
							});
							$textarea.val(post);
							textarea.selectionStart = textarea.selectionEnd = 0;
						}
					})

					// __logic: list item == item-dirty
					.on('item-dirty', function () {
						t.syncer.debounceSync();
					})

					// __logic: __m8: list item == item-hover
					.on('item-hover', function (force, opts) {
						opts = opts || {};
						t.hoverOnItem(itemView.$el, force, opts);
					})

					// __logic: list item == remove-item
					.on('remove-item', function (isUIRemoveOnly) {
						var parent = t.getParent(itemView);
						if (itemView.item.removed) {
							return;
						}

						var target = t.g.hoveredTarget();
						if (target.view == itemView) {
							t.unhoverOnItem(target);
						}

						var $newHover = itemView.$el.next();
						if (!$newHover.length) {
							$newHover = itemView.$el.prev();
						}
						if (!$newHover.length) {
							$newHover = itemView.$el.parent().parent();
						}
						if ($newHover.length) {
							t.hoverOnItem($newHover, true);
						}
						t.g.toggleSelect(itemView, false);

						var itemId = itemView.item.id
						t.removeDef(itemId);
						t.removeDict(itemId);

						if (!isUIRemoveOnly) {
							itemView.$('li.item-wrapper').each(function () {
								var $t = $(this), v = $t.data('view');
								v.item.removed = 1;
								if (!v.hasUpdate) v.dirty();
							});

							itemView.item.removed = 1;
							if (!itemView.hasUpdate) itemView.dirty();
							t.reorderItems(parent.$);
						} else {
							delete t.itemViews[itemView.item.id];
						}

						itemView.$el.remove();

						if (parent.data) {
							parent.view.checkChildren();
						}
					})

					// logic: list item == new-item-prev
					.on('new-item-prev', function (item) {
						var newItem = createNew();
						var newItemView = t.getItem(newItem);

						itemView.$el.before(newItemView.$el);
						var parent = t.getParent(itemView);
						if (parent.data) newItem.parent_item_id = parent.data.id;
						t.reorderItems(parent.$);

						if (!item) {
							newItemView.onEdit();
						} else {
							newItem.content = item.content;
							newItemView.refreshBody();
							newItemView.dirty();
							newItemView.postSaveActions();
						}
					})

					// __m28: logic: list item == new-item-next
					.on('new-item-next', function (item) {
						if (itemView.$el.is('.empty-list')) {
							itemView.onEdit();
							return;
						}
						var newItem = createNew();
						var newItemView = t.getItem(newItem);

						var parent = t.getParent(itemView);
						if (parent.data) newItem.parent_item_id = parent.data.id;
						itemView.$el.after(newItemView.$el);
						t.reorderItems(parent.$);

						if (!item) {
							newItemView.onEdit();
						} else {
							newItem.content = item.content;
							newItemView.refreshBody();
							newItemView.dirty();
							newItemView.postSaveActions();
						}
					})

					// __logic: list item == new-item-parent
					.on('new-item-parent', function (items) {
						items = items || [itemView];
						var newItem = createNew();
						var newItemView = t.getItem(newItem);

						var first = items[0];
						first.$el.before(newItemView.$el);

						var parent = t.getParent(first);
						if (parent.data) {
							newItem.parent_item_id = parent.data.id;
						}

						t.sortItemViews(items);
						_.each(items, function (item) {
							newItemView.$('>.children').append(item.$el);
							item.item.parent_item_id = newItem.id;
						});

						t.reorderItems(newItemView.$('>.children'));
						t.reorderItems(parent.$);
						newItemView.onEdit();
						newItemView.checkChildren();
						newItemView.onToggleExpanded(null, true);
					})

					// __m8: item preview expand/collapse
					.on('preview-expand', function () {
						t.previewExpand(itemView.$el);
					})

					.on('prevew-collapse', function () {
						t.previewCollapse();
					})

					// __logic: list item == new-item-child
					.on('new-item-child', function (item) {
						var newItem = createNew();
						var newItemView = t.getItem(newItem);

						if (item && item.firstChild) {
							itemView.$('> .children').prepend(newItemView.$el);
						}
						else {
							itemView.$('> .children').append(newItemView.$el);
						}
						newItem.parent_item_id = itemData.id;
						t.reorderItems(itemView.$('> .children'));
						itemView.checkChildren();
						itemView.onToggleExpanded(null, true);

						if (!item) {
							newItemView.onEdit();
						} else {
							newItem.content = item.content;
							newItemView.refreshBody();
							newItemView.dirty();
							newItemView.postSaveActions();
						}
					})

					// __logic: list item == move-up
					.on('move-up', function (items) {
						items = items || [itemView];

						t.sortItemViews(items);
						var first = items[0];
						var parent = t.getParent(first);

						var $tmp = $('<span></span>');
						var $prev = first.$el.prev();
						if ($prev.length) {
							$prev.before($tmp);
						} else {
							parent.$.append($tmp);
						}

						_.each(items, function (item) {
							$tmp.before(item.$el);
//							console.log(item.item.content);
						});
						$tmp.remove();
						t.hoverOnItem(items[0].$el, true);

						t.reorderItems(parent.$);
					})

					// __logic: list item == move-down
					.on('move-down', function (items) {
						items = items || [itemView];

						t.sortItemViews(items);
						var mark = items[items.length - 1];
						var parent = t.getParent(mark);

						var $tmp = $('<span></span>');

						var $mark = mark.$el.next();
						if ($mark.length) {
							$mark.after($tmp);
						} else {
							parent.$.prepend($tmp);
						}

						_.each(items, function (item) {
							$tmp.before(item.$el);
						});
						$tmp.remove();
						t.hoverOnItem(items[0].$el, true);

						t.reorderItems(parent.$);
					})
					// __logic: list item == move-parent
					.on('move-parent', function (items) {
						items = items || [itemView];

						var parent = t.getParent(items[0]);

						if (parent.data) {
							t.sortItemViews(items);
							var $tmp = $('<span></span>');
							t.unhoverOnItem(items[0].$el);
							parent.$.parent().after($tmp);
							_.each(items, function (item) {
								$tmp.before(item.$el);
//								console.log(item.item.content);
								item.item.parent_item_id = parent.data.parent_item_id ?
									parent.data.parent_item_id : 0;
								item.dirty();
							});
							parent.view.checkChildren();
							t.reorderItems($tmp.parent());
							t.reorderItems(parent.$);
							t.hoverOnItem(items[0].$el, true);
							$tmp.remove();
						}
					})
					// __logic: list item == move-child
					.on('move-child', function (items) {
						items = items || [itemView];
						t.sortItemViews(items);

						var mark = items[0];
						var $mark = mark.$el.prev();
						var newParent = $mark.data('view');

						if ($mark.length) {
							var $childrenContainer = $mark.find('> .children');
							t.unhoverOnItem(items[0].$el);
							newParent.checkChildren(true);
							newParent.onToggleExpanded(null, true);
							_.each(items, function (item) {
								$childrenContainer.append(item.$el);
								item.item.parent_item_id = newParent.item.id;
//								console.log(item.item.content);
								item.dirty();
							});
							if (newParent.$el.hasClass('is-collapsed')) {
								t.hoverOnItem(newParent.$el, true);
							} else {
								t.hoverOnItem(items[0].$el, true);
							}
							t.reorderItems($childrenContainer);
						}
					})
					// __logic: list item == save-item-status
					.on('save-item-status', function (status) {
						t.status.items[itemData.id] = status;
						t.statusDirty();
					})
					// __logic: list item == toggle-child
					.on('toggle-child', function (skipSelect) {
						itemView.onToggleExpanded(null, true);
						t.toggleItems(itemView.$('>.children'));
					})
					// __logic: list item == toggle-item
					.on('toggle-item', function (skipSelect) {
						itemView.onToggleExpanded();
					})
					// __logic: list item == toggle-siblings
					.on('toggle-siblings', function (skipSelect) {
						var parent = t.getParent(itemView);
						t.toggleItems(parent.$, !(!itemView.status || !!itemView.status.expanded));
						itemView.scrollIntoView();
					})
					// __logic: list item == toggle-parent
					.on('toggle-parent', function (skipSelect) {
						var $parent = itemView.getParent();
						if ($parent) {
							var v = $parent.data('view');
							v.onToggleExpanded(null, false);
							t.hoverOnItem($parent, true);
						} else {
							itemView.onToggleExpanded();
						}
					})
				;

				t.itemViews[itemView.item.id] = itemView;
				return itemView;
			},
			// __logic: list.hoverOnItem
			_hoverTimer: null,

			clickOnItem: function (e) {
				var $t = $(e.currentTarget).parent();
				if (!$t.is('.hovered')) {
					this.hoverOnItem($t, true, {
						preventScrolling: true
					});
				}
			},

			hoverOnItemByItemId: function (itemId) {
				var t = this;
				t.hoverOnItem(t.$('[item-id=' + itemId + ']').eq(0), true, {
					preview: true
				});
			},

			// __m8: __m18: hoverOnItem()
			/**
			 *
			 * @param e
			 * @param force
			 * @param opts
			 *  'preventScrolling': to prevent scrolling to the item
			 */
			hoverOnItem: function (e, force, opts) {
				var t = this;
				opts = opts || {};
				var $t = e.jquery ? e : $(e.currentTarget).parent();
				clearTimeout(t._hoverTimer);
				var target = t.g.hoveredTarget();
				var v = $t.data('view');
				if (!v) {
					if (opts.permaLink) {
						// todo: we shall use the item id to navigate to the list
						noty({
							text: 'The item cannot be found, please make sure that it is still a valid link',
							layout: 'bottomRight',
							type: 'warning',
							timeout: 2000
						});
					}
					return;
				}

				if ((!t.g._skipHoverAction || force)
					&& (!target || !target.type || e.currentTarget != target.$el.get(0))) {
					var _hover = function () {
						t.unhoverOnItem(target);
						$t.parents('li').addClass('parent-hovered');

						// __m12: set menu - hovered
						$t.addClass('hovered');
						if (!$t.find('> .item #context-menu-trigger').length) {
							$t.find('> .item > .status').prepend(t.$contextMenu);
							if (t.$contextMenu.is('.active')) {
								t.$contextMenu.trigger('click');
							}
						}
						var view = $t.data('view');
						t.g.app.itemUrl(view && view.item);

						t.g.hoveredTarget('item', $t);
						if (e.jquery && !opts.preventScrolling) {
							$t.data('view').scrollIntoView(!e.jquery, opts);
						}
					};

					if (force) _hover();
					else t._hoverTimer = setTimeout(function () {
						_hover();
						t.$searchBox.blur();
					}, 140);

				}

				// 剔除 focused item
				delete t.$focusedItem;
			},

			// __logic: list.unhoverOnItem
			unhoverOnItem: function (target) {
				if (!target) return;
				if (target.jquery) $el = target
				else $el = target.$el;
				if (!$el) return;

				var t = this;
				_.each(t.itemViews, function (view) {
					view.$el.removeClass('parent-hovered').removeClass('hovered');
				});

				$el.parents('li').removeClass('parent-hovered');
				$el.removeClass('hovered');
				t.g.removeHoveredTarget();
			},

			//__logic: list.stopHoverTimer
			stopHoverTimer: function (e) {
				var t = this;
				clearTimeout(t._hoverTimer);
			},

			// __logic: list.parseData
			parseData: function (item) {
				var t = this;

				item.id_seq = parseInt(item.id_seq);
				if (!item.id_seq || isNaN(item.id_seq)) {
					item.id_seq = t.g.idSeq++;
				}

				item.content = $.trim(utils.addColonForTags(item.content));

				if (!item.spent_date || item.spent_date < utils.getToday()) {
					item.task_points = 0;
					item.task_spent = 0;
				} else {
					// __m11: init task points & spent
					item.task_points = t.parseDoubleDefault(item.task_points, 0);
					item.task_spent = t.parseDoubleDefault(item.task_spent, 0);

				}

				item.seq = parseInt(item.seq);
				if (!item.seq || isNaN(item.seq)) {
					item.seq = 999999;
				}
			},

			parseIntDefault: function (value, def) {
				var i = parseInt(value);
				if (isNaN(i)) return def;
				return i;
			},

			parseDoubleDefault: function (value, def) {
				var i = Number(value);
				if (isNaN(i)) return def;
				return i;
			},

			parseItemDataAndGetIndex: function (items, extraOp) {
				var t = this;
				var itemHash = {};
				items.forEach(function (item) {
					t.parseData(item);
					if (item.parent_item_id === '0' ||
						item.parent_item_id === 0) {
						item.parent_item_id = null;
					}

					itemHash[item.id] = item;
					extraOp && extraOp(item);
				});
				return itemHash;
			},

			// __logic: list.parseItems
			parseItems: function (items) {
				var t = this;
				var parsedItems = [], orphans = parsedItems.orphans = [];
				var itemHash = t.parseItemDataAndGetIndex(items, function (item) {
					item.children = [];
				});

				items.forEach(function (item) {
					if (!item.parent_item_id) {
						parsedItems.push(item);
						return;
					}

					var parent = itemHash[item.parent_item_id];
					if (parent) {
						parent.children.push(item);
					} else {
						item.isOrphan = true;
						orphans.push(item);
					}
				});

				return parsedItems;
			},

			// __logic: list.reorderItems
			reorderItems: function ($list) {
				var seq = 1, t = this;
				$list.find('> .item-wrapper').each(function () {
					var $t = $(this);
					var itemView = $t.data('view');
					if (itemView) {
//							console.log(itemView.item.seq, ' => ', seq);
						itemView.item.seq = seq++;
						itemView.dirty();
					}
				})
			},

			sortItems: function (items) {
				items.sort(function (a, b) {
					if (a.seq > b.seq) return 1;
					else if (a.seq < b.seq) return -1;
					else if (a.user_hash > b.user_hash) return 1;
					else if (a.user_hash < b.user_hash) return -1;
					else return 0;
				})
			},

			createListFromItem: function (v) {
				var t = this;
				Mousetrap.pause();

				var $createDialog = dialog.open({
					className: 'create-list vex-theme-wireframe',
					message: t.g.tpls.createList({}),
					buttons: [
						{
							text: 'Create list',
							type: 'button',
							className: 'primary-button',
							click: function () {
								createList();
							}
						},
						{
							text: 'Cancel',
							type: 'button',
							click: function () {
								closeDialog();
							}
						}
					],
					afterClose: function () {
						Mousetrap.unpause();
					}
				});

				var $title = $createDialog.find('.text');

				// we don't like task. and question.
				var content = v.$('> .item .body').text();
				content = content || '';
				content = content.replace(/^\s*(bookmark)?\s*(task|question)\.\S+[\s\n\t]+/, '');

				$title.val(content.substr(0, 50)).select().focus()
					.on('keydown', function (e) {
						if (e.keyCode == keymap.enter) {
							createList();
							e.stopImmediatePropagation();
						}
					});
				$createDialog.on('click', '.move-children', function () {
					$(this).toggleClass('selected');
				})

				var createList = function () {
					if (t.syncer.needSync) {
						t.notyWarning('Please try again after the syncing completes', 1200);
						return;
					}

					var moveChildren = $createDialog.find('.move-children').is('.selected');

					var items = [], children = [], removed = [];
					if (moveChildren) {
						v.$('> .children > li').each(function () {
							var $t = $(this), v = $t.data('view');
							children.push(v.item.id);
							removed.push(v);

							$t.find('li').each(function () {
								var v = $(this).data('view');
								items.push(v.item.id);
								removed.push(v);
							});
						});
					}

					var title = $.trim($createDialog.find('.text').val()).substr(0, 50);

					ajax.post('create_list', {
						listId: t.list.id,
						listTitle: t.list.title.substr(0, 28),
						title: title,
						children: children,
						items: items
					}, {
						ok: function (res) {
							v.item.content = v.item.content + ' @{' + res.listId + '||' + title + '}@';

							if (moveChildren) {
								_.each(children, function (child) {
									delete t.itemViews[child];
								});
								_.each(items, function (item) {
									delete t.itemViews[item];
								});
								_.each(removed, function (v) {
									delete v.g;
									v.remove(true);
								});
								v.checkChildren(false);
							}

							v.refreshBody();
							v.dirty();
						}
					})

					closeDialog();
				}

				var closeDialog = function () {
					vex.close($createDialog.data('vex').id);
				}
			},

			existingListLink: function (v) {
				var t = this;
				t.showListDialog(false, {
					forLink: true
				});

				t.$listDialog.on('click', 'li', function () {
					var $t = $(this), title = $t.data('list-title'), id = $t.data('list-id');
					var listLink = ' @{' + id + '||' + title.substr(0, 32) + '}@';

					var isDict = t.$listDialog.find('.add-as-dict').is('.selected');
					if (isDict) {
						v.trigger('new-item-child', {
							content: 'dict: ' + listLink
						});

						// adding back link
						ajax.post('add_back_link', {
							listId: id,
							backLink: t.list.id,
							backLinkTitle: t.list.title.substr(0, 32),
							backLinkText: 'Dictionary link from - '
						}, {});
					} else {
						v.item.content = v.item.content + listLink;
						v.refreshBody();
						v.dirty();
					}

					vex.close(t.$listDialog.data('vex').id);
				}).on('click', '.add-as-dict', function () {
					var $t = $(this);
					$t.toggleClass('selected');
				});
			},

			notyWarning: function (message, delay) {
				utils.notyWarning(message, delay);
			},

			_beforeCopyOrMoveItems: function (views, proceed, action) {
				var t = this;
				if (t.syncer.needSync) {
					t.notyWarning('Please wait until the syncing completes.', 1200);
				} else {
					var lists = utils.getItem('list-data');
					var hasDataFromOtherList = _.some(lists, function (list, key) {
						if (key != t.list.id && !list.moved) {
							Mousetrap.pause();
							$(document).on('keydown.copy-or-move', function (e) {
								if (e.keyCode == keymap.enter) {
									proceed(lists);
									closeDialog();
								}
							})
							var $otherList = dialog.open({
								message: 'There is data from other list. Are you want to proceed the <strong>' + action + '</strong>?',
								buttons: [
									{
										text: 'Yes',
										type: 'button',
										className: 'primary-button',
										click: function () {
											proceed(lists);
											closeDialog();
										}
									},
									{
										text: 'No',
										type: 'button',
										click: function () {
											closeDialog();
										}
									}
								],
								afterClose: function () {
									Mousetrap.unpause();
								}
							});

							var closed = false;
							var closeDialog = function () {
								if (closed) return;
								vex.close($otherList.data('vex').id);
								closed = true;
								$(document).off('keydown.copy-or-move')
							}
							return true;
						}
					});

					if (!hasDataFromOtherList) {
						if (!views.length) {
							t.notyWarning('No items selectd. Use "w" or "shift+w" to select first.', 1200);
						} else {
							proceed();
						}
					}
				}
			},

			copyItemsTo: function (v, views) {
				var t = this, opts = {seq: 9999};
				t._beforeCopyOrMoveItems(views, function (lists) {
					var data = v.getExportData(views);

					v.importFromString(data, opts);

					if (lists) {
						_.each(lists, function (list, key) {
							if (key != t.list.id && !list.moved) {
								v.importFromString(list.clone, opts);
							}
						});
					}
				}, 'Copying');
			},

			getSelectionData: function (v, views) {
				var children = [], items = [];
				var foundConflict = _.some(views, function (view) {
					children.push(view.item.id);
					view.$('> .children li').each(function () {
						var $t = $(this), v = $t.data('view');
						items.push(v.item.id);
					});
					if (v && v.$el.parents().index(view.$el) != -1) {
						return true;
					}
				});

				return {
					conflict: foundConflict,
					children: children,
					items: items
				}
			},

			moveItemsTo: function (v, views) {
				var t = this;
				t._beforeCopyOrMoveItems(views, function (lists) {
					var selection = t.getSelectionData(v, views);

					if (selection.conflict) {
						t.notyWarning('Parent item can not be moved to it\'s child item.', 1200);
						return;
					}

					_.each(views, function (view) {
						v.$('> .children').append(view.$el);
					});

					if (lists) {
						_.each(lists, function (list, key) {
							if (key != t.list.id && !list.moved) {
								list.moved = true;
								selection.children = selection.children.concat(list.children);
								selection.items = selection.items.concat(list.items);
								delete list.children;
								delete list.items;
								delete list.clone;
							}
						});
						utils.setItem('list-data', lists);
					}

					t.syncer.sync(true, true, {
						action: 'move_items',
						children: selection.children,
						items: selection.items,
						parentItem: v.item.id
					})
				}, 'Moving');
			},

			/**
			 *
			 * @param preventClose
			 * @param opts:
			 *  forLink: boolean
			 */
			showListDialog: function (preventClose, opts) {
				var t = this;
				opts = opts || {};

				t.$listDialog = dialog.open({
					className: 'list-dialog vex-theme-wireframe',
					message: t.g.tpls.listDialog({
						forLink: opts.forLink
					}),
					overlayClosesOnClick: !preventClose,
					buttons: [],
					afterClose: function () {
						Mousetrap.unpause();
					},
					escapeButtonCloses: !preventClose
				});
				Mousetrap.pause();

				t.$listDialog.find('input').on('keydown', function (e) {
					if (e.keyCode == keymap.enter) {
						if (e.metaKey || e.altKey || e.ctrlKey) {
							t.$listDialog.find('.selected .new-tab').click();
							e.stopImmediatePropagation();
						}
					}
				});

				// __m1: usage
				t.g.setupSelectableList(t.$listDialog.find('.search-list'),
					t.$listDialog.find('ul'),
					t.$listDialog.closest('.vex'),
					20);

				var searchLists = function (term) {
					if (!opts.forLink) {
						var $new = t.$listDialog.find('.create-new');
					}
					var $ul = t.$listDialog.find('ul');

					ajax.post('search_list', {
						term: term
					}, {
						ok: function (res) {
							$ul.children(':not(.create-new)')
								.remove();
							$ul.append(t.g.tpls.listResult({
								forLink: opts.forLink,
								list: res
							}));

							if (!opts.forLink) {
								$new.removeClass('selected');
								if (res.length) {
									$new.next().addClass('selected');
								} else {
									$new.addClass('selected');
								}
							} else {
								$ul.children(':eq(0)').addClass('selected');
							}
						}
					})

				}

				utils.fixVexRepainBug();

				t.$listDialog
					.find('.search-list')
					.focus()
					.data('prev', '')
					.on('keyup', _.debounce(function () {
						var $t = t.$listDialog.find('.search-list'),
							val = $t.val(), prev = $t.data('prev'),
							$new = t.$listDialog.find('.create-new');
						if (prev != val) {
							if (val) {
								$new.find('.title').text(val);
								$new.find('a').attr('href', '/app/list/new?title='
									+ encodeURIComponent(val));
							} else {
								$new.find('.title').text('未命名文档');
								$new.find('a').attr('href', '/app/list/new');
							}
							searchLists(val);

							$t.data('prev', val);
						}
					}, 200));
				searchLists('');
			},

			openLists: function (preventClose) {
				var t = this;
				t.showListDialog(preventClose);

				t.$listDialog
					.on('click', '.new-tab', function (e) {
						$(this).parent().attr('target', '_blank');
						e.stopPropagation();
						setTimeout(function () {
							if (t.g.list) {
								vex.close(t.$listDialog.data('vex').id);
							}
						}, 0);
					})
					.on('click', 'li', function (e) {
						$(this).find('a').get(0).click();
					});

			},

			sortItemViews: function (itemViews) {
				itemViews.sort(function (a, b) {
					if (a.item.seq > b.item.seq) return 1;
					else if (a.item.seq < b.item.seq) return -1;
					else return 0;
				})
			}

		});

		return ItemList;
	});
