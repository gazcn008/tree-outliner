define(['ajax', 'jquery', 'utils', 'underscore',
		'backbone', 'mousetrap', 'keymap', 'noty', 'vex-loader',
		'vex'],
	function (ajax, $, utils, _,
			  Backbone, _Mousetrap, keymap, noty, dialog,
			  vex,
			  _undefined) {

		var Mousetrap = {
			_keys: {},
			_groups: _undefined,
			setKeyEntry: function (keys, descr) {
				if (!(keys instanceof Array)) {
					keys = [keys];
				}

				if (descr) {
					this._keys[keys[0]] = {
						keys: keys,
						descr: descr
					};
				}
			},
			bind: function (keys, callback, descr, group, seq) {
				this.setKeyEntry(keys, descr, group, seq);
				_Mousetrap.bind(keys, callback);
				return this;
			},
			bindGlobal: function (keys, callback, descr, group, seq) {
				this.setKeyEntry(keys, descr, group, seq);
				_Mousetrap.bindGlobal(keys, callback);
				return this;
			},
			pause: function () {
				_Mousetrap.pause();
			},
			unpause: function () {
				_Mousetrap.unpause();
			}
		};

		Mousetrap._groups = [{
			title: '基础操作',
			keys: ['a a', 'f1', 'e', 'a g', '/', 'meta+p', 'meta+f', '`', 'space', 'z', 'escape', 'g']
		}, {
			title: '修改 Item',
			keys: ['a c', 'a m', 'alt+;']
		}, {
			title: 'Item 节点创建',
			keys: ['meta+d', 'meta+s', 'enter', 'shift+enter']
		}, {
			title: 'Item 节点移动',
			keys: ['meta+i', 'meta+k', 'meta+j', 'meta+l']
		}, {
			title: '光标移动',
			keys: ['i', 'k', 'j', 'l']
		}, {
			title: 'Item 节点选择',
			keys: ['w', 'a w', 'shift+i', 'shift+k', 'shift+j', 'shift+l']
		}, {
			title: '文档列表管理',
			keys: ['a a', 'a e', 'a d']
		}, {
			title: '控制内容的展开 / 缩起',
			keys: ['d', 'f', 'r', 's', '1', '2', '3', '4', 'meta+1', 'meta+2', 'meta+3', 'meta+4']
		}, {
			title: '快捷 Tag 操作',
			keys: ['shift+/', 'shift+1', 'space']
		}, {
			title: '其他操作',
			keys: ['c', 'v', 'a r']
		}]

		/*
		 navigates:
		 jikl
		 expand/collapse:
		 sedf
		 create:
		 meta(or ctrl?) + sedf
		 move:
		 meta(or ctrl?) + jikl

		 , search nav
		 . search item
		 / long command

		 */
		// all key bindings
		// __logic: * keyManager
		var g;
		var keyManager = {
			handler: function (e, callbacks, type) {
				var target = g.hoveredTarget(null, null, type == 'navigate' ? true : false);
				if (target.type in callbacks) {
					var view = target.$el.data('view');
					callbacks[target.type](view);
					e && e.preventDefault && e.preventDefault();
					return;
				}
				if (callbacks.any) {
					callbacks.any();
					e && e.preventDefault && e.preventDefault();
				}
			},

			expandingHandler: function (level) {
				this.handler(null, {
					item: function (v) {
						var $prevs = v.$el;
						for (var i = 0; i <= 4; i++) {
							$prevs.each(function () {
								$(this).data('view').onToggleExpanded(null, level > i);
							});
							$prevs = $prevs.find('> .children > li');
						}
					}
				})
			},

			expandingTopHandler: function (level, e) {
				var $prevs = $('#main-list > li')

				for (var i = 0; i <= 4; i++) {
					$prevs.each(function () {
						$(this).data('view').onToggleExpanded(null, level > i);
					});
					$prevs = $prevs.find('> .children > li');
				}

				e && e.preventDefault();
			},

			// __m19: timerHanlder()
			// type: 'rest' || undefined
			timerHandler: function (e, time, type) {
//                if (type == 'rest') {
//                    utils.openTimer(time, {
//                        userHash: g.userHash,
//                        itemId: -1,
//                        title: 'Quick task',
//                        listId: g.list.list.id,
//                        listTitle: 'Task a rest'
//                    })
//                } else {
//                    this.handler(e, {
//                        item: function (v) {
//                            var $item = v.$('>.item .body').clone();
//                            $item.find('[data-tag="bookmark"]').remove().end()
//                                .find('[tag-group="task."]').remove().end()
//                                .find('[tag-group="question."]').remove();
//
//                            utils.openTimer(time, {
//                                userHash: g.userHash,
//                                itemId: v.item.id,
//                                title: $item.text(),
//                                listId: g.list && g.list.list.id,
//                                listTitle: g.list && g.list.list.title
//                            });
//                        }
//                    });
//                }
			},

			// __m19: timeSpentHandler()
			timeSpentHandler: function (e) {
				this.handler(e, {
					item: function (v) {
						var title = v.$('> .item .body').text();
						title = title || '';
						var timeSpent = prompt('Please enter the time spent for the item: ' + title.substr(0, 20)
							+ (title.length > 20 ? '...' : ''),
							utils.formatDuration(v.item.task_spent * 10));

						if (timeSpent !== null) {
							var time = utils.fromDurationText(timeSpent);
							time = time || 0;
							v.item.task_spent = time / 600000;
							v.dirty();
						}
					}
				})
			},
			moveHandler: function (e, direction) {
				this.handler(e, {
					item: function (v) {

						if (direction == 'right') {
							var event = 'move-child';
						} else if (direction == 'left') {
							event = 'move-parent'
						}

						var editor = g.getCurrentEditor();
						if (editor) {
							editor.trigger(event);
							if (g.focusOnEditor()) return;
						}

						var items = g.getSelectedItems('same-level');
						if (items) {
							if (items.length) {
								// __event: item.move-up
								v.trigger(event, items);
							} else {
								v.trigger(event);
							}
						}
					}
				})
			},
			list: null,
			selectWarning: null,
			_selectMode: false,
			_autoSelect: false,

			_prevSelectDir: '',
			_initSelectedItem: null,

			toggleSelectMode: function (view, disabled) {
				var t = this;
				if (disabled) {
					t._selectMode = false;
					t._autoSelect = false;
					t._prevSelectDir = '';
					g.clearSelectedItems();
					t.selectWarning && t.selectWarning.close();
					t._initSelectedItem = null;
					return;
				}

				t._selectMode = !t._selectMode;
				t._initSelectedItem = view;
				if (t._selectMode) {
					if (keyManager.selectWarning && !keyManager.selectWarning.closed)
						return;

					keyManager.selectWarning = noty({
						text: 'Now is in select mode, press <W> to exit.',
						layout: 'bottomRight'
					});
				} else {
					keyManager.selectWarning && keyManager.selectWarning.close();
					t._prevSelectDir = '';
					t._initSelectedItem = null;
				}
			},
			trySelect: function (curr, prev, direction) {
				var t = this;
				if (t._selectMode) {
					if (!t._prevSelectDir) t._prevSelectDir = direction;
					if (prev == t._initSelectedItem) {
						t._prevSelectDir = direction;
					} else if (curr == t._initSelectedItem) {
						t._autoSelect = false;
					}
//						console.log(direction + ':' + t._prevSelectDir);
					if ((direction + ':' + t._prevSelectDir) in
						{'up:down': 1, 'down:up': 1, 'parent:child': 1, 'child:parent': 1}) {
						prev.toggleSelect();
					} else {
						curr.toggleSelect();
						t._autoSelect = true;
					}
				}
			},
			trySelectTag: function (e) {
				var $tagInput = g.list.$tagInput;
				if ($tagInput.is(':visible')) {
					var $action = $tagInput
						.find('[data-shortcut=' + String.fromCharCode(e.keyCode).toLowerCase() + ']:visible')
						.click();
					if ($action.length) {
						e.preventDefault();
						e.stopImmediatePropagation();
					}
					return true;
				}
				return false;
			},
			_rangeSelectStatus: null,
			rangeSelect: function (curr, prev) {
				var t = this;
				if (t._rangeSelectStatus === null) {
					t._rangeSelectStatus = curr.toggleSelect();
				} else {
					curr.toggleSelect(t._rangeSelectStatus);
				}
				prev && prev.toggleSelect(t._rangeSelectStatus);
			},
			stopRangeSelect: function () {
				this._rangeSelectStatus = null;
			},
			parseEvent: function (e) {
				e = e || {};
				e.preventDefault = e.preventDefault || function () {
					};
				return e;
			},
			taskPointHandler: function (e, val) {
				keyManager.handler(e, {
					item: function (v) {
						v.setTaskPoints(val)
					}
				})
			},

			tryConvertTags: function (e, editor) {
				editor.tryConvertTags(e);
			},

			_dialog: null,
			openHelpDialog: function () {
				Mousetrap.pause();
				var t = this;
				t._dialog = dialog.open({
					className: 'help-dialog vex-theme-wireframe',
					message: g.tpls.helpDialog({
						keys: Mousetrap._keys,
						groups: Mousetrap._groups
					}),
					buttons: [],
					afterClose: function () {
						Mousetrap.unpause();
					}
				});

				utils.fixVexRepainBug();
			}
		}


		return {
			keyManagerHandlers: keyManager,
			setup: function (global) {
				g = global;


				// __logic: mousetrap
				;
				(function (km) {
					// prevent meta+s to popup the save dialog
					$(document).keydown(function (e) {
						if (e.keyCode == keymap.s && (e.ctrlKey || e.metaKey)) {
							e.preventDefault();
						}
					})

					Mousetrap


						.bind(['a a'], function (e) {
								g.list.openLists();
								e.preventDefault();
							},
							'打开文档列表对话框, 可以定位到另一个文档或新建一个文档',
							'Bring up the list dialog so that you can quickly navigate to ' +
							'another list.')

						.bind(['a e'], function (e) {
								km.handler(e, {
									item: function (v) {
										g.list.existingListLink(v);
									}
								})
							},
							'打开文档列表对话框, 选中之后可以创建一个指向该文档的快捷链接',
							'Open the list dialog so that you can create a link' +
							' to that list. Tick add as dictionary if you' +
							' would like to add the list as a dictionary.')

						.bind(['a d'], function (e) {
								km.handler(e, {
									item: function (v) {
										g.list.createListFromItem(v);
									}
								})
							},
							'把选中的一个 Item 转换成一个新的文档, 如果你不想移动现有的子 Item, 则取消 "移动子节点" 的选项',
							'To extract an item into a new list.' +
							' Uncheck "Move items" if you don\'t want to' +
							' move the children items to the new list.')

						.bind(['a m', 'm'], function (e) {
								km.handler(e, {
									item: function (v) {
										g.list.moveItemsTo(v, g.getSelectedItems());
									}
								});
							}, '移动选中的 Items 到当前的目标 Item 下',
							'Move selected items. They could be from the same' +
							' list or from another list from *the same origin*.')

						.bind(['a c'], function (e) {
								km.handler(e, {
									item: function (v) {
										g.list.copyItemsTo(v, g.getSelectedItems());
									}
								});
							},
							'复制选中的 Items 到当前的目标 Item 下',
							'Copy selected items. The selected items could also ' +
							'come from the other list of *the same origin*.')

						.bind(['1', 'shift+meta+1'], function (e) {
								km.expandingHandler(1);
							},
							'收起当前 Item 到它的第一级',
							'Toggle the current item\'s expanding status ' +
							'to show its 1st level.')

						.bind(['2', 'shift+meta+2'], function (e) {
								km.expandingHandler(2);
							},
							'收起当前 Item 到它的第二级',
							'Toggle the current item\'s expanding status ' +
							'to show its 2nd level')

						.bind(['3', 'shift+meta+3'], function (e) {
								km.expandingHandler(3);
							},
							'收起当前 Item 到它的第三级',
							'Toggle the current item\'s expanding status ' +
							'to show its 3rd level.')

						.bind(['4', 'shift+meta+4'], function (e) {
								km.expandingHandler(4);
							},
							'收起当前 Item 到它的第四级',
							'Toggle the current item\'s expanding status ' +
							'to show its 4th level.')

						.bind(['g'], function (e) {
							km.handler(e, {
								item: function (v) {
									window.syncer.sync(true);
								}
							})
						}, '立即同步文档')

						.bind(['t'], function (e) {
							km.handler(e, {
								item: function (v) {
									g.scheduler.show(v.item, {
										onDirty: function() {
											v.toggleIndexed(true);
										}
									});
								}
							})
						})

						.bind(['shift+/'], function (e) {
								km.handler(e, {
									item: function (v) {
										if (v) {
											v.toggleQuestionTag();
										}
									}
								});
							},
							'快速插入 question 标签',
							"Toggle the question tag inside an item.")

						// __m19: enter the time spent
						.bind(['shift+1'], function (e) {
								km.handler(e, {
									item: function (v) {
										if (v) {
											v.toggleTaskTag();
										}
									}
								});
							},
							'快速插入 task 标签',
							"To toggle the item as a task item.")

						.bind(['a r'], function (e) {
								km.handler(e, {
									item: function (v) {
										if (v) {
											var c = v.$el.find('> .item .body').text().split('\n').join(' ');
											c = c ? '|' + c.replace(/\}/g, '').substr(0, 40) : ''

											prompt('请复制以下标记, 粘贴到编辑内容即可创建一个节点内链',
												' #{' + v.item.id + c + '}# ');
										}
									}
								});
							},
							'打开 Item 的 id 对话框, 方便创建 Item 的文档内链',
							"Popup the dialog containing the item's inner link, " +
							"which you can paste to another item to create a link " +
							"to the item.")

						.bind('z', function (e) {
							km.handler(e, {
								item: function (v) {
									if (v) {
										v.shareItem();
									}
								}
							});
						}, '打开 Item 的分享内容对话框')

						// __logic: keys - edit
						.bind(['e', 'f2'], function (e) {
								var target = g.hoveredTarget();
								e = km.parseEvent(e);
								if (target.type) {
									// r: edit an item
									var view = target.$el.data('view');
									switch (target.type) {
										case 'item':
											view.onEdit(e);
											break;
									}
								}
							},
							'编辑当前 Item',
							"Edit the current item.")
						.bind(['a g', 'del'], function (e) {
								km.handler(e, {
									item: function (v) {
										var items = g.getSelectedItems('same-level');
										if (items) {
											if (items.length) {
												Mousetrap.pause();

												$(document).on('keydown.delete', function (e) {
													if (e.keyCode == keymap.enter) {
														proceed();
														closeDialog();
													}
												})

												var $dialog = dialog.open({
													message: 'Are you sure of removing the selected items?',
													buttons: [
														{
															text: 'Yes',
															type: 'button',
															className: 'primary-button',
															click: function () {
																proceed();
																closeDialog();
															}
														},
														{
															text: 'No',
															type: 'button',
															click: function () {
																closeDialog();
															}
														}
													],
													afterClose: function () {
														Mousetrap.unpause();
														$(document).off('keydown.delete');
													}
												})

												var proceed = function () {
														items.forEach(function (item) {
															item.remove();
														})
													},
													closed = false,
													closeDialog = function () {
														if (closed) return;
														closed = true;
														vex.close($dialog.data('vex').id);
													};
											} else {
												v.remove();
											}
										}

									}
								})
							},
							'删除当前 (或选中的) Item(s)',
							"Remove the current item or the selected items.")
						.bind('a w', function (e) {
								keyManager.handler(e, {
									any: function () {
										km.toggleSelectMode(null, true);
									}
								})
							},
							'取消当前的所有选择',
							"Unselect all selected items.")

						.bind('h', function (e) {
								km.handler(e, {
									item: function (v) {
										v.toggleHeading();
									}
								})
							},
							'设置为标题',
							"Make the item as heading 1.")
						// .bind('h 2', function (e) {
						// 		km.handler(e, {
						// 			item: function (v) {
						// 				v.itemStyle('h2');
						// 			}
						// 		})
						// 	},
						// 	'设置为标题 2',
						// 	"Make the item as heading 2.")
						// .bind('h 3', function (e) {
						// 		km.handler(e, {
						// 			item: function (v) {
						// 				v.itemStyle('h3');
						// 			}
						// 		})
						// 	},
						// 	'设置为标题 3',
						// 	"Make the item as heading 3."
						// )
						// .bind('h 4', function (e) {
						// 		km.handler(e, {
						// 			item: function (v) {
						// 				v.itemStyle('h4');
						// 			}
						// 		})
						// 	},
						// 	'设置为标题 4',
						// 	"Make the item as heading 4.")
						// .bind('h 0', function (e) {
						// 		km.handler(e, {
						// 			item: function (v) {
						// 				v.itemStyle('');
						// 			}
						// 		})
						// 	},
						// 	'取消 Item 的标题样式',
						// 	"Remove the heading style from the item.")

						// __m8: . to search item
						.bind(['/'], function (e) {
								g.list.$sidebar.attr('data-tab-selected', 'search');

								g.list.$splitter.trigger('resume');

								$(g.list.$searchBox).focus().select();
								e.preventDefault();
							},
							'搜索 Item',
							'Focus on the item search box')

						.bindGlobal(['meta+o', 'ctrl+o'], function (e) {
							$('#list-actions .open-sub-content').trigger('click');

							e.preventDefault();
						})

						.bindGlobal(['meta+shift+o', 'ctrl+shift+o'], function (e) {
							$('#list-actions .replace-sub-content').trigger('click');

							e.preventDefault();
						})

						.bindGlobal(['meta+\\', 'ctrl+\\'], function (e) {
							$('#list-actions .split-doc').trigger('click');

							e.preventDefault();
						})

						.bindGlobal(['meta+shift+\\', 'ctrl+shift+\\'], function (e) {
							$('#close-sub-content').trigger('click');

							e.preventDefault();
						})

						.bindGlobal(['meta+p', 'ctrl+p'], function (e) {
							g.list.$splitter.trigger('resume');
							g.list.$listSearchBox.focus().select();
							e.preventDefault();
						}, '快速搜索文档')

						.bindGlobal(['meta+f', 'ctrl+f'], function (e) {
								g.list.$splitter.trigger('resume');
								$(g.list.$searchBox).focus().select();
								e.preventDefault();
							},
							'搜索 Item',
							'Focus on the item search box')

						.bind(['`'], function (e) {
								km.handler(e, {
									item: function (v) {
										$('#list-actions .toggle-learning-mode').click();
									}
								})
							},
							'切换学习模式')

						// 暂时使用 ` 来作为 learning mode 的切换热键, 本代码先保留
						// .bind(['`'], function (e) {
						// 		km.handler(e, {
						// 			item: function (v) {
						// 				v.focusOnItem();
						// 			}
						// 		})
						// 	},
						// 	'进入专注模式',
						// 	"Toggle focus mode. When in focus mode, only the current item will be shown, the other " +
						// 	"item will appear transparently.")



						.bind('space', function (e) {
								km.handler(e, {
									item: function (v) {
										v.onToggleTaskStatus();
									}
								})
							},
							'快速切换 task 或 question 的标签状态',
							"Toggle the first task or question tag of the item. If no task or question tag is on the " +
							"current item, new task.todo tag will be added.")

						// __m28: key: split
						.bindGlobal(['alt+;', 'meta+;', 'ctrl+;'], function (e) {
								km.handler(e, {
									item: function (v) {

										var CON_maxSplit = 20;
										var editor = g.getCurrentEditor();
										if (editor) {
											editor.trigger('split-item');
										} else {
											var sep = prompt('Enter separator carefully! (separator is in form of a regexp)');
											if (sep) {
												var cont = v.item.content;
												var rgx = new RegExp('(.*?)' + sep + '');

												var len = 0;

												var m = rgx.exec(cont);

												while (m) {
													cont = cont.replace(rgx, '');
													var newVal = $.trim(m[1] + (m[2] ? m[2] : ''));
													if ($.trim(newVal)) {
														v.trigger('new-item-prev', {
															content: newVal
														});
													}

													if (len++ >= CON_maxSplit) {
														noty({
															type: 'warning',
															text: 'The result is too huge, the last node will be concated',
															timeout: 2000,
															layout: 'bottomRight'
														});
														break;
													}

													m = rgx.exec(cont);
												}

												if ($.trim(cont)) {
													v.trigger('new-item-prev', {
														content: cont
													});
												}
											}
										}
									}
								})
							},
							'利用 正则表达式 来划分 Item, 将其打散成一系列 Item',
							"To split the item's content. You can write a regular expression to split it, " +
							"and the first group of the regular expression will be retained.")

						.bindGlobal(['meta+1', 'alt+1', 'ctrl+1'], function (e) {
							km.expandingTopHandler(1, e);
						}, '文档顶层收缩至第一层')

						.bindGlobal(['meta+2', 'alt+2', 'ctrl+2'], function (e) {
							km.expandingTopHandler(2, e);
						}, '文档顶层收缩至第二层')

						.bindGlobal(['meta+3', 'alt+3', 'ctrl+3'], function (e) {
							km.expandingTopHandler(3, e);
						}, '文档顶层收缩至第三层')

						.bindGlobal(['meta+4', 'alt+4', 'ctrl+4'], function (e) {
							km.expandingTopHandler(4, e);
						}, '文档顶层收缩至第四层')

						// __m29: __logic: keys - create
						.bindGlobal(['meta+d', 'alt+d', 'ctrl+d'], function (e) {
								var editor = g.getCurrentEditor();
								if (!editor && km.trySelectTag(e)) return;
								var target = g.hoveredTarget();
								if (target.type) {
									e = km.parseEvent(e);
									var view = target.$el.data('view');
									switch (target.type) {
										case 'item':
											if (editor) {
												editor.onInsertChild(e);
												e.preventDefault();
												return;
											}

											view.onInsertChild(e);
											e.preventDefault();
											break;
									}
								}
							},
							'新建子 Item 节点',
							"Create a new children item of the current item.")

						.bindGlobal(['escape'], function (e) {
								km.handler(e, {
									item: function (v) {
										v.hide$tagInput(true);
										g.scheduler.hide();
									}
								})
							},
							'关闭对话框',
							"Hide the tag input inline dialog or exit edit mode.")
						// __m29: key:
						.bindGlobal(['enter'], function (e) {
								var editor = g.getCurrentEditor();
								if (!editor && km.trySelectTag(e)) return;
								km.handler(e, {
									item: function (v) {
										if (g.list.$tagInput.is(':visible')) return;
										var $prompt = $('body > .prompt-dialog');
										if ($prompt.is(':visible')) {
											vex.close($prompt.data('vex').id);
											return;
										}

										if (editor) {
											var content = v.item.content;
											if (content.indexOf('\n') == -1) {
												editor.trigger('new-item-next');
											}
											return;
										}

										v.trigger('new-item-next');
									}
								})
							},
							'在下方新建同级 Item 节点',
							"Create a new sibling item following the current item.")
						.bindGlobal(['shift+enter'], function (e) {

								var editor = g.getCurrentEditor();
								if (!editor && km.trySelectTag(e)) return;
								km.handler(e, {
									item: function (v) {
										if (editor) {
											editor.trigger('new-item-prev');
											return;
										}

										v.trigger('new-item-prev');
									}
								})
							},
							'在上方新建同级 Item 节点',
							"Create a new sibling item above the current item.")
						.bindGlobal(['meta+s', 'alt+s', 'ctrl+s'], function (e) {

								var editor = g.getCurrentEditor();
								if (!editor && km.trySelectTag(e)) return;
								km.handler(e, {
									item: function (v) {
										var editor = g.getCurrentEditor();
										if (editor) {
											editor.trigger('new-item-parent');
											return;
										}

										var items = g.getSelectedItems('same-level');
										if (items) {
											if (items.length) {
												v.trigger('new-item-parent', items);
												km.toggleSelectMode(null, true);
											} else {
												v.trigger('new-item-parent');
											}
										}
									}
								})
							},
							'新建父级 Item 节点, 将选中 (或当前) 的 Item(s) 建立群组',
							"Create a new parent item that holds the current item or the selected item(s).")

						// __logic: keys - move
						.bindGlobal(['meta+i', 'ctrl+i', 'meta+up', 'ctrl+up'], function (e) {
								keyManager.handler(e, {
									item: function (v) {
										var editor = g.getCurrentEditor();
										if (editor) {
											editor.trigger('move-up');
											if (g.focusOnEditor()) return;
										}

										var items = g.getSelectedItems('same-level');
										if (items) {
											if (items.length) {
												// __event: item.move-up
												v.trigger('move-up', items);
											} else {
												v.trigger('move-up');
											}
										}
									}
								})
							},
							'向上移动 Item(s)',
							"Move up the current item or selected item(s).")

						.bind('q', function (e) {
							keyManager.handler(e, {
								item: function (v) {
									v.toggleIndexed();
								}
							})
						})

						.bindGlobal(['meta+k', 'ctrl+k', 'meta+down', 'ctrl+down'], function (e) {
								km.handler(e, {
									item: function (v) {
										var editor = g.getCurrentEditor();
										if (editor) {
											editor.trigger('move-down');
											if (g.focusOnEditor()) return;
										}

										var items = g.getSelectedItems('same-level');
										if (items) {
											if (items.length) {
												// __event: item.move-up
												v.trigger('move-down', items);
											} else {
												v.trigger('move-down');
											}
										}
									}
								})
							},
							'向下移动 Item(s)',
							"Move down the current item or selected item(s).")

						.bindGlobal(['meta+j', 'ctrl+j', 'shift+tab'], function (e) {
								km.moveHandler(e, 'left');
							},
							'反缩进 Item(s)',
							"Outndent (move left) the current item or selected item(s).")

						.bindGlobal(['meta+l', 'ctrl+l', 'tab'], function (e) {
								km.moveHandler(e, 'right');
							},
							'缩进 Item(s)',
							"Indent (move right) the current item or selected item(s).")
						.bind(['w'], function (e) {
								keyManager.handler(e, {
									item: function (v) {
										v.toggleSelect();
									}
								})
							},
							'切换当前 Item 的选择状态',
							"Toggle the selection status of an item.")

						// __logic: keys -- navigation
						.bind(['i', 'up'], function (e) {
								keyManager.handler(e, {
									'item': function (v) {
										if (keyManager.list) {
											var $prev = v.getPrevItem();
											$prev && keyManager.list.hoverOnItem($prev, true);
										}
									}
								}, 'navigate')
							},
							'向上移动光标',
							"Move your cursor up, in another words, set the item above as the current item.")
						.bind(['k', 'down'], function (e) {
								keyManager.handler(e, {
									item: function (v) {
										if (keyManager.list) {
											var $next = v.getNextItem();
											$next && keyManager.list.hoverOnItem($next, true);
										}
									}
								}, 'navigate')
							},
							'向下移动光标',
							"Move your cursor down, in another words, set the following item as the current item.")
						.bind(['j', 'left'], function (e) {
								keyManager.handler(e, {
									item: function (v) {
										if (keyManager.list) {
											var $parent = v.getParent();
											if ($parent) {
												v.rememberChild();
												keyManager.list.hoverOnItem($parent, true);
											}
										}
									}
								}, 'navigate')
							},
							'向左移动光标',
							"Move your cursor left, in another words, set the parent item as the current item.")
						.bind(['l', 'right'], function (e) {
								e = km.parseEvent(e);
								keyManager.handler(e, {
									item: function (v) {
										if (keyManager.list) {
											var $child = v.recallChild();
											if ($child) {
												keyManager.list.hoverOnItem($child, true);
											}
										}
									}
								}, 'navigate')
							},
							'向右移动光标',
							"Move your cursor down, in another words, set the most recent child item as the current item.")
						.bind(['shift+i', 'shift+up'], function (e) {
								keyManager.handler(e, {
									'item': function (v) {
										if (keyManager.list) {
											var $prev = v.getPrevItem();
											if ($prev) {
												keyManager.list.hoverOnItem($prev, true);
												km.rangeSelect(v, $prev.data('view'));
											} else {
												km.rangeSelect(v);
											}
										}
									}
								})
							},
							'向上批量选择 Item(s)',
							"Toggle the current item's selection status, and continue to the item above.")
						.bind(['shift+k', 'shift+down'], function (e) {
								keyManager.handler(e, {
									item: function (v) {
										if (keyManager.list) {
											if (keyManager.list) {
												var $next = v.getNextItem();
												if ($next) {
													keyManager.list.hoverOnItem($next, true);
													km.rangeSelect(v, $next.data('view'));
												} else {
													km.rangeSelect(v);
												}
											}
										}
									}
								})
							},
							'向下批量选择 Item(s)',
							"Toggle the current item's selection status, and continue to the following item.")
						.bind(['shift+j', 'shift+left'], function (e) {
								keyManager.handler(e, {
									item: function (v) {
										if (keyManager.list) {
											var $parent = v.getParent();
											if ($parent) {
												v.rememberChild();
												keyManager.list.hoverOnItem($parent, true);
											}

											var $siblings = v.$el.siblings().each(function () {
												km.rangeSelect(v, $(this).data('view'));
											});
											if (!$siblings.length) {
												km.rangeSelect(v);
											}
										}
									}
								})
							},
							'批量选择同级 Item(s)',
							"Toggle the selection status of the sibling items")
						.bind(['shift+l', 'shift+right'], function (e) {
								keyManager.handler(e, {
									item: function (v) {
										if (keyManager.list) {
											var $child = v.recallChild();
											if ($child) {
												var ch = $child.data('view');
												keyManager.list.hoverOnItem($child, true);
												var $children = v.$('> .children > li').each(function () {
													km.rangeSelect(ch, $(this).data('view'));
												});
												if (!$children.length) {
													km.rangeSelect(ch);
												}
											}
										}
									}
								})
							},
							'批量选择子级 Item(s)',
							"Toggle the selection status of the children items of the current item.")

						.bind('shift', function (e) {
							km.stopRangeSelect();
						})

						// __logic: keys - expanding
						.bind('d', function (e) {
								km.handler(e, {
									item: function (v) {
										//__event: item.expand-child
										v.trigger('toggle-child');
									}
								});
							},
							'展缩子级节点',
							"Toggle expanding status of the children items.")

						.bind('f', function (e, opts) {
								km.handler(e, {
									item: function (v) {
										//__event: item.expand-item
										v.trigger('toggle-item');
									}
								})
							},
							'展缩当前节点',
							"Toggle expanding status of the current item.")

						.bind('r', function (e) {
								km.handler(e, {
									item: function (v) {
										//__event: item.expand-item
										v.trigger('toggle-siblings');
									}
								})
							},
							'展缩同级节点',
							"Toggle expanding status of the sibling items.")

						.bind('s', function (e) {
								km.handler(e, {
									item: function (v) {
										//__event: item.expand-item
										v.trigger('toggle-parent');
									}
								})
							},
							'缩起父级节点',
							"Toggle expanding status of the parent item.")

						// __m22: key: c & v
						.bind('c', function (e) {
								km.handler(e, {
									item: function (v) {
										var items = g.getSelectedItems('same-level');
										if (items) {
											if (items.length) {
												v.trigger('export-data', items);
											} else {
												// export item & sub items to text
												v.trigger('export-data');
											}
										}
									}
								})
							},
							'打开内容导出对话框, 方便你使用当前内容作为模板使用',
							"Export the data in text, so that you can import to another list or save locally.")

						.bind('v', function (e) {
								km.handler(e, {
									item: function (v) {
										v.trigger('import-data');
									}
								})
							},
							'导入内容',
							"Import items from exported data.")

						.bindGlobal('f1', function (e) {
								km.openHelpDialog();
							},
							'打开帮助对话框',
							"Open this help dialog.")

					;
				})(keyManager);

				console.log('keys - ', Mousetrap._keys);
			},
			setList: function (list) {
				keyManager.list = list;
			}
		}
	});
