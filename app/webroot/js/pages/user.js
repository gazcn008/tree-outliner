/*<MAIN_SCRIPT>*/

/*<DEV*/require(['js/main.js'], function() {/*DEV>*/
	require(['jquery', 'utils', 'text!tpls/common.tpl'], function($, utils, commonTplStr) {
		var commonTpls = utils.parseTpls(commonTplStr);
		var $body = $('body');
		var $user = $('#user');
		
		if (pv.errors) {
			pv.errors.forEach(function(e) {
				$user.after('<div class="error">' + e.message + '</div>');
			});
		} 
		if (pv.success) {
			pv.success.forEach(function(e) {
				$user.after('<div class="success">' + e.message + '</div>');
			})
		}
	})
    /*<DEV*/});/*DEV>*/