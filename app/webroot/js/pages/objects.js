/*<MAIN_SCRIPT>*/

/*<DEV*/
require(['/js/main.js'], function() {
    /*DEV>*/
    require(['utils', 'jquery', 'underscore', 'text!tpls/objects.tpl', 'md5',
            'window-messenger', 'backbone', 'ajax', 'jquery-ui',
        'obj-comps/my-object', 'obj-comps/object-syncer', 'obj-comps/obj-views', 'obj-comps/local-data'],
        function(utils, $, _, tplStr, md5,
                 messener, Backbone, ajax, _jqueryUI,
                 MyObjectIniter, AjaxQueue, ViewsIniter, initLocalData,
                 undefined) {

            'use strict';

            ajax.skipDefault();
            var seq = 0;
            var _ERRORS = {

            };
            var errors = function(key) {
                if (!_ERRORS[key]) return 'error key: {' + key + '}';
                else return _ERRORS[key];
            }


            var CON_prefix = '_obj_',
                CON_prefixLen = CON_prefix.length,
                pageCache = {},
                itemMetaData,
                CON_maxItemMetaDataNum = 1000,
                CON_itemMetaDataKey = 'objects_meta_data';

            var App = function(ajaxQueue) {
                var t = this;
                t.ajaxQueue = ajaxQueue;
            };
            App.prototype = {
                clearAll: function() {
                    localStorage.clear();
                },
                getCache: function() {
                    itemMetaData = utils.getItem(CON_itemMetaDataKey, {
                        objs: []
                    });
                },
                setCache: function() {
                    utils.setItem(CON_itemMetaDataKey, itemMetaData);
                },
                lazySetCache: _.debounce(function() {
                    this.setCache();
                }, 500),
                get: function(id, raw, isRefresh) {
                    if (!id && !raw) return false;
                    if (id && pageCache[id] && !isRefresh) return pageCache[id];

                    if (!raw) {
                        raw = localStorage.getItem(CON_prefix + id);
                        if (!raw) return false;
                        try {
                            raw = JSON.parse(raw);
                        } catch (ex) {
                            return false;
                        }

                        if (!isRefresh) {
                            return pageCache[id] = new MyObject(raw);
                        } else {
                            var tmp = new MyObject(raw);
                            pageCache[id].d = tmp.d;
                            return pageCache[id];
                        }
                    } else {
                        id = raw.id;
                        if (!pageCache[id]) return pageCache[id] = new MyObject(raw);

                        var tmp = new MyObject(raw);
                        pageCache[id].d = tmp.d;
                        return pageCache[id];
                    }
                },
                _setQueue: [],
                _lazyTimer: null,
                _lazyInterval: 1000,
                lazyset: function(obj) {
                    var t = this;
                    clearInterval(t._lazyTimer);
                    t._lazyTimer = setInterval(function() {
                        var obj;
                        while (obj = t._setQueue.pop()) {
                            t.set(obj);
                        }
                    }, t._lazyInterval);
                },
                clearItems: function() {
                    var keys = [];
                    for (var i = 0; i < localStorage.length; i++) {
                        var key = localStorage.key(i);
                        if (key.substr(0, CON_prefixLen) == CON_prefix) {
                            keys.push(key);
                        }
                    }
                    _.each(keys, function(k) {
                        localStorage.removeItem(k);
                    });
                },
                saveAll: function() {
                    var t = this;
                    _.each(t._dirty, function(_tmp, id) {
                        t.save(t.get(id));
                    });
                },
                _dirty: {},
                set: function(obj, skipReduce, skipTouch) {
                    var t = this;
                    if (!obj) {
                        return false;
                    }

                    var id = obj.id && obj.id();
                    if (!id) {
                        return false;
                    }
                    t._dirty[id] = 1;

                    if (pageCache[id]) {
                        pageCache[id].d = obj.d;
                        obj = _.extend(pageCache[id], obj);
                    }

                    try {
                        var raw = obj.raw();
                        localStorage.setItem(CON_prefix + id, JSON.stringify(raw));
                    } catch (ex) {
                        console.log('problem found when setting - ', ex);
                        if (!skipReduce) {
                            this.reduceCache();
                            try {
                                this.set(obj, true);
                            } catch (ex) {
                                return false;
                            }
                        }
                    }

                    while (itemMetaData.objs.length > CON_maxItemMetaDataNum) {
                        itemMetaData.objs.pop();
                    }

                    var prevObj;
                    _.some(itemMetaData.objs, function(obj) {
                        if (!itemMetaData.objs || !itemMetaData.objs.id) return;
                        if (obj.id == id) {
                            prevObj = obj;
                            itemMetaData.objs.splice(idx, 1);
                            return true;
                        }
                    });

                    itemMetaData.objs.unshift(_.extend({}, itemMetaData[id], {
                        id: id,
                        touched: skipTouch ? 0 : new Date().getTime(),
                        updated: raw.updated
                    }));
//                    console.log(itemMetaData);
                    itemMetaData.objs.sort(function (a, b) {
                        if (!a && !b) return 0;
                        if (!a) return 1;
                        if (!b) return -1;
                        if (a.touched > b.touched) return -1;
                        if (a.touched < b.touched) return 1;
                        if (a.updated > b.updated) return -1;
                        if (a.updated < b.updated) return 1;
                        return 0;
                    });
//                    console.log(itemMetaData);
                    t.lazySetCache();

                    return obj;
                },
                removeCache: function(id) {
                    localStorage.removeItem(CON_prefix + id);
                },
                reduceCache: function() {
                    var removed = [], limit = localStorage.length / 2;
                    for (var i = 0; i < localStorage.length && removed.length < limit; i++) {
                        var key = localStorage.key(i);
                        if (key.substr(0, CON_prefixLen) == CON_prefix) {
                            removed.push(key);
                        }
                    }
                    _.each(removed, function(key) {
                        localStorage.removeItem(key);
                    });
                },
                session: {
                    clear: function() {
                        localStorage.setItem('obj-session', '{}');
                    },
                    clearItems: function(keyOrArray) {
                        var session = this.read();
                        var keys = typeof keyOrArray == 'string' ? [keyOrArray] : keyOrArray;

                        _.each(keys, function(k) {
                            delete session[k];
                        });
                        localStorage.setItem('obj-session', JSON.stringify(session));
                    },
                    read: function(keyOrArray) {
                        try {
                            var session = JSON.parse(localStorage.getItem('obj-session'));
                        } catch (ex) {
                            session = {};
                        }
                        if (!session) session = {};
                        if (!keyOrArray) return session;

                        if (typeof keyOrArray == 'string') {
                            return session[keyOrArray];
                        }

                        var ret = {}
                        _.each(keyOrArray, function(k) {
                            ret[k] = session[k];
                        });
                        return ret;
                    },
                    write: function(keyOrObj, value) {
                        var session = this.read();
                        var newVals = typeof keyOrObj == 'string' ? (({})[keyOrObj]=value) : keyOrObj;
                        _.extend(session, newVals);
                        localStorage.setItem('obj-session', JSON.stringify(session));
                    }
                },
                load: function(id, callback) {
                    var t = this;
                    if (!id || typeof id != 'string') {
                        console.log('id should be a nonnull string.');
                        callback && callback('no-id-set');
                        return;
                    }

                    t.ajaxQueue.load(id, function(raw) {
                        var obj = t.get(null, raw);
                        if (raw) {
                            obj = t.set(obj, false, true);
                            callback && callback(false, obj);
                        } else {
                            callback && callback('not-found');
                        }
                    });
                },
                diff: function(prevObj, currObj) {
                    var diff = {prev: {}, curr: {}}, found = false;
                    if (prevObj) {
                        for (var k in prevObj.d) {
                            if (currObj.d[k] !== prevObj.d[k]) {
                                diff.curr[k] = currObj.d[k];
                                diff.prev[k] = prevObj.d[k];
                                found = true;
                            }
                        }
                        for (var k in currObj.d) {
                            if (currObj.d[k] !== prevObj.d[k]) {
                                diff.curr[k] = currObj.d[k];
                                diff.prev[k] = prevObj.d[k];
                                found = true;
                            }
                        }
                    } else {
                        return _.extend({}, currObj);
                    }
                    if (found) return diff;
                    return false;
                },
                save: function(obj, callback) {
                    var t = this;
                    if (!obj) {
                        console.log('object is required by save().');
                        callback && callback('no-obj');
                        return;
                    }
                    var id = obj && obj.id();
                    if (!id || typeof id != 'string') {
                        console.log('id should be a non-null string.');
                        callback && callback('no-id');
                        return;
                    }

                    var raw = _.extend(obj.raw(), {id: id});
                    obj = t.set(obj);
                    t.ajaxQueue.save(id, raw, function(raw) {
                        if (raw) {
                            callback && callback(false, obj);
                        } else {
                            t.removeCache(obj.id);
                            callback && callback('save-error');
                        }
                    });
                },
                newObj: function(content) {
                    content = content || {};
                    content._owned_ = true;
                    content.id = this.id();
                    var n = new MyObject(content);
                    n.set();
                    return n;
                },
                id: function() {
                    return Math.floor(Math.random() * Math.pow(36, 4)).toString(36) + (seq++).toString(36) + '_' + pv.seq;
                },
                _listView: null,
                refresh: function() {
                    var t = this;
                    app.list();
                    t._listView.refresh();
                },
                list: function() {
                    var t = this;
                    if (!t._listView || t._listView.isRemoved()) {
                        t._listView = new ListView();
                    }
                    $body.append(t._listView.$el);
                },
                alias: function(alias, objToSet) {
                    if (arguments.length == 1) {
                        var objs = [];
                        _.each(itemMetaData.objs, function (obj) {
                            if (!obj || !obj.id) return;
                            if (objs.alias == alias)
                                objs.push(app.get(obj.id));
                        });
                        return objs;
                    } else {
                        _.some(itemMetaData.objs, function (obj) {
                            if (!obj || !obj.id) return;
                            if (objToSet.id() == obj.id) {
                                obj.alias = alias;
                                return true;
                            }
                        });
                    }
                },
                aliasStart: function(term) {
                    var objs = [], len = term.length;
                    _.each(itemMetaData.objs, function(obj) {
                        if (!obj || !obj.id) return;
                        if (objs.alias && objs.alias.substr(0, len) == term)
                            objs.push(app.get(obj.id));
                    });
                    return objs;
                },
                searchLocal: function(name, space, alias, founds) {
                    name = name || '';
                    space = space || '';
                    alias = alias || '';

                    var match = function(obj) {
                        if (obj.id in founds) return false;
                        obj.alias = obj.alias || '';
                        obj.name = obj.name || '';
                        obj.space = obj.space || '';
                        if (obj.name.substr(0, lenName) == name &&
                            obj.space.substr(0, lenSpace) == space &&
                            obj.alias.substr(0, lenAlias) == alias) {
                            founds[obj.id] = 1;

                            return true;
                        }
                        return false;
                    }

                    var lenName = name.length, lenSpace = space.length, lenAlias = alias.length;
                    var count = 0, results = [];

                    var itemsToRemvoe = [];
                    _.some(itemMetaData.objs, function(obj) {
                        if (!obj || !obj.id) return;
                        var o = app.get(obj.id);
                        if (!o) {
                            itemsToRemvoe.push(obj.id);
                            return;
                        }
                        var searchObj = {
                            id: obj.id,
                            name: o.d._name_,
                            space: o.d._space_,
                            descr: o.d._descr_
                        }

                        var res = match(searchObj)
                        if (res) results.push(searchObj);

                        if (results.length >= CON_searchNum) {
                            return true;;
                        }
                    });

                    if (results.length >= CON_searchNum) {
                        return results;
                    } else {
                        for (var i = 0; i < localStorage.length && results.length < CON_searchNum; i++) {
                            var key = localStorage.key(i);
                            if (key.substr(0, CON_prefixLen) == CON_prefix) {
                                var obj = JSON.parse(localStorage.getItem(key))
                                obj = {
                                    id: obj.id,
                                    name: obj._name_,
                                    space: obj._space_,
                                    descr: obj._descr_
                                }
                                var res = match(obj);
                                if (res) {
                                    results.push(obj);
                                }
                            }
                        }
                    }

                    return results;
                },
                searchRemote: function(name, space, offset, callback) {
                    var tks = token.getReqParam();

                    ajax.post('search_object', {
                        req: JSON.stringify({
                            name: name,
                            space: space,
                            tokens: tks.all,
                            inUse: tks.inUse,

                            offset: offset,
                            step: CON_searchNum
                        })
                    }, {
                        ok: function(rsp) {
                            var res = _.map(rsp, function(raw) {
                                app.get(null, raw).set(true);
                                return {
                                    name: raw._name_,
                                    space: raw._space_,
                                    id: raw.id,
                                    descr: raw._descr_
                                }
                            });
                            callback && callback(res);
                        }
                    })
                }
            }

            var CON_searchNum = 20;

            var Router = Backbone.Router.extend({
                routes: {
                    'list(/)': 'list',
                    'o/:id/:method(/*argument)': 'object',
                    'r/:id/:method(/*argument)': 'objectRenew',
                    '(/)': 'setup'
                },
                setup: function() {
                    setup();
                },
                list: function() {
                    setup(function() {
                        app.list();
                    });
                },
                refresh: function() {

                },
                callObjMethod: function(obj, method, args) {
                    if (typeof obj.d[method] == 'function') {
                        obj.d[method].apply(obj, args);
                    } else {
                        alert('The target method is not a callable function.');
                    }
                },
                objectRenew: function(objectId, method, arg) {
                    var t = this;
                    setup(function() {
                        t.loadObjectAndCall(objectId, method, arg);
                    });
                },
                object: function(objectId, method, arg) {
                    var t = this;

                    setup(function() {
                        var obj = app.get(objectId);
                        if (!obj) {
                            t.loadObjectAndCall(objectId, method, arg);
                        } else {
                            t.callObjMethod(obj, method, arg ? arg.split('/') : []);
                        }
                    });
                },
                loadObjectAndCall: function(objectId, method, arg) {
                    var t = this;
                    app.load(objectId, function(e, obj) {
                        if (e) {
                            alert('Cannot find the object with id#' + objectId);
                        } else {
                            t.callObjMethod(obj, method, arg ? arg.split('/') : []);
                        }
                    });
                }
            });

            var $body = $('body'), ListView, ObjectView, token, aliasManager, MyObject;

            var setup = function(callback) {
                $(function () {
                    var ajaxQueue = new AjaxQueue();
                    var app = window.app = new App(ajaxQueue);
                    app.getCache();
                    ViewsIniter.init(app, CON_searchNum, $body, errors);
                    ListView = ViewsIniter.ListView,
                        ObjectView = ViewsIniter.ObjectView;

                    var localData = initLocalData(app);
                    token = localData.token;
                    aliasManager = localData.aliasManager;

                    MyObjectIniter.init(app, errors);
                    MyObject = MyObjectIniter.MyObject;

                    console.log('--- app ready ---');

                    callback && callback()
                });
            }

            new Router();
            Backbone.history.start({
                pushState: true,
                root: '/objects/'
            });

        });
    /*<DEV*/
});
/*DEV>*/
