/*<MAIN_SCRIPT>*/

/*<DEV*/
require(['/js/main.js'], function() {
    /*DEV>*/
	require(['utils', 'jquery', 'underscore', 'text!tpls/popup.tpl', 'md5',
        'window-messenger', 'backbone'],
			function(utils, $, _, tplStr, md5,
                     messener, Backbone,
					undefined) {

                var tpls = utils.parseTpls(tplStr);
                var CON_smallest = 20000, CON_unitMax = 600000;;

                /**
                 * $el should be $('body')
                 */
                var Popup = Backbone.View.extend({
                    events: {
                        'mousemove': 'onMousemove',
                        'change .hour-graph': 'onHourGraphChanged',
                        'click .finish-restart': 'onFinishRetart',
                        'click [data-finish-restart]': 'onFinishRestartWithTime',
                        'click [data-take-rest]': 'onTaskRestWithTime',
                        'click .goto-window': 'onGotoList'
                    },
                    _info: {},
                    onGotoList: function(e) {
                        // need refactor this method
//                        utils.setAction(userHash, 'goto', {
//                            itemId: t._info.itemId
//                        });
                    },
                    onTaskRestWithTime: function(e) {
                        var time = $(e.currentTarget).data('take-rest'),
                            t = this,
                            info = t._info;
                        time = utils.fromDurationText(time);
                        if ($(e.currentTarget).is('.cancel')) {
                            utils.setAction(t.userHash, 'cancel-restart', t.taskActionParams(info));
                            t.recordTimeSpentItem(info, 'cancel');
                        } else {
                            utils.setAction(t.userHash, 'finish-restart', t.taskActionParams(info));
                            t.recordTimeSpentItem(info, 'finish');
                        }
                        var now = new Date().getTime();
                        info.start = now;
                        info.time = time;
                        info.itemId = -1;
                        info.title = 'Quick task';
                        info.updated = now;
                        utils.setItem('task-timer', info);
                        utils.setAction(t.userHash, 'rest');
                        t.periodCheck(true);
                    },
                    onFinishRestartWithTime: function(e) {
                        var t = this;
                        var info = t._info;

                        var time = $(e.currentTarget).data('finish-restart');
                        t.recordTimeSpentItem(info, 'finish');
                        time = utils.fromDurationText(time);

                        var tasks = utils.getItem('recent-tasks', {val: []});
                        var itemId = $(e.currentTarget).closest('.row').data('item');
                        utils.setAction(t.userHash, 'finish-restart', t.taskActionParams(info));

                        var task = _.filter(tasks.val, function(task) {
                            return task.itemId == itemId;
                        });
                        _.extend(info, task[0]);

                        var now = new Date().getTime();
                        info.start = now;
                        info.time = time;
                        info.updated = now;
                        info.userHash = t.userHash;
                        localStorage.setItem('task-timer', JSON.stringify(info));
                        t.periodCheck(true);
                    },
                    onFinishRetart: function() {
                        var t = this, info = t._info;
                        utils.setAction(t.userHash, 'finish-restart', t.taskActionParams(info));
                        t.recordTimeSpentItem(info, 'finish');
                        info.start = new Date().getTime();
                        localStorage.setItem('task-timer', JSON.stringify(info));
                        t.periodCheck(true);
                    },
                    onHourGraphChanged: function(e) {
                        var $t = $(e.currentTarget).find('input');
                        var t = this;
                        t._todayExtra.showHourGraph = $t.is(':checked');
                        utils.setItem('timer-today-extra', t._todayExtra);

                        if (t._todayExtra.showHourGraph) {
                            t.updateUI();
                        } else {
                            // enlarge the window
                            window.resizeTo(550, 320);
                            $('#graph').hide();
                        }
                    },
                    onMousemove: function() {
                        var t = this;
                        t._isActive = true;
                        clearTimeout(t._deactivateTimer);
                        t._deactivateTimer = setTimeout(function() {
                            t._isActive = false;
                        }, 5000);
                    },

                    userHash: null,

                    initialize: function(opts) {
                        var t = this;
                        _.extend(t, opts);

                        t.userHash = location.search.substr(1);

                        t.readTodayExtra();

                        if (t._todayExtra.dateStamp != t._today) {
                            t._todayExtra.dateStamp = t._today;
                            t._todayExtra.items = null;
                        }

                        if (!(t._todayExtra.items instanceof Array)) {
                            t._todayExtra.items = [];
                            utils.setItem('timer-today-extra', t._todayExtra);
                        }

                        t.currPos = {
                            x: window.screenX,
                            y: window.screenY
                        };

                        t.$unit = $('.unit-time.progress-bar .complete');
                        t.$task = $('.task.progress-bar .complete');
                        t.$taskText = $('.progress .text');
                        t.$title = $('.progress .title');

                        t.updateUI();
                        t.startCheckingInfo();
                    },
                    taskActionParams: function(info) {
                        var date = new Date(), now = date.getTime(),
                            t = this, info = t._info;
                        var length = info.time;
                        var elapsed = now - info.start;
                        var spent = elapsed < length ? elapsed : length;
                        var params = {
                            spent: spent,
                            itemId: info.itemId,
                            time: info.time,
                            start: info.start,
                            update: info.update,
                            title: info.title,
                            userHash: t.userHash,
                            listId: info.listId,
                            listTitle: info.listTitle
                        }
                        return params;
                    },
                    renderGraph: function() {
                        var t = this;

                        // enlarge the window
                        window.resizeTo(550, 650);

                        // default for now
                        // task : #3b7fc4 , rest : #84bbc6
                        t._todayExtra.items.sort(function(a, b) {
                            return a.start - b.start;
                        });


                        var $items = $('#graph > .items');
                        $items.empty();

                        var now = new Date();
                        var prev = new Date(now.getFullYear(), now.getMonth(), now.getDate()).getTime();
                        var first = prev;
                        var hours = 0;

                        var markerText = function(hours) {
                            if (hours % 2) {
                                var h = (hours - 1) / 2;
                                var val = h > 12 ? h - 12 : h;
                                val = val ? val : 12;
                                return val + ':' + 30;
                            } else {
                                if (hours == 0) {
                                    return '<span class="clock">12</span> AM'
                                } else {
                                    var h = hours / 2;
                                    var val = h > 12 ? h - 12 : h;
                                    if (val == 0) val = 12;
                                    var ampm = h >= 12 ? 'PM' : 'AM';
                                    return '<span class="clock">' + val + '</span> ' + ampm;
                                }
                            }
                        }

                        var insertMarkers = function($item, start, time) {
                            var diff = (start - first) / 1800000;
                            var total = time / 1800000
                            var end = total + diff;
                            for (; hours <= end; hours++) {
                                $item.append(tpls.hourMarker({
                                    height: (hours - diff) / total * 100,
                                    text: markerText(hours),
                                    half: hours % 2
                                }));
                            }
                        }

                        var setEmptyBlock = function(start, time) {
                            var $empty = $(tpls.empty({
                                height: time / 900000 * 30
                            }));

                            $items.append($empty);
                            insertMarkers($empty, start, time);
                        }

                        var colors = {}, prevStart, prevItemEnd;
                        var newItems = [], prevItem, merged = false;
                        console.log('--------------');
                        _.each(t._todayExtra.items, function(item) {
                            if (prevItem && prevItem.itemId == item.itemId &&
                                item.start - prevItemEnd < CON_smallest) {
                                merged = true;

                                prevItem.title = item.title;
                                prevItem.listId = item.listId;
                                prevItem.spent = item.start + item.spent - prevItem.start;
                            } else {
                                prevItem = item;

                                newItems.push(item);
                            }
                            prevItemEnd = prevItem.start + prevItem.spent;
                        });

                        if (merged) {
                            t._todayExtra.items = newItems;
                            utils.setItem('timer-today-extra', t._todayExtra);
                        }

                        _.each(t._todayExtra.items, function(item) {
                            // show text only 5min = 30px
                            // min height = 10px
                            if (item.itemId == -1) {
                                var color = 'inherit';
                                var cls = 'rest';
                            } else {
                                var list = item.listId ? 'list-id:' + item.listId : 'list-id-unknown';
                                if (colors[list]) color = colors[list];
                                else color = colors[list] = t.hashColor(list);
                                cls = '';
                            }

                            if (prevStart == item.start) {
                                return;
                            }
                            prevStart = item.start;

                            if (prev) {
                                setEmptyBlock(prev, item.start - prev);
                            }
                            prev = item.start + item.spent;

                            if (item.type == 'cancel') {
                                cls += ' ' + item.type;
                            }

                            var tmp = item.spent / 100000;
                            var rate = (((tmp - 3) / 50) * 6) * 100000;
                            rate = rate < 600000 ? 600000 :
                                    rate > 1000000 ? 1000000 : rate;

                            var base = location.protocol + '//' + location.host + '/app/list/';
                            var data = {
                                height: item.spent / rate * 30,
                                color: '#' + color,
                                cls: cls,
                                itemTitle: item.title,
                                listTitle: item.listTitle,
                                listId: item.listId,
                                time: utils.formatDuration(item.spent / 60000),
                                url: (item.listId && item.itemId != -1) && base + item.listId   +
                                    (item.itemId == -1 ? '' : '/' + item.itemId)
                            };
                            data.title = item.title + ' (' + utils.formatDuration(item.spent / 60000) + ') ';

                            data.listMD5 = md5(base + item.listId);
                            if (item.spent) {
                                var fontSize = item.spent / 600000 * 14;
                            }

                            if (fontSize > 18) {
                                fontSize = 18;
                            }
                            data.fontSize = fontSize;
                            var $item = $(tpls.spentItem(data));
                            $items.append($item);
                            insertMarkers($item, item.start, item.spent);
                        });


                        if (now.getTime() - prev) {
                            setEmptyBlock(prev, now.getTime() - prev);
                        }

                    },

                    _today: utils.getDateStamp(),
                    _todayExtra: null,
                    _lastUpdated: null,
                    readTodayExtra: function() {
                        var t = this, recentUpdated = localStorage.getItem('timer-today-updated');
                        if (!recentUpdated || !t._lastUpdated || recentUpdated > t._lastUpdated) {
                            t._todayExtra = utils.getItem('timer-today-extra');
                            t._lastUpdated = recentUpdated;
                            return true;
                        }
                        return false;
                    },
                    recordTimeSpentItem: function(info, type) {
                        var t = this;

                        t.readTodayExtra();

                        var data = t.taskActionParams(info);
                        delete data.userHash;
                        delete data.update;
                        delete data.time;
                        data.type = type;

                        if (!data.start || data.spent < CON_smallest) return;

                        t._todayExtra.items.push(data);
                        utils.setItem('timer-today-extra', t._todayExtra);
                        t._lastUpdated = new Date().getTime();
                        localStorage.setItem('timer-today-updated', t._lastUpdated);
                        t.updateUI();
                    },
                    updateUI: function(lazyScroll) {
                        var t = this;
                        if (!lazyScroll || !t._isActive) {
                            // update check box
                            if (t._todayExtra.showHourGraph ||
                                typeof t._todayExtra.showHourGraph == 'undefined') {
                                $('.hour-graph input').attr('checked', 'checked');

                                // render graph
                                t.renderGraph();
                            } else {
                                $('.hour-graph input').removeAttr('checked');
                            }
                            $('#graph').show();
                            $(window).scrollTop(1);
                            $(window).scrollTop(9999999);

                            AJS.$('#graph .block').tooltip({
                                gravity: 'w',
                                html: true
                            });

                            var recentTasks = utils.getItem('recent-tasks', {val: []});
                            var $extra = $('.extra');
                            $extra.siblings('.task').remove();
                            _.each(recentTasks.val, function(task) {
                                $extra.before(tpls.task({
                                    itemId: task.itemId,
                                    title: task.title
                                }))
                            });
                        }
                    },

                    hashColor: function(str) {
                        var hash = 0;
                        for (var i = 0; i < str.length; i++) {
                            hash = str.charCodeAt(i) + ((hash << 5) - hash);
                        }
                        hash = Math.abs(hash);

                        var b = [];
                        b.push((hash >> 24) & 0xFF);
                        b.push((hash >> 16) & 0xFF);
                        b.push((hash >> 8) & 0xFF);

                        var a = [];
                        var seq = {
                            0: [0, 1, 2],
                            1: [0, 2, 1],
                            2: [1, 0, 2],
                            3: [1, 2, 0],
                            4: [2, 1, 0],
                            5: [2, 0, 1]
                        }

                        var order = seq[hash % 6];
                        for (var i = 0; i < 3; i++) {
                            a.push(b[order[i]]);
                        }

                        var hex = function(num) {
                            return num < 16 ? '0' + num.toString(16) : num.toString(16);
                        }
                        return hex(a[0]) + hex(a[1]) + hex(a[2]);
                    },

                    checkRecentTasks: function(info) {
                        var t = this;
                        // reset the recent task every 20 seconds
                        if (info.itemId != -1) {
                            var recentTasks = utils.getItem('recent-tasks', {
                                val: []
                            });

                            if (!recentTasks.val[0] || recentTasks.val[0].itemId != info.itemId ||
                                recentTasks.val[0].title != info.title) {
                                // add task to list
                                var found = _.some(recentTasks.val, function (task, idx) {
                                    if (task.itemId == info.itemId) {
                                        recentTasks.val.splice(idx, 1);
                                        return true;
                                    }
                                });
                                while (recentTasks.val.length >= 6) recentTasks.val.pop();
                                recentTasks.val.unshift(info);
                                utils.setItem('recent-tasks', recentTasks);
                                t.updateUI();
                            }

                        }
                    },

                    periodCheck: function(skipNotification) {
                        var date = new Date(), now = date.getTime();
                        var seconds = date.getSeconds();
                        var t = this, info = t._info;
                        if (info.time != -1) {
                            var unit = now % CON_unitMax;
                            t.$unit.css('width', (unit / CON_unitMax) * 100 + '%');

                            var length = info.time;
                            var elapsed = now - info.start;
                            var percentage = elapsed / length;
                            var remain = length - elapsed;
                            percentage = percentage > 1 ? 1 : percentage;
                            t.$task.css('width', percentage * 100 + '%');

                            if (percentage == 1) {
                                t.$taskText.html('Times up').addClass('times-up');
                            } else {
                                t.$taskText.html(utils.formatDuration(remain / 60000, 'ceiling'))
                                    .removeClass('times-up');
                            }

                            t.$title.text(info.title);

                            if (!skipNotification && percentage == 1 && seconds % 10 == 0 && !t._isActive) {
                                utils.setAction(t.userHash, 'times-up');
                            }
                        } else {
                            t.$taskText.html('No task running').removeClass('times-up');
                            t.$title.text('');
                        }

                        // check the current window pos
                        if (t.currPos.x != window.screenX || t.currPos.y != window.screenY) {
                            var pos = utils.getItem('timer-window-pos');
                            if (pos.x != window.screenX || pos.y != window.screenY) {
                                utils.setItem('timer-window-pos', {
                                    x: window.screenX,
                                    y: window.screenY
                                });
                            }
                        }

                        // check if the graph has updated & every minute
                        if (t.readTodayExtra() || seconds % 60 == 0) {
                            t.updateUI(true);
                            t.checkRecentTasks(info);
                        }

                    },

                    _inited: false,
                    startCheckingInfo: function() {
                        var t = this;
                        utils.checkTimerInfo(function(info) {
                            if (!t._inited) {
                                t._info = info;

                                t.checkRecentTasks(info);
                                // rendering progress bar
                                setInterval(function() {
                                    t.periodCheck();
                                }, 1000);
                                t.periodCheck(true);
                                t._inited = true;

                            }

                        }, function(newInfo, oldInfo) {
                            console.log('wtf - ', newInfo, oldInfo);

                            if (oldInfo.time == -1) {
                                return;
                            } else if (newInfo.listId != oldInfo.listId
                                || newInfo.userHash != oldInfo.userHash) {
                                utils.setAction(t.userHash, 'finish-switch-task', t.taskActionParams(oldInfo));
                            } else if (newInfo.finish) {
                                utils.setAction(t.userHash, 'finish', t.taskActionParams(oldInfo));
                            } else {
                                utils.setAction(t.userHash, 'finish-restart', t.taskActionParams(oldInfo));
                            }
                            t.recordTimeSpentItem(oldInfo, 'finish');
                            t.checkRecentTasks(newInfo);

                            t._info = newInfo;
                            t.periodCheck(true);
                        });
                    }

                })

		$(function() {
            var popup = new Popup({
                el: $('body')
            });
		});
	});
    /*<DEV*/
});
/*DEV>*/

/**
 * only for test purpose
 * @param max
 */
var clearTimeItems = function(max) {
	var extra = JSON.parse(localStorage.getItem('timer-today-extra'));
	var items = extra.items;
	for (var i = items.length; i-- > 0;) {
		if (items[i].spent < max || !items[i].start || !items[i].spent) {
			items.splice(i, 1);
		}
	}
	localStorage.setItem('timer-today-extra', JSON.stringify(extra));
}





