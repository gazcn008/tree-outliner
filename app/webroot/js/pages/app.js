/*<MAIN_SCRIPT>*/

/*<DEV*/
require(['/js/main.js'], function () {
	/*DEV>*/
	require(['ajax', 'jquery', 'utils', 'lodash', 'text!tpls/common.tpl',
			'backbone', 'mousetrap', 'keymap', 'noty', 'vex-loader',
			'vex', 'help-content', 'md5', 'item',
			'item-list', 'syncer', 'key-manager', 'header-dropdown', 'aui-dropdown2',
			'spinner', 'markdown', 'slides', 'tutorial-loader', 'bookmark-scheduler',
			'window-messenger', 'moment', 'notification-utils'],
		function (ajax, $, utils, _, commonTplStr,
				  Backbone, Mousetrap, keymap, noty, dialog,
				  vex, helpContent, md5, Item,
				  ItemList, Syncer, KeyManager, HeaderDropdown, _dropdown2,
				  _spinner, Markdown, Slides, TutorialLoader, Scheduler,
				  messenger, moment, notificationUtils,
				  _undefined) {

			moment.locale('zh-cn');

			var $currentTime = $('#current-time .text');
			$currentTime.html(moment().format('MM-DD HH:mm'));
			setInterval(function () {
				$currentTime.html(moment().format('MM-DD HH:mm'));
			}, 15 * 1000);

			// hacky way for aui dropdown2 for using mouseenter to show the dropdown menu
			var debounceHideMenu = _.debounce(function () {
				$(document.body).trigger('click', {
					hideMenu: true
				});
			}, 400);

			$('#context-menu-trigger')
				.on('mouseenter', '.aui-dropdown2-trigger', function () {
					debounceHideMenu && debounceHideMenu.cancel();
					if (!$('.control-context-menu').is(':visible')) {
						$(this).trigger('click');
					}
				});

			$(document.body)
				.on('mouseenter', '.control-context-menu, #context-menu-trigger', function () {
					debounceHideMenu.cancel();
				})
				.on('mouseleave', '.control-context-menu, #context-menu-trigger', function () {
					debounceHideMenu();
				});

			$(document).ready(function () {

				// __m1: __m8: setup func - selectable list
				var setupSelectableList = function ($text, $list, $container, margin, opts) {
					opts = opts || {};

					var _mouseDisabled = false;
					var disableMouseMove = function () {
						_mouseDisabled = true;
						_.delayRun('disable-mouse-move-' + opts.id, function () {
							_mouseDisabled = false;
						}, 800);
					}

					var moveSelected = function (direction) {
						var t = this;
						if (direction && direction.jquery) {
							var $here = direction;
							$list.find('.selected').removeClass('selected');
							$here.addClass('selected').trigger('result-hover');
						} else {
							var $selected = $list.find('.selected');
							if (direction == 'up') {
								var $new = $selected.prev();
								if (!$new.length) {
									$new = $list.find('li').eq(-1);
								}
							} else {
								$new = $selected.next();
								if (!$new.length) {
									$new = $list.find('li').eq(0);
								}
							}
							$selected.removeClass('selected');
							$new.addClass('selected')
								.trigger('result-hover');

							var newH = $new.outerHeight(true), newTop = $new.position().top,
								resH = $container.height(), resTop = $container.scrollTop();
//						console.log(newTop, resTop, newH, resH);
							if (resTop > newTop - margin) {
								$container.scrollTop(newTop - margin);
							} else if (resTop + resH - margin < newTop + newH) {
								$container.scrollTop(newTop + newH - resH + margin);
							}
						}
					}

					if (opts.delayMouse) {
						var t = this;
						var mouseMoveSelected = _.delayFn('result-mouse-delay-' + opts.id, function () {
							if (_mouseDisabled) return;
							moveSelected($(this));
						})
					} else {
						mouseMoveSelected = function () {
							if (_mouseDisabled) return;
							moveSelected($(this));
						}
					}

					$list.on('click', 'li', mouseMoveSelected);
					if (opts.delayMouse) {
						$list.on('mouseleave', 'li', _.cancelDelayFn('result-mouse-delay-' + opts.id));
					}

					if ($text) {
						$text
							.off('.selectable')
							.on('keydown.selectable', function (e) {
								if (e.keyCode == keymap.down) {
									disableMouseMove();
									moveSelected('down');
								} else if (e.keyCode == keymap.up) {
									disableMouseMove();
									moveSelected('up');
								} else if (e.keyCode == keymap.enter) {
									var $selected = $list.find('.selected');
									$selected.trigger('click');
								}
							});
					}
				}

				// __logic: * g
				var g = {
					userHash: '',
					idSeq: 0,
					userSeq: 0,
					bigint: Math.pow(36, 8),
					$list: $('#main-list'),
					tpls: utils.parseTpls(commonTplStr),
					_hoveredTarget: {},
					setupSelectableList: setupSelectableList,
					hoveredTarget: function (type, $target, isNavigating) {
						var t = this;
						if (!type) {
							var htarget = this._hoveredTarget;
							if (isNavigating && (!htarget || !htarget.view || !htarget.view.$el.is(':visible'))) {
								var $item = t.$list.find('.parent-hovered').eq(-1);
								if (!$item.length) {
									$item = t.$list.children('li').eq(0);
								}
								if ($item.length) {
									var v = $item.data('view');
									htarget = this._hoveredTarget = {
										type: 'item',
										$el: $item,
										view: v
									}
									v.trigger('item-hover');
								}
							}
							return htarget;
						} else {
							this._hoveredTarget = {
								type: type,
								$el: $target,
								view: $target.data('view')
							}
						}
					},
					_resumeHoveringTimer: null,
					_skipHoverAction: false,
					stopLazyHovering: function () {
						var t = this;
						clearTimeout(t._resumeHoveringTimer);
						t._resumeHoveringTimer = setTimeout(function () {
							t._skipHoverAction = false;
						}, 1000);
						t._skipHoverAction = true;
					},
					_currentEditor: null,
					setCurrentEditor: function (view) {
						var t = this;
						t.savePrevEditor();
						t._currentEditor = view;
					},
					savePrevEditor: function () {
						var t = this;
						if (t._currentEditor) {
							t._currentEditor.onSaveClick();
						}
					},
					getCurrentEditor: function () {
						if (this._currentEditor && !this._currentEditor.$el.is(':visible'))
							return;
						return this._currentEditor;
					},
					clearCurrentEditor: function () {
						this._currentEditor = null
					},
					resetList: function (items) {
						var t = this;
						t.list.resetList(items);
					},
					focusOnEditor: function () {
						var t = this;
						if (t._currentEditor) {
							$editor = t._currentEditor.$('> .item textarea');
							if ($editor.is(':visible')) {
								$editor.focus();
								return true;
							} else {
								t.clearCurrentEditor();
								return false;
							}
						}
					},
					$parentLi: $('<li><a></a></li>'),
					cloneItemForVertialMap: function ($item) {
						var t = this, $new = $item.find('>.item >.body').clone(),
							view = $item.data('view');
						var $li = t.$parentLi.clone().find('a')
							.data('item-id', view.item.id)
							.html($new).end();
						$li.find('#tag-input').remove();
						return $li;
					},
					removeHoveredTarget: function () {
						this._hoveredTarget = {};
					},
					_selectedItems: {},
					toggleSelect: function (itemView, enabled) {
						var t = this, item = itemView.item;
						var hit = t._selectedItems[item.id];

						if (hit) {
							if (enabled !== true) {
								itemView.setSelect(false);
								delete t._selectedItems[item.id];
								return false;
							}
						} else {
							if (enabled !== false) {
								itemView.setSelect(true);
								t._selectedItems[item.id] = itemView
								return true;
							}
						}
						return enabled;
					},

					// type: 'same-level'
					getSelectedItems: function (type) {
						var t = this;
						var itemViews = [];
						if (t.hasSelectedItems()) {
							var $itemsInOrder = $();
							for (var key in t._selectedItems) {
								$itemsInOrder = $itemsInOrder.add(t._selectedItems[key].$el);
							}
							var $prevItem, selected = [];
							$itemsInOrder.each(function () {
								var $t = $(this);
								if ($prevItem && $t.parents('li').index($prevItem) != -1) {
									var itemId = $t.attr('item-id');
									t.toggleSelect($t.data('view'), false);
									delete t._selectedItems[itemId];
								} else {
									selected.push($t.data('view'));
									$prevItem = $t;
								}
							});

							_.each(selected, function (view) {
								if (!view.$el.is(':visible')) {
									t.toggleSelect(view, false);
									return;
								}
								itemViews.push(view);
							})
						}
						return itemViews;
					},
					hasSelectedItems: function () {
						var t = this;
						for (var key in t._selectedItems) {
							if (t._selectedItems.hasOwnProperty(key)) {
								return true;
							}
						}
						return false;
					},
					clearSelectedItems: function () {
						var t = this;
						for (var key in t._selectedItems) {
							if (t._selectedItems.hasOwnProperty(key)) {
								var v = t._selectedItems[key];
								v.setSelect(false);
							}
						}
						t._selectedItems = {};
					},
					_isFocus: true
				}

				// __logic: debug
				debug = {
					items: function (data, conf, level) {
						_.each(data, function (item) {
							var output = [];
							_.each(conf, function (subConf, key) {
								if (subConf == 1) {
									output.push(item[key]);
								}
							})
							if (!level) console.log.apply(console, output);
						});
					},
					patch: function (patch) {
						var t = this;
						t.items(patch, {
							id: 1, user_hash: 1, synced: 1, parent_item_id: 1, content: 1
						})
					},
					ajax: ajax,
					g: g,
					testProb: function () {
						require(['test/test-probability'], function (testProb) {
							debug.testProb = testProb;
						});
					}
				}

				// __m14: ui - screen block
				$('#screen-block').addClass('hidden');
				KeyManager.setup(g);

				// __logic: * Router
				var Router = Backbone.Router.extend({
					timePerTaskPoint: 10,
					routes: {
						'': 'noList',
						'list/new?title=(:title)': 'newList',
						'list/new(/)': 'newList',
						'list(/)': 'noList',
						'list/:list(/)': 'list',
						'list/:list/:item': 'list', // __m18: url for item
						'play/:list': 'renderPlay',
						'md(/)': 'noMarkdown',
						'md/:list': 'renderMarkdown',
						'slides/:list': 'renderSlides',
						'webroot(/)': 'resetUrl'
					},
					resetUrl: function () {
						location.href = '/app/list';
					},
					noList: function () {
						Mousetrap.pause();

						var list = new ItemList({g: g});
						list.openLists(true);
					},

					noMarkdown: function () {
						location.href = '/app/list';
					},

					shouldSkipUrlChange: false,
					withinMarkdownPage: false,
					renderMarkdown: function (list) {
						var t = this;

						t.shouldSkipUrlChange = true;
						t.withinMarkdownPage = true;

						t.list(list);
					},

					withinSlidesPage: false,
					renderSlides: function (list) {
						var t = this;

						t.shouldSkipUrlChange = true;
						t.withinSlidesPage = true;

						t.list(list);
					},

					renderPlay: function (list) {
						var t = this;

						$('#page').attr('data-open-list', true);

						t.shouldSkipUrlChange = true;
						t.withinPlayPage = true;

						t.list(list);
					},

					list: function (list, focusedItem) {
						var t = this;

						// __m18: app - listUrl
						t.listUrl = location.origin + '/app/list/' + list + '/';

						var LIST_VERSION_KEY = 'list_version_key_' + list;

						var listVersion = utils.getItem(LIST_VERSION_KEY, {init: new Date().getTime()});
						if (typeof listVersion.updated === 'number') {
							listVersion.updated = '' + listVersion.updated;
						}
						else {
							listVersion.updated = listVersion.updated || '';
						}

						t.updateListVersion = function (updated) {
							if (updated > listVersion.updated) {
								listVersion.updated = updated;
								utils.setItem(LIST_VERSION_KEY, listVersion);
							}
						};

						var checkListVersion = function (updated) {
							if (listVersion.updated >= updated) {
								ajax.post('checkListUpdate', {
									list: list,
									updated: updated
								}, {
									changed: function (result) {
										t.updateListVersion(result.updated);

										window.syncer.sync(true);
									}
								});
							}

							t.updateListVersion(updated);
						};

						ajax.get('getList', {
							list: list,
							appVersion: window.pv && window.pv.appVersion,
							updated: listVersion.updated || 0,
						}, {
							ok: function (res) {
								t.resetUrl(res.list.id);
								g.idSeq = parseInt(res.maxId) + 1;
								if (isNaN(g.idSeq)) g.idSeq = 1;

								res.list.userId = res.userSeq;
								g.userSeq = res.userSeq.toString(36);
								var range = Math.pow(36, 5);
								g.userHash = g.userSeq + (Math.floor(Math.random() * range) + range).toString(36).substr(1);
								t.initApp(res.list, res.synced, focusedItem);

								checkListVersion(res.list.updated);
							},
							noPermission: function (res) {
								utils.showDialog({
									message: 'Sorry, this is a private list and you don\'t have permission to view it.',
									okCallback: function () {
										location.href = '/';
									}
								});

								checkListVersion(res.updated);
							}
						});

					},
					blockScreen: function (block) {
						if (block) {
							$('#screen-block').removeClass('hidden');
						} else {
							$('#screen-block').addClass('hidden');
						}
					},
					// __m18: method - itemUrl()
					itemUrl: function (item) {
						if (!g.list || !g.list.list) return;
						var t = this;
						var listUrl = '/list/' + g.list.list.id;

						if (!t.shouldSkipUrlChange) {
							if (item) {
								app.navigate(listUrl + '/' + item.id,
									{replace: true});
							} else {
								app.navigate(listUrl,
									{replace: true});
							}
						}
					},
					newList: function (title) {
						this.list(-1);
						title = title;
						this.newListTitle = title;
					},
					resetUrl: function (listId) {
						var t = this;

						if (!t.shouldSkipUrlChange) {
							if (!listId) {
								this.navigate('list', {trigger: true, replace: true});
							} else {
								this.navigate('list/' + listId, {trigger: false, replace: true});
							}
						}
					},
					gotoTutorials: function () {
						ajax.post('tutorial', {
							ok: function (listId) {
								location = location.origin + '/app/list/' + listId;
							}
						});
					},
					initApp: function (list, synced, focusedItem) {
						var t = this;

						window.name = md5(location.origin + '/app/list/' + list.id);
						document.title = list.title;

						if (list.list_type == 'tutorial') {
							var itemData = JSON.stringify(list.items);
						}

						if (window !== window.top) {
							$('#page').addClass('within-iframe');
						} else {
							$('#page').addClass('without-iframe');
						}

						window.listId = list.id;
						var syncer = window.syncer = new Syncer({
							g: g,
							lastSynced: synced
						});

						var itemList = new ItemList({
							g: g,
							list: list,
							$el: $('#main-list'),
							syncer: syncer,
							focusedItem: focusedItem
						});

						g.username = list.username;

						Item.initStatic(g);

						if (t.newListTitle) {
							itemList.setTitle(t.newListTitle);
						}

						syncer.list = itemList;
						KeyManager.setList(itemList);
						t.itemList = g.list = itemList;
						g.list.initSearches();

						var words = g.tpls.ipsumText().split(' ');
						var min = 10, max = 25;

						function randContent() {
							var counts = Math.ceil(Math.random() * (max - min) + min);
							var passage = [];
							for (var i = counts; i-- > 0;) {
								passage.push(words[Math.floor(Math.random() * words.length)]);
							}
							return passage.join(' ');
						}

						var _itemMax = 2000, allItems = [];
						window.testData = function (itemMax) {
							itemMax = itemMax || _itemMax;
							var parentId;

							while (allItems.length < itemMax) {
								var len = allItems.length, parentIndex = Math.floor(len * Math.random()) + 8;
								if (parentIndex < len) {
									parentId = allItems[parentIndex].id;
								} else {
									parentId = null;
								}

								var newItem = {
									content: randContent(),
									parent_item_id: parentId
								}
								var item = new Item({item: newItem, g: g});
								allItems.push(newItem);
							}

							list.items = allItems;
							itemList.destroy();
							var testItemList = new ItemList({
								g: g,
								list: list,
								$el: $('#main-list'),
								syncer: syncer
							});
							KeyManager.setList(testItemList);
							syncer.list = testItemList;
							g.list = itemList;

							testItemList.statusDirty();
						}

						// __m12: context menu actions
						$('.context-menu')
							.on('click', '[data-action]', function (e) {
								e.stopPropagation();
								var action = $(this).attr('data-action');
								Mousetrap.trigger(action);
							});

						// __m12: keyboard shortcut hints
						$('.context-menu a')
							.each(function () {
								var $t = $(this);
								$t.html('<span class="text">' + $t.html() + '</span>');
								$t.append('<span class="hint">' + $t.attr('title') + '</span><span class="last"></span>');
							});

						$('#create').on('click', function () {
							Mousetrap.trigger('a a');
						});

						// __m13: menu - set time / task points rate
						$('#list-actions')
							.on('click', '.help-dialog', function () {
								Mousetrap.trigger('f1');
							})
							.on('click', '.tutorials', function () {
								t.gotoTutorials();
							})
							.on('click', '.all-list', function () {
								t.itemList.openLists();
							})
							.on('click', '.toggle-learning-mode', function () {
								itemList.isLearningMode = !itemList.isLearningMode;
								if (itemList.isLearningMode) {
									itemList.renderLearningFields();
								}
								else {
									itemList.showLearningFields();
								}

								$(this).html(itemList.isLearningMode ? '关闭学习模式 (快捷键 ` )' : '打开学习模式 (快捷键 ` )');
							})
							.on('click', '.split-doc', function () {
								t.itemList.openSubContent(null, null, window.listId);
							})
							.on('click', '.open-sub-content', function () {
								if ($('#page').hasClass('sub-content-opened')) {
									window.open('/app/list/' + t.itemList.$subContent[0].contentWindow.listId);
								}
							})
							.on('click', '.replace-sub-content', function () {
								if ($('#page').hasClass('sub-content-opened')) {
									location.href = '/app/list/' + t.itemList.$subContent[0].contentWindow.listId
								}
							})
							.on('click', '.remove-list', function () {
								var $removeListConfirm = dialog.open({
									className: 'create-list vex-theme-wireframe',
									message: '你确认要删除本文档?',
									buttons: [
										{
											text: 'Yes',
											type: 'button',
											className: 'primary-button',
											click: function () {
												ajax.post('remove_list', {
													listId: window.listId
												}, {
													ok: function () {
														location.href = '/app/list'
													}
												})
											}
										},
										{
											text: 'No',
											type: 'button',
											click: function () {
												closeDialog();
											}
										}
									]
								});

								var closeDialog = function () {
									vex.close($removeListConfirm.data('vex').id);
								}
							});

						notificationUtils.initMenuItem($('#list-actions .notification-control'));

						t.initUpload();

						$('#list-actions').find('.open-markdown')
							.attr('href', '/app/md/' + list.id);

						$('#list-actions').find('.open-slides')
							.attr('href', '/app/slides/' + list.id);

						$('#list-actions').find('.play-tutorial')
							.attr('href', '/app/play/' + list.id);

						// 快捷按钮 : 设置 doc 的缩展
						$('.header').on('click', '.doc-toggler .buttons > span', function () {
							var level = ~~$(this).attr('data-level');

							KeyManager.keyManagerHandlers.expandingTopHandler(level);
						})

						var $shareType = $('.share-type');

						var refreshShareType = function (shareType) {
							var $label = $shareType.find('.share-type-label');

							$shareType.find('li').removeClass('selected');
							if (shareType === 'public') {
								$label.html('公开文档');
								$shareType.find('.public').addClass('selected');
							} else if (shareType === 'readonly') {
								$label.html('公开只读文档');
								$shareType.find('.readonly').addClass('selected');
							} else {
								$label.html('私有文档');
								$shareType.find('.private').addClass('selected');
							}
						};
						refreshShareType(list.share_type);
						if (list.owner_id == list.userId) {
							var timeleft = new HeaderDropdown({
								$el: $shareType
							});

							$shareType.on('click', 'li', function () {
								var newType = $(this).data('share-type');
								ajax.post('set_permission', {
									listId: list.id,
									shareType: newType
								}, {
									ok: function () {
										refreshShareType(newType);
									}
								})
							});
							$('#list-actions .remove-list').parent().show();
						} else {
							$shareType.addClass('disabled');
						}

						if (list.list_type == 'tutorial') {
							t.setupTutorial(itemData);
						}


						$('.page-actions .open-list').attr('href', '/app/list/' + window.listId);

						t.initMouseCoordinate();

						t.initFavDoc(list);
						t.initMarkdown();
						t.initSlides();
						t.initPlay();
						t.initScheduler();
					},

					initFavDoc: function (list) {
						var $favDoc = $('#sidebar .fav-doc');

						var renderFavDoc = function () {
							if (list.fav_seq == true) {
								$favDoc.html('取消收藏本文档');
							}
							else {
								$favDoc.html('收藏本文档')
							}
						};

						$('#sidebar')
							.on('click', '.fav-doc', function () {
								list.fav_seq = list.fav_seq === null ? 1 : null;
								renderFavDoc();

								ajax.post('favDoc', {
									list: list.id,
									isFav: list.fav_seq
								}, {})
							});

						renderFavDoc();

					},

					initMouseCoordinate: function () {
						$(document).on('mousemove', _.throttle(function (e) {
							g.mousePos = {
								x: e.pageX,
								y: e.pageY
							}
						}, 200, {trailing: true}))
					},

					initScheduler: function () {
						var t = this;

						g.scheduler = new Scheduler({
							g: g,
							$el: $('#bookmark-scheduler')
						});
					},

					initMarkdown: function () {
						var t = this;

						// 处理 markdown 逻辑
						if (t.withinMarkdownPage) {
							Mousetrap.pause();
							g.markdown = new Markdown({
								g: g,
								$el: $('#markdown')
							});

						}
					},

					initSlides: function () {
						var t = this;

						if (t.withinSlidesPage) {
							Mousetrap.pause();
							g.slides = new Slides({
								g: g
							});
						}
					},

					initPlay: function () {
						var t = this;

						if (t.withinPlayPage) {
							Mousetrap.pause();
							TutorialLoader.play(g);
						}
					},

					initUpload: function () {
						var t = this;

						if (t.withinMarkdownPage) {
							// markdown 里就别上传啦
							return;
						}

						var isFileOver = false;
						var hideFileZone = _.debounce(function () {
							$('#page').attr('data-file-over', false);
							isFileOver = false;
						}, 200);

						var MAX_FILE_SIZE = 1024 * 1024 * 10;

						$(document)
							.on('dragleave', function (e) {
								var data = e.originalEvent.dataTransfer;
								if (!data) return;

								if (data.types[0] === 'Files') {
									hideFileZone();
								}
							})
							.on('dragover', function (e) {
								var data = e.originalEvent.dataTransfer;
								if (!data) return;

								if (data.types[0] === 'Files') {
									e.preventDefault();

									if (!isFileOver) {
										isFileOver = true;
										$('#page').attr('data-file-over', true);
									}
									hideFileZone.cancel();
								}
							})
							.on('drop dragdrop', function (e) {
								var data = e.originalEvent.dataTransfer;
								if (!data) return;

								var hasLargeFile = false;
								var hasDiretory = false;

								hideFileZone();

								if (data.types[0] === 'Files') {
									e.preventDefault();

									var formData = new FormData();
									var count = 0;
									if (data.files && data.items) {
										for (var i = 0; i < data.items.length; i++) {
											var item = data.items[i].webkitGetAsEntry();
											var file = data.files[i];

											if (file.size > MAX_FILE_SIZE) {
												hasLargeFile = true;
											}
											else if (!item.isFile) {
												hasDiretory = true;
											}
											else {
												count++;
												formData.append(file.name, file);
											}
										}
									}

									if (hasLargeFile || hasDiretory) {
										var $confirm = dialog.open({
											className: 'prompt-dialog vex-theme-wireframe',
											message: '有些文件无法上传 (大小限制 10M), 且无法上传文件夹',
											buttons: [
												{
													text: '朕知道了',
													className: 'primary-button',
													type: 'button',
													click: function () {
														vex.close($confirm.data('vex').id)
													}
												}
											]
										})
									}


									if (count) {
										t.blockScreen(true);
										$.ajax({
											url: '/ajax/upload_files',
											type: 'POST',
											data: formData,
											dataType: 'json',
											cache: false,
											contentType: false,
											processData: false,
											complete: function () {
												t.blockScreen(false)
											},
											success: function (data) {

												data.data && data.data.forEach(function (file) {
													var content = '[' + file.name + '](' + file.url + ')';

													if (/\.mp4$/.exec(file.name)) {
														content += '\n\n<video width="450" height="300" src="' + file.url + '" controls></video>'
													}
													else if (/\.(jpg|gif|png|bmp|jpeg)$/.exec(file.name)) {
														content += '\n\n!' + content;
													}
													else if (/\.(mp3)$/.exec(file.name)) {
														content += '\n\n<audio width="450" height="20" src="' + file.url + '" controls></audio>'
													}

													$('.item-wrapper.hovered')
														.data('view')
														.trigger('new-item-child', {
															content: content,
															firstChild: true
														})
												})
											},
											error: function () {

											}
										});
									}
								}
							});
					},

					setupTutorial: function (items) {
						require(['tutorial-open'], function (tutorial) {
							tutorial.setup(g, items);
						});
					},

					// __m19: finish()
					finish: function (time, itemId) {
						g.list.spentTimeToItem(time, itemId);
					}
				});

				var app = g.app = new Router();
				Backbone.history.start({
					pushState: true,
					root: '/app/'
				});


			});
		});
	/*<DEV*/
});
/*DEV>*/
