define(['underscore', 'jquery', 'backbone'], function(_, $, Backbone) {

    var $body = $('body').children().hide().end(),
        $cont = $('<div><button class="add-new">add new</button><button class="change">change</button><button class="start">start</button>' +
            '<br/><span class="total"></span>' +
            '<table><thead><tr><th>Seen</th><th>Probability</th><th>Rand</th></tr></thead>' +
            '<tbody></tbody></table></div>').appendTo($body);


    var CON_base = 20000, nums = [],
        randSet = 500, randSeed = 500, draw = 0, CON_maxNum = 2000,
        CON_min = 0, CON_max = 20000, CON_delay = 0, CON_delta;

    var testProb = Backbone.View.extend({
        events: {
            'click button.add-new': 'addNew',
            'click .change': 'change',
            'click .start': 'run'
        },
        initialize: function() {
            var t = this;
            t.$table = t.$('tbody');
            t.$total = t.$('.total');

            t.addNew(2);
            t.refresh();

            console.profile('sort');
            t.test('sort', function() {
                console.profileEnd();

                console.profile('pure');
                t.test('pure', function() {
                    console.profileEnd();
                });
            });

        },
        test: function(type, callback) {
            var t = this, noRefresh = true, counts = 10000;
            t.resetProbs();

            while(counts-- > 0) {
                type == 'sort' ?
                    t.drawNum(noRefresh) :
                    t.pureRandDrawNum(noRefresh);
            }

            t.refresh();
            $body.append('<b>[done]</b> ');
            callback && callback();
        },
        run: function(callback) {
            var t = this, noRefresh = false, counts = 1000;
            clearInterval(t.timer);
            t.timer = setInterval(function() {
                t.type == 'sort' ?
                    t.drawNum(noRefresh) :
                    t.pureRandDrawNum(noRefresh);
                if (counts-- < 0)  {
                    clearInterval(t.timer);
                    t.refresh();
                    $body.append('<b>[done]</b> ');
                    callback && callback();
                }
            }, CON_delay);
        },
        type: 'sort',
        change: function() {
            var t = this;
            t.type = t.type == 'sort' ? 'pure' : 'sort';
            t.resetProbs();
        },
        rand: function(min, max) {
            min = min || 0;
            max = max || CON_base;
            return Math.floor(Math.random() * (max + 1 - min)) + min;
        },
        drawNum: function(noRefresh) {
            var t = this;

            nums.sort(function(a, b) {return a.r - b.r});
            var num = nums[0];
            num.seen ++;
            draw ++;
            num.r = t.rand() + num.r;

            CON_max = nums[nums.length - 1].r;
            CON_min = nums[0].r;

            noRefresh || t.refresh();
        },
        pureRandDrawNum: function(noRefresh) {
            var t = this;

            _.each(nums, function(num) {
                num.r = t.rand();
            });

            nums.sort(function(a, b) {return a.r - b.r});

            var num = nums[0];
            num.seen ++;
            draw ++;

            noRefresh || t.refresh();

        },

        resetProbs: function() {
            _.each(nums, function(num) {
                num.seen = 0;
                draw = 0;
            });
        },
        reset: function() {
            nums = [];
            this.addNew(2);
        },
        addNew: function(count) {
            count = ~~count || 1;
            while (count-- > 0) {
                nums.push({
                    r: this.rand(CON_min, CON_max)
                });
            }

            this.resetProbs();
        },
        refresh: _.throttle(function() {
            var t = this, average = 1/nums.length;
            t.$total.html('draw: ' + draw + ' rand set:' + randSet + ' num:' + nums.length +
                ' average prob:' + (average).toFixed(5) + ' type:' + t.type);

            t.$table.empty();
            var delta = 0;
            _.each(nums, function(num) {
                var prob = num.seen / draw;
                t.$table.append('<tr><td>' + num.seen + '</td><td>' + prob.toFixed(5) + '</td>'
                + '<td>' + num.r + '</td>');
                delta += Math.abs(prob - average);
            });

            t.$total.append (' delta:' + delta);
        }, 800)
    })

    return new testProb({
        el: $cont.get(0)
    });
})