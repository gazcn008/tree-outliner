define(['vex-dialog'], function(dialog, vex) {
	dialog.defaultOptions.className = 'vex-theme-wireframe';
	return dialog;
});