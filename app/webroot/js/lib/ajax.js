define(['jquery', 'underscore', 'vex-loader'], function($, _, dialog) {

    var skipDefault = false;

	var PostRequest = function(opts) {
		_.extend(this, opts);
		this.handlers = {};
		this.visitedHandlers = [];
	}

	PostRequest.prototype = {
		abort: function() {
			this.xhr.abort();
		},
		// stop running the rest of the handlers except the 'last' handler
		end: function() {
			this._stopBubbling = true;
		},
		handlers: null,
		visitedHandlers: null,
		setHandlers: function(callbacks, handlers) {
			var self = this;
			_.each(callbacks, function(cb, key) {
				if (typeof cb == 'function') {
					self.setHandler(key, cb, handlers);
				} else if (typeof cb == 'object') {
					self.setHandler(key, cb.fn, handlers);
					_.each(cb.accept, function(from) {
						self.setHandler(from, cb.fn, handlers);
					});
				}
			});


			// beneath are all special problem keys
			if ('anyError' in (self.handlers || handlers)) {
				// handle all errors except abort which is coming from user with consecius
				self.setHandler('error', self.handlers.anyError, handlers, true);
				self.setHandler('timeout', self.handlers.anyError, handlers, true);
				self.setHandler('parsererror', self.handlers.anyError, handlers, true);
			}

            if (self.handlers && !skipDefault) {
                self.setHandler('need-auth', function () {
                    location.reload();
                });
                self.setHandler('error', function () {
                    dialog.alert('Something wrong with the server or internet connection. ' +
                        'Please contact the admin for further help if the problem persists.');
                }, handlers, true);
                self.setHandler('timeout', function () {
                    dialog.alert('Sorry, a timeout problem is encountered. ' +
                        'Please wait for a moment and try again.');
                }, handlers, true);
                self.setHandler('parsererror', function () {
                    dialog.alert('Sorry, the return response cannot be recognised. ' +
                        'Please contact the admin for further help if the problem persists.');
                }, handlers, true);
            }
		},
		setHandler: function(key, callback, handlers, checkExist) {
			var h = handlers || this.handlers;
			if (h[key] && checkExist) return;
			if (!h[key]) {
				h[key] = [];
			}
			if (callback instanceof Array) {
				h[key] = h[key].concat(callback)
			} else {
				h[key].push(callback);
			}
		},
		dispatch: function(rsp, flag) {
			var h = this.handlers, self = this;
			this.run(h, flag, rsp);
			this.run(globalHandlers, flag, rsp);
			this.run(h, 'last', rsp);
			this.run(globalHandlers, 'last', rsp);
		},
		checkVisited: function(handler) {
			var t = this;
			var visited = !_.every(t.visitedHandlers, function(visitedHandler) {
				if (visitedHandler == handler) return false;
				return true;
			});
			if (!visited) {
				t.visitedHandlers.push(handler);
			}
			return visited;
		},
		run: function(handlers, flag, rsp) {
			var self = this;
			if (handlers[flag]) {
				_.each(handlers[flag], function(cb) {
					if (self.checkVisited(cb)) {
						return true;
					}
					cb(rsp, flag, self);
				})
			}
		}
	}

	var globalHandlers = {};

	return {
		/**
		 * callback: {}
		 * 	$flag: function(data) { ... }
		 *  $flag: {}
		 *  	accept: [ $flag1, $flag2, $flag3 ]
		 *  	fn: function(data) { ... }
		 *
		 *  NOTE: the flag 'anyError' will be triggered when any error is triggerred.
		 *
		 *  e.g. {
		 *  	ok: function(data) { ... },
		 *  	error: {
		 *  		accept: ['timeout', 'error'],
		 *  		fn: function(data) { ... }
		 *   	}
		 *  }
		 *
		 */
		post: function(url, data, callback) {
			if (typeof callback == 'undefined') {
				callback = data;
				data = {};
			}

			var xhr = $.ajax({
				url: '/ajax/' + url,
				type: 'POST',
				data: data,
				dataType: 'json',
				success: function(rsp) {
					postReq.dispatch(rsp.data, rsp.flag);
				},
				// result: "timeout", "error", "abort", and "parsererror"
				error: function(xhr, result) {
					postReq.dispatch(null, result);
				}
			});

			var postReq = new PostRequest({
				xhr: xhr
			});
			postReq.setHandlers(callback);

			return postReq;
		},

		get: function(url, data, callback) {
			if (typeof callback == 'undefined') {
				callback = data;
				data = {};
			}

			var xhr = $.ajax({
				url: '/ajax/' + url,
				type: 'GET',
				data: data,
				dataType: 'json',
				success: function(rsp) {
					postReq.dispatch(rsp.data, rsp.flag);
				},
				// result: "timeout", "error", "abort", and "parsererror"
				error: function(xhr, result) {
					postReq.dispatch(null, result);
				}
			});

			var postReq = new PostRequest({
				xhr: xhr
			});
			postReq.setHandlers(callback);

			return postReq;
		},

		globalHandler: function(flag, fn) {
			if (typeof flag == 'string') {
				PostRequest.prototype.setHandlers({
					flag: fn
				}, globalHandlers);
			} else {
				PostRequest.prototype.setHandlers(flag, globalHandlers);
			}
		},

        skipDefault: function() {
            skipDefault = true;
        }
	}
})
