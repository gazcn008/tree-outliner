define([], function() {
	var content = 
		[{title: 'About this',
			content: 
			[{text: 'This is a help box that will guide you while using the outliner tool.'},
			{text: 'Simply press @/@ to focus on the help box anytime you need it.'},
			{text: 'Press @ESC@ to hide this help box.'},
			{text: 'You can type in the filter box to quickly find something you need.'}
			]
		},
		 {
			title: 'Basic command',
			content: 
				[{text: ' @a a@ to bring up the list dialog',},
				 {text: ' @e@ to edit an item', cmd: 'edit-item'},
				 {text: ' @a g@ to remove an item', cmd: 'edit-item'},
				 {text: ' @i@ or @up@ to move the focus up', cmd: 'move-up'},
				 {text: ' @k@ or @down@ to move the focus down', cmd: 'move-down'},
				 {text: ' @j@ or @left@ to move the focus left', cmd: 'move-left'},
				 {text: ' @l@ or @right@ to move the focus right', cmd: 'move-right'}
				 ]
		},
		{
			title: 'Create new item',
			content: [{text: ' @meta + d@ or @ctrl + d@ or @enter@ to create an item as a following sibling'},
			 {text: ' @meta + f@ or @ctrl + f@ to create a child item'},
			 {text: ' @meta + e@ or @ctrl + e@ to create an item as a sibling above'},
			 {text: ' @meta + s@ or @ctrl + s@ to create parent item and wrap the current selected items into that item, aka. grouping'},
			 ]
		},
		{
			title: 'Move item',
			content: [{text: ' @meta + i@ or @ctrl + i@ to move the item(s) up'},
			 {text: ' @meta + k@ or @ctrl + k@ to move the item(s) down'},
			 {text: ' @meta + l@ or @ctrl + l@ or @tab@ to move the item(s) to the child level or aka. indent item(s)'},
			 {text: ' @meta + j@ or @ctrl + j@ or @shift + tab@ to move the item(s) to the parent level or aka. outdent item(s)'},
			 ]
		},
		{
			title: 'Expand / Collapse',
			content: [
			  {text: 'This is a perfect way to control how you view your information'},
	          {text: ' @f@ to expand / collapse an item, aka. toggle the visibility of its children item(s)'},
			 {text: ' @d@ to expand / collapse an item\'s children, aka. toggle the visibility of the grandchildren item(s)'},
			 {text: ' @r@ to expand / collapse the siblings of the item, so that you can quickly get an overview'},
			 {text: ' @s@ to expand / collapse the parent item of the current item'}
			 ]
		},
		{
			title: 'Select / unselect item(s)',
			content: [
			  {text: 'By selecting a group of items, you can apply batch operation on them, e.g. remove or move them.'},
			  {text: 'Note that currently you cannot select items from different levels, however this interaction will be enhanced later.'},
	          {text: ' @w@ to select / unselect a single item'},
			 {text: ' @shift + i@ or @shift + up@ to select / unselect the current item and the previous sibling item'},
			 {text: ' @shift + k@ or @ctrl + down@ to select / unselect the current item and the following sibling item'},
			 {text: ' @a w@ to unselect all items'}
			 ]
		},
		{
			title: 'Task management',
			content: [{text: ' Turn your list into a useful todo list!'},
			          {text: 'You can simply put an exclamation mark @!@ at the beginning when editing an item to turn it into a task item.'},
			          {text: ' @`@ to toggle an item as a normal item or a task item (a task item will come with a checkbox to track its status)'},
			 {text: ' @space@ to toggle a task item\'s status. There are three states - todo, done, cancelled'},
			 {text: 'The task complexity is really awesome as it will be maintain automatically by the tool and always let you know about the progress of your complex task.'},
			 {text: ' You can press @0@ to set a task item as a normal task item.'},
			 {text: ' Or you can press @1@ ~ @9@ to set assign a complexcity value to an item. 1 to 9 will map as 1, 2, 3, 5, 8, 13, 21, 34, 55 (Fibonacci numbers)'},
			 {text: ' Pressing @=@ will allow you to assign a custom complexity to the item.'}
			 ]
		},
		{
			title: 'Search and help',
			content: [{text: ' Key @.@ to quickly focus on the search box and type to search. While in search mode, you can use esc or enter to exit.'},
			          {text: ' Key @/@ to quickly search for some help'}
			 ]
		},
		{
			title: 'Item styling',
			content: [
			          {text: ' @h 1@ (or start with @h1.@ when editing an item) to turn the item into heading 1'},
			          {text: ' @h 2@ (or start with @h2.@ when editing an item) to turn the item into heading 2'},
			          {text: ' @h 3@ (or start with @h3.@ when editing an item) to turn the item into heading 3'},
			          {text: ' @h 4@ (or start with @h4.@ when editing an item) to turn the item into heading 4'},
			          {text: ' @h 0@ (or remove any @hX.@ from the beginning of an item) to turn the item into normal text'}
			 ]
		},
		{
			title: 'Simple markups',
			content: [
			          {text: 'Any content in an item starts with @http(s)://@ will be recognised as a hyperlink'},
			          {text: 'To create a tag, simply type @#@ and following by a word without space. e.g. #a-tag'},
			          {text: 'If you want to make the tag as a colored one, separate the style with @:@ and refer to aui-lozenge\'s style.'}, 
			          {text: 'There are 6 styles for aui-lozenge - @:default@ @:complete@ @:success@ @:current@ @:error@ @:moved@'},
			          {text: 'And you can use @:subtle@ to have a subtle version of aui lozenge'},
			          {text: 'Example: @#my-tag:complete@ @#new-project:success:subtle@'}
			          ]
		},
		{
			title: 'Tagging shorthands',
			content: [
			          {text: 'There are also some shorthands for marking up a tag.'},
			          {text: '<b>Normal tag:</b> e.g. #my-tag:red<br/> @red@ , @green@ , @blue@ , @grey@ , @yellow@ , @brown@'},
			          {text: '<b>Subtle tag:</b> e.g. #other-tag:blue2<br/> @red2@ , @green2@ , @blue2@ , @grey2@ , @yellow2@ , @brown2@  '},
			          {text: '<b>Tiny normal tag:</b> e.g. #hello:br<br/> @r@ , @g@ , @b@ , @gr@ , @y@ , @br@ '},
			          {text: '<b>Tiny subtle tag:</b> e.g. #foo:gr2<br/> @r2@ , @g2@ , @b2@ , @gr2@ , @y2@ , @br2@ '}
			          ]
		},
		{
			title: 'Item\'s read status',
			content: [
			          {text: 'An item can be unread if it is not updated by you and you haven\'t seen it since it was updated.'},
			          {text: 'An unread item is colored as <b>Blue</b>, and it\'s ascedents will be all <b>Grey</b>'},
			          {text: 'By clicking the item or focus on it for a short period will mark it as read'},
			          {text: 'You can also key @q@ to mark the current item and its descedants as read'},
			          {text: 'Key @n@ to mark the current item as read and switch to the next item'}
			          ]
		}
		]
	
	
	return content;
});