define(['backbone', 'utils', 'jquery', 'lodash'],
	function (Backbone, utils, $, _,
			  _undefined) {

		var CON_interval = 30,
			CON_appended = 80,
			CON_appear = 2,
			CON_flushing = 100,

			CON_lineHeight = 22,
			CON_padding = 15,
			CON_bottom_delta = 60;

		var g = {};
		var $hidden = $('#hidden-zone'), $body = $('body');

		var ConversationView = Backbone.View.extend({
			events: {
				'mousedown .dialog-content': 'flush',
				'mouseup .dialog-content': 'stopFlushing',
				'click .dialog-content': 'clickOnDialog'
			},
			initialize: function (opts) {
				var t = this;
				_.extend(t, opts);

				t.render();
				t.$dialogWrapper = t.$('.dialog-wrapper');
				t.$avatar = t.$('.dialog-avatar:not(.left-mode)');
				t.$avatarLeft = t.$('.dialog-avatar.left-mode');
				t.$popup = t.$('.popup-box');
				t.$tutorial = t.$('.tutorial');

				t.popup = new PopupView({
					$el: t.$('.popup-box')
				});

				var lang = utils.getItem('tutorial-lang');
				t._lang = lang.val || '';

				t.initEvents();
			},
			initEvents: function () {
				var t = this;
				$(window).focus(function () {
					t._isFlushing = false;
				});
			},
			render: function () {
				var t = this;
				t.$el.html(g.tpls.tutorial());
				$hidden.append(t.$el);

				return t.$el;
			},
			interact: function (action) {
				var t = this;
				if (action) t._scripts.push(action);

				if (!t._interacting) {
					var action = t._scripts.shift();
					if (action) {
						t._interact(action);
					}
				}
				return this;
			},
			formaliseUrl: function (url) {
				if (url.substr(0, 1) != '/' && url.substr(0, 7) != 'http://' && url.substr(0, 8) != 'https://') {
					return this.contextPath + url;
				} else {
					return url;
				}
			},
			setAvatar: function (avatar) {
				var t = this;
				t.$tutorial
					.toggleClass('avatar-left', !!avatar.left);

				if (!avatar.left) {
					t.$avatar
						.css('background-image', 'url(' + t.formaliseUrl(avatar.url) + ')');
				} else {
					t.$avatarLeft
						.css('background-image', 'url(' + t.formaliseUrl(avatar.url) + ')');
				}
			},
			_scripts: [],
			_interacting: false,
			$audios: {},
			_interact: function (action) {
				var t = this;
				if (!action) return;

				// other types
				// image, form, questions, wait, text
				t._interacting = true;

				if (action.context) {
					t.contextPath = action.context.path + '/';
				}

				if (action.avatar) {
					t.setAvatar(action.avatar);
				}

				if (action.image) {
					if (action.image.hide) {
						t.popup.hide();
					} else {
						t.popup.showImage(t.formaliseUrl(action.image.url));
						if (action.image.width) {
							action.image = _.extend({},
								action.image, {maxWidth: action.image.width});
						}
						t.popup.setImageInfo(action.image);
					}
				}

				if (action.scene) {
					$body.css('background-image', 'url(' + t.formaliseUrl(action.scene.url) + ')');
				}

				if (action.audio) {
					t.$audioContainer = t.$audioContainer || $('#audios');
					var audio = action.audio;
					audio.id = audio.id || audio.url;
					t.show();
					if (!t.$audios[audio.id]) {
						audio.src = t.formaliseUrl(audio.url);
						var $audio = t.$audios[audio.id] = $(g.tpls.tutorialAudio(audio));
						t.$audioContainer.append($audio);
					} else {
						$audio = t.$audios[audio.id];
					}

					var audioElem = $audio[0];
					if (audio.action == 'stop') {
						audioElem.pause();
					} else {
						audioElem.play();
						audioElem.currentTime = 0;
					}
				}

				if ('text' in action) {
					var text = t._lang && action['text' + t._lang] || action.text;
					if (t.hidden) {
						t.show();
					}

					t._say(text, action.logic);
				} else if (typeof action.logic == 'function') {
					t.feedback(function () {
						t._interacting = false;
						t.interact();
					}, action.logic);
				} else {
					t._interacting = false;
					t.interact();
				}

				if (action.mute) {
					t.hide();
				}
			},
			_showTimer: null,
			hidden: true,
			show: function () {
				var t = this;
				clearTimeout(t._showTimer);
				t.$el.appendTo($body);
				t.hidden = false;

				t.hideShadow();

				setTimeout(function () {
					t.$dialogWrapper.css('bottom', 0);
					t.$popup.css('left', 40);
				}, 20);

				setTimeout(function () {
					t.showShadow();
				}, 800);

			},
			hideShadow: function () {
				this.$('.has-shadow').removeClass('has-shadow').addClass('no-shadow');
			},
			showShadow: function () {
				this.$('.no-shadow').addClass('has-shadow').removeClass('no-shadow');
			},
			hide: function () {
				var t = this;
				clearTimeout(t._showTimer);
				t.hidden = true;

				t.hideShadow();
				setTimeout(function () {
					t.$dialogWrapper.css('bottom', -600);
					t.$popup.css('left', '-100%');
				}, 20);

				setTimeout(function () {
					t.showShadow();
					$hidden.append(t.$el);
				}, 800);
			},
			close: function () {
			},
			_say: function (message, logicFunc) {
				var t = this;
				t._saying = true;

				var chars = message.split(''), charsLen = chars.length;
				var idx = 0;
				var $cont = t.$('.dialog-content'), $inner = $cont.find('.inner');
				var checkScroll = 0;

				$inner.html(message);

				var $inss = [];

				var parseContent = function ($target) {
					$target.contents().each(function () {
						if (this.nodeType == document.TEXT_NODE) {
							var $t = $(this);
							var html = $t.text();
							var chs = html.split('');
							_.each(chs, function (ch) {
								var $ins = $('<ins/>').text(ch).data('ch', ch);
								$inss.push($ins);
								$t.before($ins);
							});
							$t.remove();
						} else {
							parseContent($(this));
						}
					});
				};
				parseContent($inner);

				var contHeight = $cont.height();
				t.timer = setInterval(function () {
					for (var i = 0; i < CON_appear || t._isFlushing && i < CON_flushing; i++) {
						(function ($ins) {
							if ($ins) {
								t._charAppended++;
								$ins.after($ins.data('ch'));

								if (checkScroll++ > 10) {
									var contScrolled = $cont.scrollTop();
									var insPos = contScrolled + $ins.position().top + CON_lineHeight + CON_padding;
									var scrolled = contScrolled + contHeight + CON_padding + CON_bottom_delta;
									if (scrolled >= insPos || t._isFlushing) {
										$cont.scrollTop(insPos - contHeight);
									}
									checkScroll = 0;
								}

								$ins.remove();

								if (!$inss.length && t.timer) {
									clearInterval(t.timer);
									t.timer = null;

									t.feedback(function () {
										t._interacting = false;
										t.interact();
									}, logicFunc);
								}
							} else {
								clearInterval(t.timer);
							}
						})($inss.shift())
					}
				}, CON_interval);
			},

			/**
			 * '' => english
			 * 'cn' => chinese
			 */
			_lang: '',

			createLogicObj: function () {
				var t = this;
				var logicObject = {
					steps: function (steps) {
						var idx = 0;

						var doStep = function () {
							var step = steps[idx];
							step(function () {
								idx++;
								doStep();
							}, function () {
								idx = 0;
								doStep();
							})
						};
						doStep();
					},
					stopWaiting: function () {
						while (logicObject._waits.length) {
							clearInterval(logicObject._waits.shift());
						}
					},
					_waits: [],
					waitFor: function (fn) {
						var done = function (next) {
							clearInterval(wait);
							next && setTimeout(next);
						};
						var wait = setInterval(function () {
							fn(done)
						}, CON_interval * 30);
						this._waits.push(wait);
					},
					off: function () {
						while (logicObject._offFns.length) {
							var off = logicObject._offFns.shift();
							off.obj.off(null, off.fn);
						}
					},
					_offFns: [],
					once: function (obj, event, fn) {
						obj.once(event, fn);
						logicObject._offFns.push({obj: obj, fn: fn})
					},
					setLang: function (lang) {
						t._lang = lang || '';
						utils.setItem('tutorial-lang', {val: lang});
					},
					_choiceChosen: function (choice) {
						logicObject._choiceObj[choice]();
					},
					_choiceObj: {},
					choices: function (choiceObj) {
						logicObject._choiceObj = choiceObj;
					}
				};
				return logicObject;
			},
			_stopFeedback: null,
			stopFeedback: function () {
				var t = this;
				clearInterval(t.checkFeedback);
				t._stopFeedback && t._stopFeedback();
				t._stopFeedback = null;
				t._preventReset = false;
			},
			_preventReset: false,
			feedback: function (callback, logicFunc) {
				var t = this;
				t._clickOnDialog = t._isFlushing ? -1 : 0;
				t._isFlushing = false;

				if (typeof logicFunc == 'function') {
					var logicObj = t.createLogicObj();

					t.$dialogWrapper
						.on('click.dialog-choice', '[choice]', function () {
							var $t = $(this);
							var choice = $t.attr('choice');
							logicObj._choiceChosen(choice);
						});

					t._stopFeedback = function () {
						logicObj.stop && logicObj.stop();
						logicObj.off();
						logicObj.stopWaiting();
						t.$dialogWrapper.off('.dialog-choice')
					};

					logicFunc(g, logicObj, function () {
						t._stopFeedback();
						callback();
					});
				} else {
					t.checkFeedback = setInterval(function () {
						if (t._clickOnDialog > 0) {
							callback();
							clearInterval(t.checkFeedback);
						}
					}, CON_interval * 5);
				}
			},
			_clickOnDialog: false,
			clickOnDialog: function (e) {
				if ($(e.target).is('a')) {
					return;
				}
				this._clickOnDialog++;
			},
			timer: null,
			_charAppended: CON_appended,
			random: function (min, max) {
				return Math.floor(Math.random() * (max - min)) + min;
			},
			_isFlushing: false,
			flush: function () {
				this._isFlushing = true;
			},
			stopFlushing: function () {
				this._isFlushing = false;
			},

			stopScript: function () {
				var t = this;
				t._charAppended = CON_appended;
				clearTimeout(t.timer);
				t.stopFeedback();
				t.timer = null;
				t._scripts = [];
				t._interacting = false;
			},

			loadScript: function (script) {
				var t = this;

				if (!script) return;

				if (!(script instanceof Array)) {
					script = [script];
				}
				_.each(script, function (action) {
					t.interact(action);
				});
			},

			clearScript: function () {
				this._scripts = [];
			}
		});

		var PopupView = Backbone.View.extend({
			initialize: function (opts) {
				var t = this;
				_.extend(t, opts);

			},
			show: function () {
				var t = this, $t = this.$el.show();
			},
			hide: function () {
				var t = this, $t = t.$el;
				$t.hide();
			},
			showImage: function (img) {
				var t = this;
				t.show();
				t.$('img').attr('src', img);
			},
			setImageInfo: function (info) {
				info = info || {maxWidth: 650};
				var t = this, css = {};
				css.maxWidth = info.maxWidth ? info.maxWidth : '100%';

				t.$('img').css(css);
			}
		});

		var tutorial = {
			_conversation: null,
			clearScript: function () {
				this.init();
				this._conversation.clearScript();
			},
			/**
			 * [{
                     *  avatar      : {url: string, left: boolean}
                     *  context     : {path: string}
                     *  text        : string can embed <a choice href>
                     *      textLANG    : LANG= e.g. 'cn', 'en' .. 不同的语言, 通过 setLang(lang) 来切换
                     *  scene       : url -- 背景
                     *  image       : {maxWidth: number, url: } -- 用于展示的图片
                     *  logic       : function( globalContext, logicObject, nextFunction )
                     *          logicObject: {
                     *              waitFor: function( done )
                     *              stopWaiting: function()
                     *              choice: { choiceName: function() } -- choiceName 由 a[choice] 给定
                     *   audio      : {id, url, action: 'stop' or 'play' } 默认是 'play'
                     *                  如果没指定 id, 则url为id
                     * }]
			 * @param script
			 */
			loadScript: function (script) {
				this.init();
				this._conversation.loadScript(script);
			},
			init: function (_g) {
				$body.addClass('tutorial-body');

				g = _g || g;
				if (!this._conversation) {
					var conversation = this._conversation = new ConversationView();
				}
			},
			context: function (path) {
				tutorial.loadScript({
					context: {
						path: path
					}
				});
				return this;
			},
			scene: function (url) {
				tutorial.loadScript({
					scene: {
						url: url
					}
				});
				return this;
			},
			hideImage: function () {
				tutorial.loadScript({
					image: {
						hide: true
					}
				});
				return this;
			},
			image: function (url, maxWidth, logic) {
				tutorial.loadScript({
					image: {
						url: url,
						maxWidth: maxWidth
					},
					logic: logic
				});
				return this;
			},

			_currMusic: null,
			stopMusic: function() {
				if (this._currMusic) {
					this._currMusic.stop();
					this._currMusic = null;
				}
			},

			playMusic: function(url) {
				this.stopMusic();
				this._currMusic = tutorial.audio(url);
				this._currMusic.play();
			},

			audio: function (url) {
				return {
					play: function (logic) {
						tutorial.loadScript({
							audio: {
								url: url,
								action: 'play',
								logic: logic
							}
						});
						return this;
					},
					stop: function (logic) {
						tutorial.loadScript({
							audio: {
								url: url,
								action: 'stop',
								logic: logic
							}
						});
						return this;
					}
				};
				return this;
			},

			_avatars: {},
			say: function (name, text, shouldShowOnLeft) {
				this._avatars[name].say(text, shouldShowOnLeft);
				return this;
			},
			mute: function () {
				tutorial.loadScript({
					mute: true
				});
				return this;
			},
			avatar: function (url, shouldShowOnLeft, name) {
				shouldShowOnLeft = !!shouldShowOnLeft;

				var avatarObject = {
					_image: null,
					image: function (url, maxWidth, logic) {
						if (logic) {
							tutorial.image(url, maxWidth, logic)
						} else {
							this._image = {
								url: url,
								maxWidth: maxWidth
							};
						}

						return this;
					},
					hideImage: function () {
						tutorial.hideImage();
						return this;
					},
					say: function (text, shouldShowOnLeftLater, image, logic) {
						if (typeof shouldShowOnLeftLater == 'boolean') {
							shouldShowOnLeft = shouldShowOnLeftLater;
						}
						tutorial.loadScript({
							avatar: {
								url: url,
								left: shouldShowOnLeft
							},
							image: this._image,
							logic: logic,
							text: text
						});

						this._image = null;
						return this;
					},
					mute: function () {
						tutorial.loadScript({
							mute: true
						});
						return this;
					}
				}

				this._avatars[name] = avatarObject;
				return avatarObject;
			},
			mute: function () {
				tutorial.loadScript({
					mute: true
				})
				return this;
			},
			logic: function (logic) {
				tutorial.loadScript({
					logic: logic
				});
				return this;
			}
		};

		return tutorial;
	});
