define(['original-marked'], function (marked) {
	var wrappedMarked = function (markdownString) {
		var html = marked(markdownString);
		html = html.replace(/<script>[^]+?<\/script>/g, '');
		return html;
	};

	wrappedMarked.setOptions = function () {
		return marked.setOptions.apply(marked, arguments);
	};

	wrappedMarked.Renderer = marked.Renderer;

	return wrappedMarked;
});
