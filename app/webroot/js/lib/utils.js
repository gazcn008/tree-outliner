define(['underscore', 'lodash', 'spin', 'jquery', 'md5', 'vex',
		'vex-loader', 'keymap', 'marked', 'highlight'],
	function (_, _l, Spinner, $, md5, vex,
			  dialog, keymap, marked, highlight) {

		var templateCache = {}, rgxTplName = /^---(\w+)\s*$/;
		var rootTpls = {}, CON_limit = 24, CON_total = CON_limit * 2 + 1;

		marked.setOptions({
			renderer: new marked.Renderer(),
			gfm: true,
			tables: true,
			breaks: false,
			pedantic: false,
			sanitize: false,
			smartLists: true,
			smartypants: false,
			highlight: function (code) {
				return highlight.highlightAuto(code).value;
			}
		});

		var utils = {
			CON: {
				spinOpts: {
					small: {
						lines: 10,
						length: 5,
						width: 3,
						radius: 10,
						corners: 0.5,
						rotate: 0,
						direction: 1,
						color: '#fff',
						speed: 1,
						trail: 60,
						shadow: false,
						hwaccel: false,
						className: 'spinner',
						zIndex: 200,
						top: '50%', // Top position relative to parent in px
						left: '50%' // Left position relative to parent in px
					}
				}
			},


			parseTpls: function (templateString, templateName) {
				if (templateCache[templateString]) {
					return templateCache[templateString];
				} else {
					var lines = templateString.split('\n');
					var tpl = '';
					var tplName = '_';
					var tpls = {};
					for (var i = 0; i < lines.length; i++) {
						var line = lines[i];
						var match = rgxTplName.exec(line);
						var generateTpl = function () {
							_.templateSettings.variable = 'd';
							var fn = _.template(tpl.trim());
							tpls[tplName] = function (d) {
								d = d || {};
								d.tpls = tpls;
								d.root = rootTpls;
								return fn.call(this, d);
							}
						}

						if (match) {
							if (tpl) {
								generateTpl();
								tpl = '';
							}
							tplName = match[1];
						} else {
							tpl += '\n' + line;
						}
					}
					if (tpl) {
						generateTpl();
					}
					if (templateName) {
						rootTpls[tempalteName] = tpls;
					}
					templateCache[templateString] = tpls;
					return tpls;
				}
			},

			urls: {
				/**
				 * videoFolderId | videoFolder, videoFile, timeText, videoId, size,
				 * second
				 */
				clipImage: function (opts) {
					opts.videoFolder = opts.videoFolder
						|| pv.videoFolders[opts.videoFolderId].web;

					return opts.videoFolder + '/' + opts.videoId + '/images/'
						+ opts.videoFile + '/' + opts.timeText + '.images/'
						+ opts.size + '/' + opts.second + '.jpg';
				},
				clipUrl: function (clip) {
					return '/pages/player/' + clip.collection_id + '/' + clip.seq;
				}
			},

			getToday: function () {
				var now = new Date();
				var d = now.getDate();
				var m = now.getMonth() + 1;
				return '' + now.getFullYear()
					+ (m < 10 ? '0' + m : m)
					+ (d < 10 ? '0' + d : d);
			},

			notyWarning: function (message, delay) {
				noty({
					text: message,
					layout: 'bottomRight',
					type: 'warning',
					timeout: delay
				})
			},

			beforeUnload: function () {
				var t = this, info = t.getItem('task-timer');
				if (info.userHash == t.userHash) {
					info.closed = true;
					t.setItem('task-timer', info);
					t._timerPopup && t._timerPopup.close();
				}
			},

			_timerPopup: null,
			_timerItemId: null,
			_popupEventsInited: false,
			_popupId: 0,
			// __m19: openTimer()
			openTimer: function (time, opts) {
				var t = this;
				var margin = 20, width = 550, height = 270;
				opts = opts || {};

				var noTask = false;
				if (time == -1) {
					var info = utils.getItem('task-timer');
					if (!info.itemId) {
						noTask = true;
						var timeInfo = {
							time: -1,
							updated: new Date().getTime(),
							userHash: opts.userHash
						}
						localStorage.setItem('task-timer', JSON.stringify(timeInfo));
					}
				}

				if (time && time != -1) {
					if (typeof time == 'string') {
						time = t.fromDurationText(time);
					}

					var now = new Date().getTime();
					var timeInfo = {
						updated: now,
						userHash: opts.userHash,
						listId: opts.listId,
						listTitle: opts.listTitle,
						itemId: opts.itemId,
						time: time,
						title: opts.title,
						start: opts.start || now
					};
					localStorage.setItem('task-timer', JSON.stringify(timeInfo));

					if (!t._popupEventsInited) {
						$(window).on('beforeunload', function () {
							t.beforeUnload()
						});
						t._popupEventsInited = true;
					}

				} else {
					t._timerPopup && t._timerPopup.close();
					if (time == -1) {
						utils.setAction(info.userHash, 'repopup');
						return;
					} else {
						opts.userHash = t.userHash;
					}
				}

				var pos = t.getItem('timer-window-pos');
				var left = pos.x || screen.width - width - margin;
				var top = pos.y || screen.height - height - margin;

				if (!t._timerPopup || t._timerPopup.closed) {
					window._testPopup = t._timerPopup = window.open('/popup?' + opts.userHash, t._popupId++,
						'width=' + width + ',height=' + height + ',' +
						'left=' + left + ',' +
						'top=' + top + ',' +
						'resizable=0,dialog=true,scrollbars=1');
				}

				setTimeout(function () {
					if (!t._timerPopup) {
						alert('Please allow this website to open popups. :)');
					}
//                else if (time && time != -1) {
//                    t.showRunning(opts.itemId);
//				}
				}, 20);
			},

			fixVexRepainBug: function () {
				var $vex = $('.vex').on('scroll', _.debounce(function () {
					$vex.toggleClass('-webkit-repaint');
				}, 50));

			},

			/**
			 *
			 * @param opts:
			 *  'message',
			 *  'okCallback'
			 */
			showDialog: function (opts) {
				opts = opts || {};
				Mousetrap.pause();
				$(document).on('keydown.current-dialog', function (e) {
					if (e.keyCode == keymap.enter) {
						opts.ok && opts.ok();
						closeDialog();
					}
				});

				var $dialog = dialog.open({
					message: opts.message,
					buttons: [
						{
							text: 'OK',
							type: 'button',
							className: 'primary-button',
							click: function () {
								opts.okCallback && opts.okCallback();
								closeDialog();
							}
						}
					],
					escapeButtonCloses: false,
					overlayClosesOnClick: false,
					afterClose: function () {
						Mousetrap.unpause();
					}
				});

				var closed = false;
				var closeDialog = function () {
					if (closed) return;
					vex.close($dialog.data('vex').id);
					closed = true;
					$(document).off('keydown.current-dialog')
				}
			},

			showRunning: function (itemId) {
				var $item = $('[item-id=' + itemId + ']');
				$('[running]').removeAttr('running');
				if ($item.length) {
					$item.attr('running', 'self');
					$item.parents()
						.attr('running', 'parent');
				}
			},

			// __m21: __m23: detect tags
			rgxTagDef: /^(\#(?:[^{#\[!！\s\:：]+[\.。])?(?:[^\[#{\!！\s\:：]+)(?:[\:：](?:[^\s\!\:：！]*)(?:[\:：](?:[^\!\s\:：！]+))?)?)((?:[\!！][^\s\!！]+)*) *[\=＝]/,
			detectTags: function (content) {
				var t = this, tags = [];
				var m;
				if (m = t.rgxTagDef.exec(content)) {
					var def = m[1];
					content = content.replace(t.rgxTagDef, '$1$2 =');
				} else {
					def = null;
				}

				while (true) {
					var m = t.rgxTag.exec(content);
					if (!m) break;

					var style = ':'
						+ (m[4] ? m[4] : '')
						+ (m[5] ? m[5] : '');

					tags.push({
						text: (m[2] ? m[2] : '')
						+ m[3] + style,
						alias: (m[6] ? m[6].split('!') : [])
					});
				}
				return {
					def: def,
					content: content,
					tags: tags
				};
			},

			// __m21: tag to lozenge
			rgxTag: /(^|\s|\n|\t)#([^#\[{!！\s:：]+[\.。])?([^\[#{!！\s:：]+)(?:[:：]([^\s!:：！]*)(?:[\:：]([^!\s:：！]+))?)?(([\!！][^\s\!！]+)*)/g,
			replaceTags: function (html) {
				return html
					.replace(this.rgxTagDef, '<span class="tag-def-label"> $1 </span>')
					.replace(this.rgxTag,
						'$1<span class="aui-lozenge aui-lozenge-$4 aui-lozenge-$5" tag-group="$2"' +
						'tag-text="$2$3:$4$5" data-tag="$2$3"><span class="g">$2</span><span class="c">$3</span></span>');
			},

			getTags: function (content) {
				var t = this;
				content = content.replace(this.rgxTagDef, ' $1 ');
				var m, tags = {};
				while (m = t.rgxTag.exec(content)) {
					tags[m[2] ? m[2] + m[3] : m[3]] = 1;
				}
				return _.map(tags, function (_tmp, tag) {
					return tag;
				});
			},

			rgxTagNoColon: /(^|\s|\n|\t)\#([^\[#{\!\s\:：\=\＝]+)(?=$|\s|\n|\t|=|＝|\!\！)/g,
			addColonForTags: function (content) {
				var t = this;
				if (!content) return '';
				return content.replace(t.rgxTagNoColon, '$1#$2:');
			},

			tagMap: {
				'red': 'error',
				'blue': 'complete',
				'yellow': 'current',
				'brown': 'moved',
				'green': 'success',
				'grey': 'default',
				'r': 'error',
				'b': 'complete',
				'y': 'current',
				'br': 'moved',
				'g': 'success',
				'gr': 'default'
			},
			tagRgx: /(\s+|aui-lozenge-?)/g,
			parseTags: function ($cont) {
				var t = this;
				$cont.find('.aui-lozenge').each(function () {
					var $t = $(this);

					var cls = $t.attr('class');
					cls = cls.replace(t.tagRgx, '');

					var isSubtle = false;
					if (cls.indexOf('3') != -1) {
						cls = cls.replace('3', '');
					} else {
						cls = cls.replace('2', '');
						isSubtle = true;
					}

					var map = t.tagMap[cls];
					if (map) {
						$t.addClass('aui-lozenge-' + map);
					}
					if (isSubtle) {
						$t.addClass('aui-lozenge-subtle');
					}

					var groupText = $t.attr('tag-group');
					if (!groupText) {
						$t.removeAttr('tag-group');
					} else if (groupText == 'task.'
						|| groupText == 'question.') {
						$t.addClass('is-task');
					}
				});
			},

			getDateStamp: function () {
				var now = new Date();
				var y = now.getFullYear();
				var m = now.getDate();
				var d = now.getDate();

				m = m < 10 ? '0' + m : m;
				d = d < 10 ? '0' + d : d;
				return '' + y + m + d;
			},

			// __m19: syncTimerInfo()
			timerInfo: null,
			readTimerInfo: function (callback, onChanged, onItemChanged) {
				var t = this;
				var timerInfoString = localStorage.getItem('task-timer');
				if (timerInfoString) {
					try {
						var timerInfo = JSON.parse(timerInfoString);
					} catch (ex) {
						console.log(ex);
						timerInfo = null;
					}

//				console.log('time info string - ', timerInfoString, timerInfo, t.timerInfo,
//                        t.timerInfo && timerInfo &&  t.timerInfo.updated < timerInfo.updated);
					if (t.timerInfo && timerInfo &&
						(t.timerInfo.updated < timerInfo.updated || !timerInfo.userHash)) {
						console.log('come on - change me', onChanged);
						onChanged && onChanged(timerInfo, t.timerInfo);
					}
					if (!t.timerInfo || t.timerInfo.updated < timerInfo.updated) {
						t.timerInfo = timerInfo;
					}
					callback && callback(t.timerInfo);
				}
			},
			checkTimerInfo: function (callback, onChanged, delay) {
				var t = this;
				setInterval(function () {
					t.readTimerInfo(callback, onChanged);
				}, delay || 800);
			},

			setTimerClosed: function () {
				var t = this;
				t.timerInfo.closed = true;
				localStorage.setItem('task-timer', JSON.stringify(t.timerInfo));
			},

			removeRunningIcons: function () {
				$('[running]').removeAttr('running');
			},

			setRedirect: function (urlHash) {
				this.setItem('redirect-list', {id: urlHash});
			},

			checkRedirect: function (urlHash) {
				var list = this.getItem('redirect-list');
				if (list && list.id == urlHash) {
					this.setRedirect('');
					return true;
				}
			},

			tryOpenTimer: function (userHash, app, listId) {
				var t = this;
				t.setUserHash(userHash);

				if (!t._popupEventsInited) {
					setInterval(function () {
						var actions = t.getItem('task-action');

						var action = false, actionIdx, firstRun = true;
						while (action || firstRun) {
							action = firstRun = false;

							_.some(actions, function (curr, idx) {
								if (curr.userHash == t.userHash) {
									action = curr;
									actionIdx = idx;
									return true;
								}
							});

							if (action) {
								var delayAlert = function () {
									setTimeout(function () {
										if (!document.hasFocus() &&
											(t._timerPopup && !t._timerPopup.document.hasFocus())) {
											alert('Task finished');
										}
									}, 1000);
								}

								var p = action.params;

								switch (action.action) {
									case 'goto':
										if (action.params.itemId != -1) {
											app.itemList.hoverOnItem($('[item-id=' + action.params.itemId + ']'), true);
										}
										confirm('Hello, welcome back.');
										break;
									case 'times-up':
									case 'repopup':
										t.openTimer();
										break;
									case 'finish':
										app.finish(action.params.spent, action.params.itemId);
										utils.setItem('task-timer', {});
										t.removeRunningIcons();
										break;
									case 'cancel':
										t.removeRunningIcons();
										utils.setItem('task-timer', {});
										break;
									case 'finish-restart':
										app.finish(p.spent, p.itemId);
										break;
									case 'finish-switch-task':
										app.finish(p.spent, p.itemId);
										t.removeRunningIcons();
										break;
									case 'rest':
										t.removeRunningIcons();
										break;
								}

								while (actions.length > 6) {
									actions.shift();
								}
								actions.splice(actionIdx, 1);
								t.setItem('task-action', actions);
							}
						}
					}, 800);
				}

				t.readTimerInfo(function (timerInfo) {
					if (timerInfo && timerInfo.closed) {
						timerInfo.closed = false;
						t.openTimer(timerInfo.time, {
							start: timerInfo.start,
							itemId: timerInfo.itemId,
							userHash: userHash,
							title: timerInfo.title,
							listId: timerInfo.listId,
							listTitle: timerInfo.listTitle
						})
					}
				});
			},

			// __m19: setAction()
			userHash: null,
			setUserHash: function (userHash) {
				this.userHash = userHash;
			},
			setAction: function (userHash, action, params) {
				var t = this;
				params = params || {};
				try {
					var actions = JSON.parse(localStorage.getItem('task-action'));
					if (!(actions instanceof Array)) actions = [];
				} catch (e) {
					actions = [];
				}
				actions.push({
					userHash: userHash,
					action: action,
					params: params
				});
				localStorage.setItem('task-action', JSON.stringify(actions));
			},
			setItem: function (key, val) {
				localStorage.setItem(key, JSON.stringify(val));
			},
			getItem: function (key, defaultValue) {
				var itemString = localStorage.getItem(key);
				if (itemString) {
					try {
						var item = JSON.parse(itemString);
					} catch (ex) {
						item = null;
					}
				}
				if (!item) item = defaultValue || {};
				return item;
			},

			_completedTasks: {},
			taskDone: function (view) {
				var t = this, item = view.item;
				clearTimeout(t._completedTasks[item.id]
					&& t._completedTasks[item.id].timer);

				t._completedTasks[item.id] = {
					view: view,
					timer: setTimeout(function () {
						var info = t.getItem('task-timer');

						if (info.userHash == t.userHash && info.itemId == view.item.id) {
							info.finish = true;
							info.updated = new Date().getTime();
							t.setItem('task-timer', info);
						}

					}, 800)
				}
			},

			/**
			 *
			 * @param duration in minutes
			 * @param method
			 * @returns {string}
			 */
			formatDuration: function (duration, method) {
				if (!method) {
					duration = Math.round(duration);
				} else if (method == 'floor') {
					duration = Math.floor(duration);
				} else if (method == 'ceiling') {
					duration = Math.ceil(duration);
				}
				if (duration < 60) {
					return duration + 'm';
				}

				var hour = Math.floor(duration / 60);
				var min = Math.floor(duration % 60);
				if (min == 0) {
					return hour + 'h';
				} else {
					return hour + 'h ' + min + 'm';
				}
			},

			// __m19: fromDurationText() in ms
			_rgxTimeToken: /(\d+(?:\.\d+)?)(h|m)?/g,
			fromDurationText: function (timeText) {
				var t = this, time = 0;
				while (match = t._rgxTimeToken.exec(timeText)) {
					var val = Number(match[1]);
					if (isNaN(val)) continue;
					if (match[2] == 'h') {
						val *= 3600;
					} else if (match[2] == 'm') {
						val *= 60;
					} else {
						val *= 600;
					}
					time += val;
				}
				return time * 1000;
			},

			// __m14: rgx - parse links & images
			rgxItem: /#\{(\w+)(?:\|([^\}]*))?\}#/g,
			rgxLink: /(^|\s|<|>|\n|\t)((?:(?:https?|ssh|ftp|svn)\:\/\/|mailto\:)[^\s\'\<\n]+)(?=\s|$|<|>)/g,
			rgxListItem: /@\{(\w+)?\|(\w+)?\|([^\}]+)\}@/g,
			rgxImage: /(^|\s|<|>)!([^\s!]+)!(?=\s|$|<|>)/g,
			rgxImageFirstLine: /^![^\!\s]+!/,
			rgxImageMarkdownFirstLine: /^!\[[^\[]+\]/,
			rgxHighlightLeft: /_\(/g,
			rgxHighlightRight: /\)_/g,
			rgxHighlightLeftEscaped: /(_\\*)\\(\()/g,
			rgxHighlightRightEscaped: /(\)\\*)\\(_)/g,

			getBody: function (content, item) {
				var that = this;

				if (content) {
					if ((content.substr(0, 1) == '!' || content.substr(0, 1) == '！')
						&& !that.rgxImageFirstLine.exec(content)
						&& !that.rgxImageMarkdownFirstLine.exec(content)) {
						content = '#task.todo:r2 ' + content.substr(1);
						item && (item.content = content);
					} else if ((content.substr(0, 1) == '?' || content.substr(0, 1) == '？')) {
						content = '#question.open:r2 ' + content.substr(1);
						item && (item.content = content);
					}

					if (!content && item) {
						item.content = content;
					}

					var base = location.origin;

					var bodyHtml = content

					// __m14: content - parse links & images
						.replace(that.rgxItem, function (whole, id, text) {
							text = text || '';
							text = text.split('#').join('&#35;');

							return '<a class="inner-link" ' +
								'title="inner link to ' + text.split('"').join('&quot;') + ' : ' + id
								+ '" data-id="' + id + '">节点: [ <span class="item-id">' + (text || id) + '</span> ]</a>'
						})
						.replace(that.rgxListItem, function (whole, id, itemId, title) {
							var title = title || '';
							title = title.split('#').join('&#35;');

							return '<a class="list-item-link"' +
								' title="link to ' + title + '"' +
								' data-list-id="' + id + '">列表: [ ' + title + ' ]</a>'
						});

					bodyHtml = utils.replaceTags(bodyHtml)
						.split('\\#').join('#');

					bodyHtml = bodyHtml
						.replace(that.rgxImage,
							"$1<a href='$2' class='embedded-image-link' target='_blank'>" +
							"<img src='$2' class='embedded-image' title='$2' /></a>")

						.replace(that.rgxHighlightLeft, '<a class="field">')
						.replace(that.rgxHighlightRight, '</a>')
						.replace(that.rgxHighlightLeftEscaped, '$1$2')
						.replace(that.rgxHighlightRightEscaped, '$1$2');

					bodyHtml = marked(bodyHtml);
				} else {
					bodyHtml = '<em>该节点无内容</em>';
				}
				return bodyHtml;
			},

			getStandaloneBody: function (content) {
				var t = this, bodyHtml = t.getBody(content);
				var $tmp = $('<div/>');
				$tmp.html(bodyHtml);
				t.parseTags($tmp);
				t.enhanceEmbeddeds($tmp);
				return $tmp;
			},

			enhanceEmbeddeds: function ($tmp) {
				var that = this, $item = $tmp;

				$item.find('a:not(.list-item-link):not(.embedded-image-link):not(.inner-link)')
					.each(function () {
						var $t = $(this), html = $t.html(), oldHtml = html;
						try {
							html = decodeURI(html);
						} catch (ex) {
						}

						if (html.length > CON_total) {
							var firstPart = html.substring(0, CON_limit);
							var secondPart = html.substr(-CON_limit);
							html = firstPart + '…' + secondPart;
							$t.text(html);
						} else if (html != oldHtml) {
							$t.text(html);
						}

						$t.addClass('embedded-link')
							.attr('target', '_blank');
					});

				$item.find('.list-item-link').each(function () {
					var $link = $(this),
						link = location.origin + '/app/list/' + $link.data('list-id');

					$link.attr('href', link)
						.attr('target', '_blank');
				});

				$item.find('.emedded-image-link').attr('target', '_blank');

				$item.find('img')
					.each(function () {
						var $t = $(this);

						if (!$t.closest('.embedded-image-link').length) {
							$t.wrap('<a href="' + $t.attr('src') + '" class="embedded-image-link" target="_blank"></a>');
						}
					})
			}
		}

		var delayRunFns = {}, delayFnIdSeq = 0;
		_l.delayRun = _.delayRun = function (id, fn, delay) {
			_.delayFn(id, fn, delay)();
		}

		_l.delayFn = _.delayFn = function (id, fn, delay) {
			if (typeof delay == 'undefined') {
				delay = 250;
			}
			return function () {
				id = id || '_tmp_id_' + (delayFnIdSeq++);
				clearTimeout(delayRunFns[id]);
				var self = this, args = arguments;
				delayRunFns[id] = setTimeout(function () {
					fn.call(self, args);
				}, delay);
			}
		}

		_.cancelDelayRun = _.cancelDelayRun = function (id) {
			clearTimeout(delayRunFns[id]);
		}

		_.cancelDelayFn = _.cancelDelayFn = function (id) {
			return function () {
				clearTimeout(delayRunFns[id]);
			}
		}

		utils.hasFocus = true;
		$(window).focus(function () {
			utils.hasFocus = true;
		})
			.blur(function () {
				utils.hasFocus = false;
			});

		$(document).on('click', 'a', function () {
			var $a = $(this);
			utils.setRedirect($a.attr('target'));
		});

		return utils;
	})
