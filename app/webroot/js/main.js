require.config({
	baseUrl : '/js/',
	waitSeconds: 30,
	urlArgs: 'bust=v'
	/*<DEV*/
	 + 7// + (new Date()).getTime()
	/*DEV>*/
	 /*<PROD
	  + ##APP_DEPLOY_TIME##
	  PROD>*/
	,
	paths : {
		text : 'lib/text',
		utils : 'lib/utils',
		jquery : 'lib/jquery-2.1.0.min',
		backbone : 'lib/backbone-min',
		underscore : 'lib/underscore-min',
		lodash: 'lib/lodash',
		less : 'lib/less.min',

		'original-marked': 'lib/marked',
		marked: 'lib/marked-wrapper',

		highlight: 'lib/highlight/highlight.min',
		'tab-indent': 'lib/tab-indent',

		'item-processor': 'comps/item-processor',
		markdown: 'comps/markdown',
		slides: 'comps/slides',

		'notification-utils': 'comps/notification-utils',

		'bookmark-scheduler': 'comps/bookmark-scheduler',
		pickmeup: 'lib/pickmeup/pickmeup',

		tutorial: 'lib/tutorial',
		'tutorial-loader': 'comps/tutorial-loader',

		unslider : 'lib/unslider.min',
		/* http://github.hubspot.com/vex/ */
		vex : 'lib/vex/js/vex',
		'vex-dialog' : 'lib/vex/js/vex.dialog.min',
		'vex-loader' : 'lib/vex/js/vex-loader',
		ajax : 'lib/ajax',
		spin : 'lib/spin.min',
		spinjQuery : 'lib/spin',
		libVideo: 'lib/videojs/video',
		buzz: 'lib/buzz.min',
		'fs-player': 'lib/fs-player',
		noty:'lib/jquery.noty.packaged',
		mousetrap: 'lib/mousetrap.min',
		keymap: 'lib/keymap',
		'help-content': 'lib/help-content',
		'md5': 'lib/md5.min',
        'TweenLite': 'lib/greensock-js/src/uncompressed/TweenLite',
        'draggable': 'lib/greensock-js/src/uncompressed/utils/Draggable',
        'CSSPlugin': 'lib/greensock-js/src/uncompressed/plugins/CSSPlugin',
        //'CSSPlugin': 'lib/greensock-js/src/minified/plugins/CSSPlugin.min',
        'item': 'comps/item',
        'item-list': 'comps/item-list',
        'saved-search': 'comps/saved-search',
        'syncer': 'comps/syncer',
        'key-manager': 'comps/key-manager',
        'header-dropdown': 'comps/header-dropdown',
        'tutorial-open': 'tutorial/tutorial',
        'moment': 'lib/moment.min',
        'window-messenger': 'comps/window-messenger',
        'aui-dropdown2': 'lib/aui/dropdown2',
        'spinner': 'lib/aui/spin',
       	'jquery-ui': 'lib/jquery-ui-1.11.2.custom/jquery-ui.min'
	},
	shim : {
		'unslider' : {
			deps : [ 'jquery' ]
		},
        'draggable': {
            deps: ['TweenLite', 'CSSPlugin'],
            exports: 'Draggable'
        },
		"spin" : {
			exports : "Spinner"
		},
		'noty': {
			deps: ['jquery'],
			exports: 'noty'
		}
	}
});

window.CONs = {
	isDebug: true
}
