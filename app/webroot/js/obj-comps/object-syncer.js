define(['utils', 'jquery', 'underscore', 'text!tpls/objects.tpl', 'md5',
        'window-messenger', 'backbone', 'ajax'],
    function(utils, $, _, tplStr, md5,
             messener, Backbone, ajax,
             undefined) {



    var CON_syncInterval = 2000, CON_maxOps = 20;
    var AjaxQueue = function() {
        var t = this;

        setInterval(function() {
            var now = new Date().getTime();
            if (now - t._lastSync > CON_syncInterval) {
                t.sync();
            }
        }, 1000);
    };
    AjaxQueue.prototype = {
        _queue: [],
        load: function(id, callback) {
            var t = this;
            t._queue.push({id: id, callback: callback, method: 'get'});
        },
        save: function(id, raw, callback, force) {
            var t = this;
            t._queue.push({id: id, raw: raw, callback: callback, method: 'set'});
        },
        _lastSync: 0,
        _syncing: false,
        sync: function(force) {
            var t = this;
            if (t._queue.length && !t._syncing || force) {
                var ops = {};
                _.each(t._queue, function(op) {
                    if (ops[op.id] && ops[op.id].method == 'set' && op.method == 'get') return;
                    ops[op.id] = op;
                });

                var len = _.reduce(ops, function(len, i) {return len + 1}, 0);
                for (var id in ops) {
                    if (len <= CON_maxOps) break;
                    delete ops[id];
                    len --;
                }

                var tks = token.getReqParam();

                ajax.post('object_sync', {ops: JSON.stringify(ops), tokens: tks.all, use: tks.inUse}, {
                    ok: function(result) {
                        _.each(result, function(obj) {
                            for (var i = t._queue.length; i-- > 0;) {
                                var op = t._queue[i];
                                if (obj.id == op.id) {
                                    op.callback && op.callback(obj.raw);
                                    t._queue.splice(i, 1);
                                }
                            }
                        });
                    },
                    error: function() {
                        _.each(ops, function(op) {
                            for (var i = t._queue.length; i-- > 0;) {
                                var opInQueue = t._queue[i];
                                if (op.id == opInQueue.id) {
                                    t._queue.splice(i, 1);
                                }
                            }
                        });
                    },
                    last: function() {
                        t._lastSync = new Date().getTime();
                        t._syncing = false;
                    }
                });
            }
        }
    }

        return AjaxQueue;
})