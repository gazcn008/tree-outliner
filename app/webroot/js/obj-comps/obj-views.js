define(['utils', 'jquery', 'underscore', 'text!tpls/objects.tpl', 'md5',
        'window-messenger', 'backbone', 'jquery-ui', 'lib/behave', 'keymap'],
    function(utils, $, _, tplStr, md5,
             messener, Backbone, _jqueryUI, Behave, keymap,
             undefined) {
        'use strict';

    var tpls = utils.parseTpls(tplStr);

    var lastPos = {left: 0, top: 0},
        CON_viewStepX = 15, stepCountX = 1,
        CON_viewStepY = 8, stepCountY = 1,
        CON_maxTop = 8 * CON_viewStepY,
        CON_defaultWidth = 600;


    var View = Backbone.View.extend({
        className: 'view',
        events: {
            'mousedown': 'onFocus',
            'click .handle .close': 'close',
            'blur input,textarea': 'onBlurOnText'
        },
        onBlurOnText: function(e) {
            this.$lastText = $(e.currentTarget);
        },
        search: function() {

        },
        initialize: function(opts) {
            var t = this;
            _.extend(t, opts);
        },
        render: function(title) {
            var t = this;

            t.$el.html(tpls.view({
                title: title
            }));


            t.$handle = t.$('.handle');
            t.$content = t.$('.content');

            setTimeout(function() {
                lastPos.left = CON_viewStepX * (stepCountX ++) + ((stepCountX+1) % 2) * CON_defaultWidth ;
                lastPos.top = CON_viewStepY * (stepCountY ++);
                if (lastPos.top > CON_maxTop) {
                    stepCountX = stepCountY = 0;
                }
                t.$el.draggable({
                    handle: '.handle',
                    stop: function() {
                        var pos = t.$el.position();
                        pos.left = pos.left < 0 ? 0 : pos.left;
                        pos.top = pos.top < 0 ? 0 : pos.top;
                        t.$el.css(pos);
                    }
                })
                    .css('position', 'absolute')
                    .css({
                        left: lastPos.left,
                        top: lastPos.top
                    })
                    .find('.content-wrapper')
                    .resizable({
                        handles: "se",
                        start: function() {
                            t.$el.css({
                                width: 'auto',
                                height: 'auto'
                            });
                        },
                        resize: _.throttle(function() {
                            t.$handle.width(t.$content.outerWidth() - 20)
                        }, 200, {trailing: true})
                    });
            });

            t.focus();
            return this.$el;
        },
        onFocus: function() {
            this.focus(true);
        },
        $lastFocus: null,
        focus: function(skipAppend) {
            var t = this;
            t.$el.css('z-index', nextZIndex());
            if (!skipAppend) {
                if (!t.$el.is(':visible')) $body.append(t.$el);
                if (t.$lastText) t.$lastText.focus();
//                else t.$('input').eq(0).focus();

                if (navigator.userAgent.search("Safari") >= 0 &&
                    navigator.userAgent.search("Chrome") < 0)
                {
                    var $c = t.$('.content-wrapper');
                    $c.height($c.height() + 1);
                }
            }
        },
        isRemoved: function() {
            return !this.$el || !$.hasData(this.$el[0]);
        },
        close: function() {
            var t = this;
            t.$el.remove();

        }
    });

    var objectViews = {}, zIndex = 0,
        CON_maxZIndex = 999000,
    nextZIndex = function() {
        zIndex += 5;
        if (zIndex > CON_maxZIndex) {
            var $views = $('.view').sort(function(a, b) {
                var $a = $(a), $b = $(b);
                var za = ~~$a.css('z-index'), zb = ~~$b.css('z-index');
                return za - zb;
            });

            zIndex = 0;
            _.each($views, function(v) {
                zIndex += 5;
                $(v).css('z-index', zIndex);
            });
        }
        zIndex += 5;
        return zIndex;
    };

    var ObjectView = View.extend({
        events: _.extend({
            'focus textarea.function-content': 'onFunctionTextareaSetup',
            'focus textarea.long-text': 'onLongTextTextareaSetup',
            'blur textarea': 'onTextareaBlur',
            'click .actions .open': 'onOpenObject',
            'click .actions .execute': 'onExecuteFunction',
            'click .actions .switch': 'onSwitchLongText',

            'click .actions .remove': 'onItemRemove',

            'keyup input,textarea': 'onKeyupOnText',
            'blur input,textarea': 'onBlurOnText',
            'keyup input.basic-value': 'onKeyupOnBasicValue',

            'keyup textarea': 'onTextareaChange'
        }, View.prototype.events),

        onSwitchLongText: function(e) {
            var $t = $(e.currentTarget).closest('.field'),
                $basicValue = $t.find('> .value .basic-value'),
                $blocks = $t.find('> .block');
            var $longTextBlock = $blocks.filter('.long-text'),
                $longText = $longTextBlock.find('textarea');
            if ($longTextBlock.length) {
                var isActive = $longTextBlock.is('.active');
                if (isActive) {
                    $basicValue.val("'" + $longText.val());
                } else {
                    $longText.val($basicValue.val().substr(1));
                    $basicValue.val("'");
                }
                $longTextBlock.toggleClass('active');

            } else {
                $longTextBlock = $(tpls.longTextBlock({content: $basicValue.val().substr(1)}));
                $basicValue.val("'");
                $t.append($longTextBlock);
            }
        },

        onItemRemove: function(e) {
            var $t = $(e.currentTarget).closest('.field');
            var obj = $t.data('object'), key = $t.data('key');

            delete obj[key];
            $t.children('.block').remove();
            if ($t.siblings('.field').length) {
                $t.remove();
            } else {
                $t.find('input').val('');
            }
        },

        onExecuteFunction: function(e) {
            var $t = $(e.currentTarget).closest('.field');
            var obj = $t.data('object'), key = $t.data('key');

            if (obj && obj[key])  {
                obj[key]();
            } else {
                alert('Function not found');
            }
        },

        onKeyupOnBasicValue: function(e) {
            if (e.keyCode == keymap.enter) {
                var t = this;
                var $field = $(e.currentTarget).closest('.field'),
                    obj = $field.data('object'), key = $field.data('key');
                if (obj instanceof Array) {
                    key = ~~key;
                    obj.splice(key+1, 0, undefined);
                    var $new = t._newObjectField(obj, key);
                    $field.after($new);
                    $field.nextAll().each(function() {
                        var $t = $(this), key = ~~$t.data('key');
                        key = key + 1;
                        $t.find(' > .value .key').html(key);
                        $t.data('key', key);
                    });
                } else {
                    $new = t._newObjectField(obj)
                    $field.after($new);
                }
                $new.find('input').eq(0).focus();
            }
        },

        onKeyupOnText: function(e) {
            var t = this;
            var $field = $(e.currentTarget).closest('.field'), $value = $field.find('> .value'),
                $basicVal = $value.find('.basic-value'),
                basicValue = $basicVal.val().replace(/^\s*/, ''),
                $label = $value.find('label'),
                obj = $field.data('object'),
                $key = $label.length ? $label : $value.find('.key'),
                key = $key.val() || $key.html(),
                $blocks = $field.children('.block'),
                nKey = key;

            $blocks.removeClass('active');

            t.switchValue(basicValue, {
                stringValue: function(val) {
                    $field.attr('field-type', 'string');
                    var $longText = $blocks.filter('.long-text');
                    if ($longText.length) {
                        $longText.addClass('active');
                    }
                },
                myObjectValue: function() {
                    $field.attr('field-type', 'my-object');
                },
                trueValue: function() {
                    $field.attr('field-type', '');
                },
                falseValue: function() {
                    $field.attr('field-type', '');
                },
                nullValue: function() {
                    $field.attr('field-type', '');
                },
                functionValue: function() {
                    $field.attr('field-type', 'function');
                    var $function = $blocks.filter('.function');
                    if ($function.length) {
                        $function.addClass('active');
                    } else {
                        $(tpls.functionBlock({args: '', content: ''}))
                            .appendTo($field);
                    }
                },
                undefinedValue: function() {
                    $field.attr('field-type', '');
                    return 'all';
                },
                objectValue: function() {
                    $field.attr('field-type', '');
                    var $object = $blocks.filter('.object');
                    if ($object.length) {
                        $object.addClass('active');
                    } else {
                        t._renderObjectBlock($field, {'': undefined});
                    }
                },
                numberValue: function(number) {
                    $field.attr('field-type', '');
                },
                arrayValue: function() {
                    $field.attr('field-type', '');
                    var $array = $blocks.filter('.array');
                    if ($array.length) {
                        $array.addClass('active');
                    } else {
                        t._renderObjectBlock($field, [undefined]);
                    }
                },
                otherValue: function() {
                    $field.attr('field-type', 'string');
                    var $longText = $blocks.filter('.long-text');
                    if ($longText.length) {
                        $longText.addClass('active');
                    }
                }
            });

            delete obj['_o_' + key];
            delete obj['_f_' + key];
            delete obj['_fold_' + key];
        },

        onBlurOnText: function(e) {
            ObjectView.__super__.onBlurOnText.apply(this, arguments);

            var t = this, $t = $(e.currentTarget).closest('.field'),
                obj = $t.data('object');

            var oldKey = $t.data('key'), $key = $t.find('> .value .key'),
                newKey = $key.val() || $key.html();
            obj[newKey] = obj[oldKey];
            delete obj[oldKey];
            delete obj['_o_' + oldKey];
            delete obj['_f_' + oldKey];
            delete obj['_fold_' + oldKey];
            $t.data('key', newKey);
            var data = t.wrapData($t);
            _.extend(obj, data);
            t.obj.set();
        },

        switchValue: function(basicValue, callbacks) {
            var firstChar = basicValue.substr(0, 1),
                firstTwoChars = basicValue.substr(0, 2);

            if (firstChar == "'" || firstChar == '"') {
                // is string
                return callbacks.stringValue(basicValue.substr(1));
            } else if (firstChar == '=') {
                // is object link
                return callbacks.myObjectValue(basicValue.substr(1));
            } else if (basicValue == 'true') {
                // is boolean true
                return callbacks.trueValue();
            } else if (basicValue == 'false') {
                // is boolean false
                return callbacks.falseValue();
            } else if (basicValue == 'null') {
                return callbacks.nullValue();
            } else if (basicValue == 'function') {
                return callbacks.functionValue();
            } else if (basicValue == '') {
                return callbacks.undefinedValue();
            } else if (firstTwoChars == '{}') {
                return callbacks.objectValue();
            } else if (firstTwoChars == '[]') {
                return callbacks.arrayValue();
            } else {
                var number = Number(basicValue);
                if (isNaN(number)) {
                    return callbacks.otherValue();
                } else {
                    return callbacks.numberValue(number);
                }
            }
        },

        onTextareaChange: _.throttle(function(e) {
            this.resetTextareaHeight($(e.currentTarget));
        }, 800),

        onTextareaChangeFromBehave: _.throttle(function(textarea) {
            this.resetTextareaHeight($(textarea));
        }, 800),

        resetTextareaHeight: function($textarea) {
            var val = $textarea.val(), CON_lastEmpty = 2;
            var lines = val.split('\n'), lastCount = 0,
                mch = _.reduce(_.map(lines, function(l) {
                            lastCount = l ? 0 : lastCount + 1;
                            return Math.ceil((l||' ').length / 70)
                        }),
                    function(total, lCount) {
                        return total + lCount
                    }, 0);
//            mch -= lastCount;
            $textarea.css('height', mch * 14 + 30);
            if (lastCount < CON_lastEmpty) {
                var text = $textarea[0], start = text.selectionStart, end = text.selectionEnd;
                $textarea.val(val + (new Array(CON_lastEmpty + 1).join('\n')));
                text.selectionStart = start;
                text.selectionEnd = end;
            }
        },

        wrapData: function($fields, container, extra) {
            var t = this;
            $fields = $fields|| t.$('.object-content > .block > .field');
            container = container || {};

            $fields.each(function() {
                var $field = $(this), $value = $field.find('> .value');
                var $basicVal = $value.find('.basic-value'),
                        basicVal = $basicVal.val().replace(/^\s*/, ''),
                    $label = $value.find('label'),
                    obj = $field.data('object'),
                    $key = $label.length ? $label : $value.find('.key'),
                    key = $key.val() || $key.html(),
                    nKey = key;

                var value = t.switchValue(basicVal, {
                    stringValue: function(val) {
                        var $textarea = $field.find('> .block.active textarea');
                        if ($textarea.length) {
                            return $textarea.val();
                        } else {
                            return val;
                        }
                    },
                    myObjectValue: function(val) {
                        nKey = '_o_' + nKey;
                        if (obj[key]) {
                            obj[key] = app.get(key);
                            if (!obj[key]) {
                                app.load(key, function(e, res) {
                                    obj[key] = res;
                                });
                            }
                        }
                        return val;
                    },
                    trueValue: function() {
                        return true;
                    },
                    falseValue: function() {
                        return false;
                    },
                    nullValue: function() {
                        return null;
                    },
                    functionValue: function() {
                        nKey = '_f_' + nKey;
                        var args = $field.find('.function-args').val(),
                            content = $field.find('.function-content').val();
                        t.obj.decodeFunc(obj, key, args, content);
                        return args + '|' + content;
                    },
                    undefinedValue: function() {
                        return undefined;
                    },
                    objectValue: function() {
                        var $childField = $field.find('> .block.active > .field').eq(0),
                            childObj = $childField.data('object');
                        return childObj || {};
                    },
                    numberValue: function(number) {
                        return number;
                    },
                    arrayValue: function() {
                        var $childField = $field.find('> .block.active > .field').eq(0),
                            childObj = $childField.data('object');
                        return childObj || [];
                    },
                    otherValue: function() {
                        // assume all others will be string
                        $basicVal.val("'" + basicVal);
                        return basicVal;
                    }
                });

                container[nKey] = value;
            });

            return _.extend(container, extra);
        },

        onOpenObject: function(e) {
            var $t = $(e.currentTarget).closest('.value');
            var val = $t.find('.basic-value').val();
            var id = val.substr(1);
            var target = app.get(id);

            if (!target) {
                app.load(id, function(e, obj) {
                    if (e) {
                        alert('object cannot be found. error occured : ' + errors(e));
                    } else {
                        obj.open();
                    }
                })
            } else {
                target.open();
            }
        },
        onFunctionTextareaSetup: function(e) {
            this.textareaSetup(e.currentTarget);
        },
        onLongTextTextareaSetup: function(e) {
            this.textareaSetup(e.currentTarget, true);
        },
        onTextareaBlur: function(e) {
            $(e.currentTarget).off('.scroll-restriction')
        },
        textareaSetup: function(textarea, isLongText) {
            var t = this, $t = $(textarea);

            $t.on('mousewheel.scroll-restriction DOMMouseScroll.scroll-restriction', function(e) {
                var text = this;

                var e0 = e.originalEvent,
                    delta = e0.wheelDelta || -e0.detail;

                if (text.scrollHeight <= this.clientHeight) return;

                text.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
                e.preventDefault();
            });

            if ($t.data('editor')) return;

            if (isLongText) {
                var editor = new Behave({
                    textarea: textarea,
                    tabSize: 2,
                    replaceTab: true,
                    softTabs: true,
                    autoStrip: false,
                    autoIndent: false,
                    rememberIndent: true
                });
                $t.data('editor', editor);
            } else {
                editor = new Behave({
                    textarea: textarea,
                    tabSize: 2,
                    autoIndent: false,
                    rememberIndent: true
                });
                $t.data('editor', editor);
            }

        },
        initialize: function(opts) {
            var t = this;
            opts = opts || {};

            View.prototype.initialize.call(this, opts);
            View.prototype.render.call(t, t.obj.name() + '@' + t.obj.space() + ' {' +  t.obj.id() + '}');
            t.renderObject();

            t.search();
        },
        _bind: function(object, key, cache) {
            var t = this;
            cache.seq = cache.seq ? cache.seq + 1 : 1;
            cache[cache.seq] = object;
            return ' data-bound-obj="' + cache.seq + '" data-key="' + key + '" ';
        },
        _bindToDom: function($target, cache) {
            $target.find('[data-bound-obj]')
                .add($target.filter('[data-bound-obj]'))
                .each(function() {
                    var $t = $(this);
                    var obj = $t.data('bound-obj');
                    $t.data('object', cache[obj]);
                    $t.removeAttr('data-bound-obj');
                });
        },
        renderObject: function() {
            var t = this;
            this.$content.html(tpls.objectViewContent({}));
            this._renderObjectBlock(t.$('.object-content'), this.obj.d);

            t.$('.object-content').find('textarea').each(function() {
                t.resetTextareaHeight($(this));
            });
        },
        _renderObjectBlock: function($field, obj) {
            var t = this, cache = {};
            $field.append(tpls.objectBlock({
                val: obj,
                bind: function(obj, k) {
                    return t._bind(obj, k, cache);
                },
                isArray: obj instanceof Array
            }));
            t._bindToDom($field, cache);
        },
        _newObjectField: function(obj, k) {
            var t = this, cache = {};
            var $field = $(tpls.plainField({
                obj: obj,
                k: k || '',
                v: undefined,
                bind: function(obj, k) {
                    return t._bind(obj, k, cache);
                },
                isArray: obj instanceof Array
            }));
            t._bindToDom($field, cache);
            return $field;
        }
    })


        var lastSpacename = '';
    /**
     * _v_
     * _f_
     * _o_
     * _a_
     *
     */
    var ListView = View.extend({
        events: _.extend({
            'keyup .alias,.name,.space': 'onSearch',
            'click .more .refresh': 'onSearch',
            'click .more .remote:not(.disabled)': 'onSearchRemote',
            'click .more .local:not(.disabled)': 'onSearchLocal',
            'click .more .create': 'onCreateNew',
            'click td.name': 'openObject'
        }, View.prototype.events),

        onCreateNew: function() {
            var newObj = app.newObj().set();
            newObj.open();
            this.refresh();
        },

        refresh: function() {
            this.onSearch();
        },

        openObject: function(e) {
            var $tr = $(e.currentTarget).closest('.meta');
            var id = $tr.data('id');
            var obj = app.get(id);
            if (obj) obj.open();
        },
        initialize: function(opts) {
            var t = this;
            opts = opts || {};
            View.prototype.initialize.call(this, opts);
            View.prototype.render.call(t, 'Object list');
            t.$menu = t.$('.menu-wrapper');
            t.renderList();

            t.$search = t.$('.search');
            t.$aliasText = t.$('.text.alias');
            t.$nameText = t.$('.text.name');
            t.$spaceText = t.$('.text.space');

            t.$results = t.$('.results tbody');

            t.$searchRemote = t.$('.more .remote');
            t.$searchLocal = t.$('.more .local')

            t.search();
        },
        _shortcutSeq: 0,
        renderList: function() {
            var t = this;
            t.$menu.html(tpls.listMenu());
            this.$content.html(tpls.list());
        },
        onSearch: _.throttle(function() {
            var t = this;


            t._remoteOffset = 0;
            t._shortcutSeq = 0;
            t._searchReqSeq = t._searchReqSeq > 9999000 ? 0 : t._searchReqSeq + 1;

            t.search();
        }, 1000),
        onSearchLocal: function() {
            this.search('local', true);
        },
        onSearchRemote: function() {
            this.search('remote', true);
        },
        _localOffset: 0,
        _remoteOffset: 0,
        _searchReqSeq: 0,
        _appendResult: function(results) {
            var t = this;
            _.each(results, function(res) {
                var $res = t.$results.children('[data-id=' + res.id + ']');
                if ($res.length) {
                    $res.addClass('found');
                } else {
                    var sh = (t._shortcutSeq++).toString(36);
                    shortcuts['$_' + sh] = res.id;
                    shortcuts['$' + sh] = function () {
                        return this.get(sh)
                    };
                    t.$results.append(tpls.searchResult(_.extend({shortcut: sh}, res)));
                }
            });
        },
        search: function(type, skipEmpty) {
            var t = this;
            type = type || 'local';


            var alias = $.trim(t.$aliasText.val()),
                name = $.trim(t.$nameText.val()),
                space = $.trim(t.$spaceText.val());

            if (!skipEmpty) {
                t.$results.empty();
            }

            if (type == 'remote') {
                var reqSeq = t._searchReqSeq;
                t.$searchRemote.addClass('disabled');
                app.searchRemote(name, space, t._remoteOffset, function(results) {
                    t.$searchRemote.removeClass('disabled');
                    if (reqSeq == t._searchReqSeq) {
                        t._appendResult(results);
                    }
                });
                t._remoteOffset += CON_searchNum;
            } else {
                t._appendResult(app.searchLocal(name, space, alias,
                    _.invert(_.map(t.$results.children('.meta'), function(tr) {return $(tr).data('id')}))
                ));
            }
        },
        more: function() {

        }
    });

        var app, CON_searchNum, $body, errors;
        return {
            init: function(_app, _CON_searchNum, _$body, _errors) {
                app = _app;
                CON_searchNum = _CON_searchNum;
                $body = _$body;
                errors = _errors;
            },
            ListView: ListView,
            ObjectView: ObjectView
        };
})