define(['utils', 'jquery', 'underscore', 'text!tpls/objects.tpl', 'backbone', 'ajax', 'jquery-ui',
        'obj-comps/obj-views'],
    function(utils, $, _, tplStr, Backbone, ajax, _jqueryUI,
             ViewsIniter,
             undefined) {

        var ObjectView = ViewsIniter.ObjectView;

    var MyObject = function(raw) {
        var t = this;
        t.d = raw;
        t._is_mo_ = true;
        if (typeof t.d._json_ == 'string') {
            try {
                var json = JSON.parse(t.d._json_);
                t.d = _.extend(t.d, json, _.extend({}, t.d));
            } catch (ex) { }
        } else {
            t.d = _.extend(t.d, t.d._json_, _.extend({}, t.d));
        }
        t.decode();
        t.local = {};
        delete t.d._json_;
    }
    var myObjGlobal = {};
    MyObject.prototype = {
        global: myObjGlobal,
        local: null,
        attr: function(key) {

        },
        _linkings: [],
        /**
         * opts: {}
         *  level: *'one', 'all'
         *  type: 'cache', *'mix', 'remote'
         *  target: object, *undefined
         *  verbose: *true || false
         *
         * @param opts
         */
        resolve: function(opts, callback) {
            if (typeof opts == 'function') {
                callback = opts;
                opts = null;
            }

            opts = _.extend({
                level: 'one',
                type: 'mix',
                verbose: true,
                target: this.d,
                callback: callback,
                total: 0
            }, opts);

            opts.cache = {};

            this._res(opts.target, opts);
            if (!opts.total) {
                opts.callback && opts.callback();
            }
        },
        _loadTarget: function(obj, nk, c, opts, callback) {
            if (c == this.id()) {
                obj[nk] = this;
                return true;
            }

            var cache = opts.cache[c];
            if (cache instanceof Array) {
                opts.verbose && console.log('>>> Pushed to the loading queue at ' + nk, obj);
                cache.push(function(target) {
                    obj[nk] = target;
                    callback(target);
                });
                return true;
            } else if (cache) {
                opts.verbose && console.log('>>> Target already resolved, loaded directly at ' + nk, obj);
                obj[nk] = opts.cache[c];
                return true;
            }

            opts.verbose && console.log('>>> Loading dependency at ' + nk, obj);
            opts.cache[c] = [];
            app.load(c, function(e, target) {
                if (!e) {
                    obj[nk] = target;
                    opts.verbose && console.log('<<< Finish loading dependency at ' + nk + ' with id#' + c, obj);
                    var cache = opts.cache[c];
                    if (cache instanceof Array) {
                        while (cache.length) {
                            cache.pop()(target);
                        }
                    }
                    opts.cache[c] = target;
                    callback && callback(target);
                } else {
                    console.log('!!! ERROR: Could not load dependency at ' + nk + ' with id#' + c, errors(e), obj);
                }
                opts.total --;
                if (!opts.total) {
                    opts.callback && opts.callback();
                }
            });
        },
        _res: function(obj, opts) {
            if (!obj || typeof obj != 'object' || obj._v_) return;
            var t = this;
            obj._v_ = true;

            for (var k in obj) {
                if (!obj.hasOwnProperty(k)) continue;
                var c = obj[k];

                if (k.substr(0, 3) == '_o_') {
                    var nk = k.substr(3);

                    if (opts.type == 'remote') {
                        var res = t._loadTarget(obj, nk, c, opts, function(curr) {
                            if (opts.level == 'all') {
                                t._res(curr.d, opts);
                            }
                        });
                        if (!res) opts.total ++;
                    } else {
                        var target = app.get(c);
                        if (target) {
                            obj[nk] = target;
                            if (opts.level == 'all') {
                                t._res(target.d, opts);
                            }
                        } else if (opts.type == 'mix') {
                            var res = t._loadTarget(obj, nk, c, opts, function(curr) {
                                if (opts.level == 'all') {
                                    t._res(curr.d, opts);
                                }
                            });
                            if (!res) opts.total ++;
                        } else {
                            console.log('!!! WARNING: id#' + c + ' cannot be found for key#' + nk, obj);
                        }
                    }
                } else if (!(c instanceof MyObject)) {
                    t._res(c, opts);
                } else if (opts.level == 'all') {
                    t._res(c.d, opts);
                }
            }

            delete obj._v_;
        },
        decodeFunc: function(obj, key, args, content, force) {
            var t = this;
            try {
                var funcStr = args + '|' + content;
                obj['_f_' + key] = funcStr;
                if (!force && obj['_fold_' + key] == funcStr && obj[key]) return;
                obj['_fold_' + key] = funcStr;
                obj[key] = _.bind(new Function(args,
                        "'use strict';var localStorage={},alias={},window={},token={},top={},self={},parent={},t=this;\n"
                        + content), t);
            } catch (ex) {
                console.log('error found when parsing the method - ' + key, content, ex);
                obj[key] = undefined;
            }
        },
        url: function(method) {
            var args = Array.prototype.slice.call(arguments, 1);
            return '/objects/o/' + this.id() + '/' + decodeURIComponent(method) + '/' +
                (args.length ? _.map(args, function(arg) {return decodeURIComponent(arg);}).join('/')
                : '');
        },
        decode: function(target) {
            this._decode(target, target ? true : false, true);
        },
        _decode: function(val, hasVal, force) {
            var t = this;
            val = hasVal ? val : t.d;

            if (val && val._a_) {var newVal = []}
            else newVal = val;

            if (newVal && typeof newVal == 'object' && (!newVal._v_ || !hasVal)) {
                newVal._v_ = true;

                for (var k in val) {
                    if (!val.hasOwnProperty(k)) continue;
                    var c = val[k];

                    if (k.substr(0, 3) == '_f_') {
                        var nk = k.substr(3);
                        var prts = c.split(/\|/), args = prts.length > 1 ? prts.shift() : '', content = prts.join('|');
                        t.decodeFunc(newVal, nk, args, content, force);
                    } else if (k.substr(0, 3) == '_o_') {
                        var nk = k.substr(3);
                        newVal[k] = c;
//                                t._linkings.push((function(newVal, nk, c) {
//                                    return function() {newVal[nk] = app.get(c)}
//                                })(newVal, nk, c));
                    } else if (k != '_v_' && k != '_a_') {
                        if (!val || !val._a_ || !(k in newVal)) {
                            if (c && typeof c == 'object' && !c._is_mo_) {
                                newVal[k] = t._decode(c, true);
                            } else {
                                newVal[k] = c;
                            }
                        }
                    }
                }

                delete newVal._v_;
            }

            if (!hasVal) {
                setTimeout(function() {
                    while (t._linkings.length) {
                        t._linkings.pop()();
                    }
                });
            }
            return newVal;
        },
        encode: function(val, key) {
            var t = this;
            if (val && typeof val == 'object' && !val._v_) {
                val._v_ = true;

                if (val instanceof Array) {
                    var res = {_a_: true}
                } else {
                    res = {};
                }

                for (var k in val) {
                    if (!val.hasOwnProperty(k)) continue;
                    var c = val[k];
                    if (c instanceof MyObject) {
                        res['_o_' + k] = c.id();
                    } else {
                        var ch = t.encode(c, k);
                        if (typeof ch != 'undefined' && k != '_v_' || (k.substr(0, 3) == '_o_' && !res[k])) {
                            res[k] = ch;
                        }
                    }
                }

                delete val._v_;
                return res;
            } else if (typeof val != 'function') {
                return val;
            }
        },
        raw: function() {
            var obj = this, t = this;
            var json = _.omit(obj.d, '_name_', '_space_', '_descr_', 'id', '_json_', '_readonly_', '_public_edit_',
                '_owned_', '_local_', '_v_',
                'created', 'updated');
            var raw = _.extend({}, obj.d, {_json_: JSON.stringify(t.encode(json))});
            raw = _.pick(raw, '_name_', '_space_', '_descr_', 'id', '_json_', '_readonly_', '_public_edit_', '_owned_',
                'created', 'updated');

            return raw;
        },
        set: function(skipTouch) {
            app.set(this, false, true);
            return this;
        },
        reset: function() {
            var obj = app.get(this.id(), null, true);
            if (!obj) {
                console.log('The local copy has been destroyed. for id#' + this.id());
            } else {
                this.d = obj.d;
            }
            return this;
        },
        load: function(callback) {
            var t = this;
            app.load(this.id(), function(err, obj) {
                if (err) {
                    console.log('error for id#' + t.id() + ' ' + errors(err));
                } else {
                    t.d = obj.d;
                }

                callback && callback(err, obj);
            });
            return this;
        },
        save: function(callback) {
            var t = this;
            app.save(this, function(err, obj) {
                if (err) {
                    console.log('error for id#' + t.id() + ' ' + errors(err));
                }

                callback && callback(err, obj);
            });
            return this;
        },
        id: function() {
            return this.d.id;
        },
        _attr_: function(key, val) {
            if (!val.length) return this.d[key] || '';
            else {
                this.d[key] = val[0];
                app.lazyset(this);
                return this;
            }
        },
        name: function(name) {
            return this._attr_('_name_', arguments);
        },
        space: function(space) {
            return this._attr_('_space_', arguments);
        },
        descr: function(space) {
            return this._attr_('_descr_', arguments);
        },
        readonly: function() {},
        open: function() {
            var id = this.id();
            var view = objectViews[id];
            if (view && !view.isRemoved()) {
                objectViews[id].focus();
            } else {
                var obj = app.get(id);
                if (obj) {
                    objectViews[id] = new ObjectView({
                        obj: obj
                    });
                } else {
                    app.load(id, function(e, obj) {
                        if (e) {
                            alert('The object dose not exist.');
                        } else {
                            objectViews[id] = new ObjectView({
                                obj: obj
                            });
                        }
                    })
                }
            }
            return this;
        }
    }

        var app, errors, objectViews= [];
        return {
            init: function(_app, _errors) { app = _app; errors = _errors},
            MyObject: MyObject
        }
})