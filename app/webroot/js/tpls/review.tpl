---item
<div data-item="<%- d.itemId %>" data-list="<%- d.listId %>"
        data-review-type="<%- d.reviewType %>">
    <div>
        <div class="parent">
            <%= d.parent %>
        </div>
        <div class="item">
            <%= d.item %>
        </div>
        <div class="stats">
            <%= d.created ? '<label>Created</label> ' + d.created : '' %>
            <%= d.reviewed ? '<label>Reviewed</label> ' + d.reviewed : '' %>
            <%= d.reviewTimes ? '<label>Times</label> ' + d.reviewTimes : '' %>
        </div>
        <div class="children">
            <%= d.children %>
        </div>
    </div>
</div>

---list
<h2><%- d.title %></h2>

---itemWrapper
<div data-item="<%- d.itemId %>"></div>

---itemCount
<div class="item-stat">
    <h2>Nubmer of remaining items : </h2>
    <span class="number"><%- d.count > 999 ? '999+' : d.count %></span>
</div>