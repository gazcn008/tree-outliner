---view
<div>
    <div class="handle-background"></div>
    <div class="handle"><span class="title"><%- d.title %></span>
        <span class="fa fa-times close"></span>
    </div>
    <div class="menu-wrapper"></div>
    <div class="content-wrapper">
        <div class="content">
        </div>
    </div>
</div>

---listMenu
<div class="more">
    <span class="refresh">Refresh</span>
    <span class="local">Search local</span>
    <span class="remote">Search remote</span>
    <span class="create">Create new</span>
</div>

---list
<div class="search">
    <input class="alias text" placeholder="alias"/>
    <input class="name text" placeholder="name"/>
    <input class="space text" placeholder="space"/>
    <!--<span class="small-spinner"></span>-->
</div>
<table class="results">
    <thead>
    <th class="shortcut">sh</th>
    <th class="alias">alias</th>
    <th class="name">name</th>
    <th class="space">space</th>
    <th class="id">id</th>
    </thead>
    <tbody>

    </tbody>
</table>

---objectViewContent
<div class="object-content">
    <%= d.blockHtml %>
</div>

---objectBlock
<div class="block active <%- d.isArray ? 'array' : 'object' %>">
    <%

    for (var k in d.val) {
        var firstThree = k.substr(0,3);
        if (firstThree == '_o_') {
            // check if the obj is or not
    // debugger;
            var nk = k.substr(3), target = d.val[nk];
            if (!target || !target._is_mo_) {
                d.val[nk] = {_is_mo_: true, _id_: d.val[k]};
            }
        } if (firstThree == '_f_') {
            var nk = k.substr(3), v = d.val[k];
            var prts = v.split(/\|/), args = prts.length > 1 ? prts.shift() : '',
            content = prts.join('|');

            print(d.tpls.plainField({k: nk, v: 'function', isArray: d.isArray, actions: 'function',
                bind: d.bind, obj: d.val,
                blockHtml: d.tpls.functionBlock({args: args, content: content})
            }));
        }
    }

    var hasField = false;
    for (var k in d.val) {
        if (k.substr(0, 1) != '_' && k != 'id' && k != 'created' && k != 'updated' || k.substr(0,3) == '_f_') {
            hasField = true;
            break;
        }
    }
    if (!hasField) {
        if (d.val instanceof Array) {d.val[0] = undefined}
        else d.val[''] = undefined;
    }

    for (var k in d.val) {
        var v = d.val[k];
        var type = typeof v;
        k = '' + k;
        if (k.substr(0, 1) != '_' && k != 'id' && k != 'created' && k != 'updated') {
            if (type == 'string') {
                if (v.indexOf('\n') == -1 && v.length < 40) {
                    print(d.tpls.plainField({k: k, v: "'" + v, isArray: d.isArray,
                        bind: d.bind, obj: d.val,
                        actions: 'string'}));
                } else {
                    print(d.tpls.plainField({k: k, v: "'", isArray: d.isArray,
                        actions: 'string',
                        bind: d.bind, obj: d.val,
                        blockHtml: d.tpls.longTextBlock({content: v})}));
                }
            } else if (v && type == 'object' && !v._v_) {
                v._v_ = true;
                if (v instanceof Array) {
                    print(d.tpls.plainField({k: k, v: '[]', isArray: d.isArray,
                        bind: d.bind, obj: d.val,
                        blockHtml: d.tpls.objectBlock({val: v, isArray: true, bind: d.bind}) }));
                } else if (v._is_mo_) {
                    print(d.tpls.plainField({k: k, v: '=' + (v._id_ || v.id && v.id() || ''), isArray: d.isArray, actions: 'my-object',
                        bind: d.bind, obj: d.val
                    }));
                } else {
                    print(d.tpls.plainField({k: k, v: '{}', isArray: d.isArray, blockHtml: d.tpls.objectBlock({val: v, bind: d.bind}),
                        bind: d.bind, obj: d.val}));
                }
                delete v._v_;
            } else if (type != 'function') {
                print(d.tpls.plainField({k: k, v: v, isArray: d.isArray,
                    bind: d.bind, obj: d.val}));
            }
        }
    } %>
</div>

---functionBlock
<div class="block active function">
    <input class="function-args" value="<%- d.args %>" />
    <textarea wrap="off" class="function-content"><%- d.content %></textarea>
</div>

---plainField
<% // debugger; %>
<div class="field" <%= d.bind(d.obj, d.k) %> field-type="<%- d.actions %>" >
    <div class="value">
        <% if (d.isArray) { %>
        <label class="key <%- d.labelClass %>"><%- d.k %></label>
        <% } else { %>
        <input class="key <%- d.labelClass %>" value="<%- d.k %>" />
        <% } %>
        <input class="basic-value" value="<%- d.v %>" />
        <span class="actions">
            <span class="fa fa-retweet switch"></span>

            <span class="fa fa-eye open"></span>
            <span class="fa fa-folder-open-o specify"></span>
            <span class="fa fa-plus create"></span>

            <span class="fa fa-play execute"></span>

            <span class="fa fa-times remove"></span>
        </span>
    </div>
    <%= d.blockHtml ? d.blockHtml : '' %>
</div>

---longTextBlock
<div class="block active long-text">
    <textarea wrap="off" class="long-text"><%- d.content %></textarea>
</div>


---searchResult
<tr class="meta" data-id="<%- d.id %>">
    <td class="shortcut" data-shortcut="<%- d.shortcut %>"><%- d.shortcut %></td>
    <td class="alias"><%- d.alias %></td>
    <td class="name"><%- d.name %></td>
    <td><%- d.space %></td>
    <td class="id"><%- d.id %></td>
</tr>
<% if (d.descr) { %>
<tr class="descr">
    <td colspan="5" class="descr"><%- d.descr %></td>
</tr>
<% } %>