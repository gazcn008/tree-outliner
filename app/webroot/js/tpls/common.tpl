---item
<div class="item"><span class="status"><%= d.statusHtml %></span><span class="body"><%= d.bodyHtml %></span></div>
<ul class="children"></ul>

---itemForm
<div class="edit">
	<textarea></textarea>
	<div class="buttons">
		<span class="save action-icon fa-3 fa-check fa"></span>
		<span class="cancel action-icon fa-3 fa-times fa"></span>
	</div>
</div>

---emptyList
<li><div class="item empty-list">Please hover on this item and key 'R' to create your first item!</div></li>

---sidebarListItem
<a class="list-item"
	data-list-fav="<%- d.fav_seq === null ? false : true %>"
	data-list-id="<%- d.id %>" href="/app/list/<%- d.id %>">
	<span class="title"><%- d.title %></span>
	<span class="button">子视图中打开</span>
</a>

---sidebarListResult
<% d.forEach(function(list) {
	print(d.tpls.sidebarListItem(list));
}) %>

---sidebarNoListFound
<div class="list-item no-result">
	<span><i>没有找到匹配的文档</i></span>
</div>

---itemLink
<li><a data-item-id="<%- d.id %>"><%
	var max = 47;
	if (d.content) {
		if (d.content.length > max) var content = d.content.substr(0, max) + '...';
		else content = d.content;

		print(_.escape(content));
	} else {
		print('<em>Empty item</em>');
	}
%></a></li>

---itemStatus
<span class="twix">
	<span class="fa fa-caret-right collapsed"></span>
	<span class="fa fa-caret-down expanded"></span>
	<span class="fa item-dot"></span>
</span>
<span class="fa fa-star indexed"></span>
---ipsumText
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris quam nunc, sollicitudin eu rutrum vitae, suscipit a ante. Etiam vitae lorem at urna facilisis tincidunt. Pellentesque eget enim semper, rhoncus libero in, tincidunt mi. Cras vitae porta eros, rutrum dignissim risus. Morbi non lobortis neque. Nam vehicula leo non libero auctor pellentesque. Vivamus dictum sem feugiat sem scelerisque, ut semper libero sollicitudin. Cras vel commodo ipsum. Nullam suscipit ullamcorper augue, eu dapibus est eleifend a. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque dictum tortor non molestie molestie. Nulla facilisi.

Nunc aliquam velit id nibh dapibus sollicitudin. Praesent imperdiet id risus nec luctus. Vivamus mattis vel mi eu tempor. Integer quis malesuada libero. Aliquam sit amet iaculis arcu. Nunc tellus nibh, imperdiet non ultricies in, tincidunt non nulla. Nullam fringilla, neque in feugiat rhoncus, eros neque dapibus justo, vitae sollicitudin eros lectus pharetra dui. Curabitur fringilla urna at venenatis tempor. Duis ultrices velit neque, non suscipit nulla blandit nec. Aliquam eleifend tristique urna ornare placerat.

Morbi pretium eros et metus vestibulum dictum. Sed nec ligula massa. Suspendisse mollis mollis turpis, sed eleifend leo pharetra sed. Praesent at orci sit amet metus lacinia consectetur. Etiam eget nisl nunc. Vestibulum eleifend quam ultricies dolor aliquet imperdiet. Praesent quis faucibus turpis.

Praesent tincidunt velit at massa rhoncus, vel lacinia eros dignissim. Donec quis libero nec dolor ultricies ornare. Vestibulum hendrerit risus sapien, eget blandit mi suscipit nec. Praesent volutpat, tellus quis fermentum dictum, dolor turpis iaculis purus, ut hendrerit risus ligula eget magna. Fusce non facilisis tellus, et consectetur urna. Cras leo tortor, facilisis nec sapien vitae, tempus placerat nisl. Vivamus non neque nec risus egestas malesuada. Aliquam blandit non libero tempor tincidunt. Nulla facilisi. Duis pellentesque, nisi at ultricies sagittis, turpis ligula scelerisque ante, id adipiscing ligula odio quis nunc.

Quisque nulla libero, dignissim id dui eu, bibendum molestie purus. Phasellus varius nibh ante, sed ultricies orci dictum ut. Vestibulum laoreet dui sit amet massa hendrerit rutrum. Fusce consequat sem sapien. In hac habitasse platea dictumst. Duis dapibus purus id sem lobortis interdum. In varius nisi nec urna tincidunt rutrum. Lorem ipsum dolor sit amet, consectetur adipiscing elit.


---command
<li><span class="descr"><%- d.descr %></span>
<%
_.each(d.keys, function(key) {
	print(d.tpls.label(key));
})
%>
</li>


---label
<span class="aui-label"><%- d %></span>

---emptyItem
<li class="empty">没找到任何 Item.</li>

---listDialog
<% if (!d.forLink) { %>
<div class="search-list-wrapper">
	<div class="hint">文件列表对话框快捷键 `a a`</div>
	<input class="search-list" placeholder="打开一个文档, 或新建一个文档" />
</div>
<% } else { %>
<div class="search-list-wrapper">
    <div class="hint">文件链接创建对话框快捷键 `a e`</div>
    <div class="add-as-dict">
        <span class="fa fa-square-o"></span>
        <span class="fa fa-check-square-o"></span>
        Add the list as a dictionary
    </div>
    <input class="search-list" placeholder="选择一个文档以创建链接" />
</div>
<% } %>
<ul class="results">
    <% if (!d.forLink) {%>
	<li class="create-new">
        <a href="/app/list/new"><span class="title">未命名文档</span><span class="aui-lozenge aui-lozenge-complete new-tab">创建新文档</span></a>
    </li>
    <% } %>
</ul>

---exportData
<div class="prompt-dialog-inner">
    <div class="descr">请复制以下文本作为模板, 在其他节点下按 `v` 以复用内容</div>
    <textarea class="value"><%- d %></textarea>
</div>

---importData
<div class="prompt-dialog-inner">
    <div class="descr">Please paste the data to the textarea and press 'OK'.
        <strong>Empty or invalid data will do nothing.</strong></div>
    <textarea class="value"></textarea>
</div>

---listItem
<li data-list-id="<%- d.item.id %>"
		data-list-fav="<%- d.item.fav_seq %>"
        data-list-title="<%- d.item.title %>">
    <a
       <% if (!d.forLink) { %> href="/app/list/<%- d.item.id %>" <% } %>
            ><%- d.item.title %>
        <% if (!d.forLink) { %><span class="aui-lozenge new-tab">在新窗口中打开</span><% } %>
    </a>
</li>

---listResult
<% _.each(d.list, function(item) {
	print(d.tpls.listItem({
        forLink: d.forLink,
        item: item
    }));
}) %>

---lightListItem

<div class="light-item" data-item-id="<%- d.id %>"><%- $.trim(d.content) ? d.content : 'Content empty' %></div>

---lightListActions

<div class="actions">
	<% d.forEach(function(action) { %>
		<a data-key="<%- action.key %>"><%- action.text %></a>
	<% }); %>
</div>

---lightListIndexedItem

<div class="light-indexed-item" data-item-id="<%- d.id %>">
	<span class="item-content"><%- $.trim(d.content) ? d.content : 'Content empty' %></span>
	<span class="list-title"><%- $.trim(d.list_title) %></span>
	<span class="main-window">在主窗口中打开</span>
</div>

---lightList

<% if (!d.blocks.length) { %>
	<div>
		<div class="light-item"><%- d.message %></div>
	</div>
<% } %>

<% _.each(d.blocks, function(block) { %>
	<div data-meta="<%- block.meta %>">
		<h4><%= block.title %>
			<% if (block.actions && block.actions.length) {
				print(d.tpls.lightListActions(block.actions));
			}%>
		</h4>
		<div class="item-block" data-can-click="<%- block.itemCanClick %>">
			<% _.each(block.items, function(item) {
				if (d.renderItem) {
					print(d.renderItem(item));
				}
				else {
					print(d.tpls.lightListItem(item));
				}
			}) %>
		</div>
	</div>
<% }) %>

---tag
<div id="tag-input">
    <div class="filter">
    </div>
    <div class="tags">
        <div class="group">
            <div class="aui-lozenge aui-lozenge-error aui-lozenge-subtle">t.todo</div>
        </div>
    </div>
</div>

---createList
<div id="create-list">
    <div class="title">Create list from the item</div>
    <input class="text" placeholder="Please enter the title for the new list"/>
    <div class="row align-right">
        <span class="move-children selected">
            <span class="fa fa-check-square-o" />
            <span class="fa fa-square-o" />
            Move the children items</span>
    </div>
</div>

---savedSearch
<div class="saved-search-title">
    <span><%= d.term %></span>
    <a class="sort">位置排序</a>
    <a class="remove">移除</a>
    <a class="top">置顶</a>
</div>
<div class="result-div"><ul></ul></div>

---h3
<h3><%= d %></h3>

---defTitle
<h4><% if (d.isSelf) { %><span class="self">[ Self ]</span><% } %><%= d.text %>
</h4>

---helpDialog
<ul>
    <% _.each(d.groups, function(group) { %>
    <h2><%= group.title %></h2>
        <ul>
        <% _.each(group.keys, function(k) {
            var key = d.keys[k];
            if (!key) { console.log('ERROR - key not found. ', k); return;}
            key.marked = 1;
            %>
            <li class="key-entry">
                <span class="keys">
                    <% _.each(key.keys, function(shortcut) { %>
                    <span class="aui-lozenge aui-lozenge-subtle"><%= shortcut %></span>
                    <% }); %>
                </span>
                <span class="key-descr"><%= key.descr %></span>
            </li>
        <% }); %>
        </ul>
    <% }); %>

    <% _.each(d.keys, function(k, id) {
        if (k.marked) {delete k.marked; return;}
        console.log('NOTICE - keys not in help dialog. ', id, k);
    }); %>
</ul>

---slidesWrapper

<link rel="stylesheet" href="/js/slides/css/reveal.css" type="text/css">
<link rel="stylesheet" href="/js/lib/highlight/styles/default.css" type="text/css">
<link rel="stylesheet" href="/js/slides/css/theme/beige.css" type="text/css">
<link rel="stylesheet" href="/js/slides/css/befe.css" type="text/css">
<div class="reveal">
	<div class="slides"></div>
</div>

---tutorial
<div class="tutorial">
    <div class="popup-box">
        <div class="popup-inner">
            <div class="image-wrapper">
                <img/>
            </div>

            <div class="text"></div>
        </div>
    </div>
    <div class="dialog-wrapper">
        <div class="dialog">
            <div class="dialog-content has-shadow">
                <div class="inner"></div>
            </div>
            <div class="dialog-avatar" style=""></div>
            <div class="dialog-avatar left-mode" style=""></div>
        </div>
    </div>
    <div id="audios">

    </div>
</div>

---tutorialAudio
<audio src="<%- d.src %>" loop></audio>
