---tutorialElements
<div class="tutorial-control has-shadow">
    <a class="collapse-expand">Collapse</a> |
    <a class="exit">Exit tutorial</a>
</div>

<div class="tutorial">
    <div class="popup-box">
        <div class="popup-inner">
            <img />
            <div class="text"></div>
        </div>

    </div>
    <div class="dialog-wrapper">
        <div class="quest-box has-shadow">
            <div class="quest-inner"></div>
        </div>
        <div class="dialog">
            <div class="dialog-avatar"
                 style="right: <%= d.right %>px; margin-top: <%= d.top %>px"
                    >
                <img src="/js/tutorial/avatars/<%= d.image %>.png"></div>
            <div class="dialog-content has-shadow"><div class="inner"></div></div>
        </div>
    </div>
</div>

---questBox
<% _.each(d.questGroups, function(group) { %>
<h4 data-title="<%= group.title %>"
    data-titlecn="<%= group.titlecn %>"></h4>
<ul>
    <% _.each(group.quests, function(quest) { %>
    <li data-id="<%= quest.id %>"
        class="<%= quest.status == 'building' ? 'building' : '' %>"
        >
        <span class="fa fa-check"></span>
        <span class="fa fa-caret-right"></span>
        <span data-title="<%= quest.title %>"
              data-titlecn="<%= quest.titlecn %>"></span>
    </li>
    <% }) %>
</ul>
<% }) %>