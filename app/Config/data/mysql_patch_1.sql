ALTER TABLE `advideo_tree_outliner`.`tr_item_lists`
    CHANGE `id` `id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `advideo_tree_outliner`.`tr_items`
    CHANGE `list_id` `list_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    CHANGE `task_points` `task_points` int(11) DEFAULT 0;
ALTER TABLE `advideo_tree_outliner`.`tr_list_shares`
    CHANGE `list_id` `list_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL;
ALTER TABLE `advideo_tree_outliner`.`tr_tags`
    CHANGE `list_id` `list_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL;
ALTER TABLE `advideo_tree_outliner`.`tr_users`
    CHANGE `user` `username` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL;
ALTER TABLE `advideo_tree_outliner`.`tr_viewed_lists`
    CHANGE `list_id` `list_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL;