<?php
$this->Html->less ( 'app.less', $appVersion array (
		'block' => 'css'
) );


?>
<div id="sidebar">
	<div id="sidebar-content">
		<div class="filter">
			<span class="aui-icon aui-icon-small aui-iconfont-search-small"></span>
			<input type="text" />
		</div>
		<div class="content-scrollable">
			<h3>Item parents</h3>
			<ul class="parents">
				<li><a>hello parenthello parenthello parenthello parent</a></li>
				<li><a>hello parent</a></li>
				<li><a>hello parent</a></li>
				<li><a>hello parent</a></li>
			</ul>
			<h3>Recent items</h3>
			<ul class="recent-items">
				<li><a>hello recent</a></li>
				<li><a>hello recent</a></li>
				<li><a>hello recent</a></li>
				<li><a>hello recent</a></li>
			</ul>
			<h3>Bookmarked items</h3>
			<ul class="bookmarks">
				<li><a>hello bookmark</a></li>
				<li><a>hello bookmark</a></li>
				<li><a>hello bookmark</a></li>
				<li><a>hello bookmark</a></li>
			</ul>
		</div>
	</div>
	<div id="splitter">
		<span class="fa fa-ellipsis-v fa-2"></span>
	</div>
</div>
<div id="content">

	<div class="header">
		<div class="primary">primary header</div>
		<div class="secondary">the content header secondary</div>
	</div>
	<div class="items">
		<ul>
			<li>
				<div class="item">hello main page item</div>
				<ul class="children">
					<li><div class="item">hello main page item</div></li>
					<li><div class="item">hello main page item</div></li>
					<li><div class="item">hello main page item</div></li>
					<li><div class="item">hello main page item</div></li>
					<li>
						<div class="item">
							<span class="status">
								<span class="aui-icon aui-icon-small aui-iconfont-expanded"></span>
								<span class="fa fa-square-o fa-3"></span>
							</span>
							<span class="body">hello main page item</span>
						</div>
						<ul class="children">
							<li><div class="item">
							<span class="status">
								<span class="aui-icon aui-icon-small aui-iconfont-collapsed"></span>
								<span class="fa fa-check-square-o fa-3"></span>
								<span class="aui-lozenge aui-lozenge-current">4/8 (12)</span>
							</span>
							<span class="body">hello main page item</span>
							</div></li>
							<li><div class="item">
							<span class="status">
								<span class="aui-icon aui-icon-small aui-iconfont-collapsed"></span>
								<span class="fa  fa-minus-square-o fa-3"></span>
								<span class="aui-lozenge aui-lozenge-success">12/12</span>
								<span class="aui-icon aui-icon-small aui-iconfont-link"></span>
								<span class="aui-label">people: nick</span>
							</span>
							<span class="body">hello main page item</span>
							</div></li>
							<li><div class="item">hello main page item</div></li>
							<li><div class="item">hello main page item</div></li>

						</ul>
					</li>
				</ul>
			</li>

			<li>
				<div class="item">hello main page item</div>
				<ul class="children">
					<li><div class="item">hello main page item</div></li>
					<li><div class="item">hello main page item</div></li>
					<li><div class="item">hello main page item</div></li>
					<li>
						<div class="item">hello main page item</div>
						<ul class="children">
							<li><div class="item">hello main page item</div></li>
							<li><div class="item">hello main page item</div></li>
							<li>
								<div class="item">hello main page item</div>
								<ul class="children">
									<li><div class="item">hello main page item</div></li>
									<li><div class="item">hello main page item</div></li>
									<li>
										<div class="item">hello main page item</div>
										<ul class="children">
											<li>
												<div class="item">hello main page item</div>
												<ul class="children">
													<li><div class="item">hello main page item</div></li>
													<li><div class="item">hello main page item</div></li>
													<li><div class="item">hello main page item</div></li>
													<li>
														<div class="item">hello main page item</div>
														<ul class="children">
															<li><div class="item">hello main page item</div></li>
															<li><div class="item">hello main page item</div></li>
															<li>
																<div class="item">hello main page item</div>
																<ul class="children">
																	<li><div class="item">hello main page item</div></li>
																	<li><div class="item">hello main page item</div></li>
																	<li>
																		<div class="item">hello main page item</div>
																		<ul class="children">
																			<li>
																				<div class="item">hello main page item</div>
																				<ul class="children">
																					<li><div class="item">hello main page item</div></li>
																					<li><div class="item">hello main page item</div></li>
																					<li><div class="item">hello main page item</div></li>
																					<li>
																						<div class="item">hello main page item</div>
																						<ul class="children">
																							<li><div class="item">hello main page item</div></li>
																							<li><div class="item">hello main page item</div></li>
																							<li>
																								<div class="item">hello main page item</div>
																								<ul class="children">
																									<li><div class="item">hello main page item</div></li>
																									<li><div class="item">hello main page item</div></li>
																									<li>
																										<div class="item">hello main page item</div>
																										<ul class="children">
																											<li><div class="item">hello main page item</div></li>
																											<li><div class="item">hello main page item</div></li>
																											<li>
																												<div class="item">hello main page item</div>
																												<ul class="children">
																													<li><div class="item">hello main page item</div></li>
																													<li><div class="item">hello main page item</div></li>
																													<li><div class="item">hello main page item</div></li>
																													<li>
																														<div class="item">hello main page item</div>
																														<ul class="children">
																															<li><div class="item">hello main page
																																	item</div></li>
																															<li><div class="item">hello main page
																																	item</div></li>
																															<li>
																																<div class="item">hello main page item</div>
																																<ul class="children">
																																	<li><div class="item">hello main page
																																			item</div></li>
																																	<li><div class="item">hello main page
																																			item</div></li>
																																	<li>
																																		<div class="item">hello main page item</div>
																																		<ul class="children">
																																			<li>
																																				<div class="item">hello main page
																																					item</div>
																																				<ul class="children">
																																					<li><div class="item">hello main
																																							page item</div></li>
																																					<li><div class="item">hello main
																																							page item</div></li>
																																					<li><div class="item">hello main
																																							page item</div></li>
																																					<li>
																																						<div class="item">hello main page
																																							item</div>
																																						<ul class="children">
																																							<li><div class="item">hello main
																																									page item</div></li>
																																							<li><div class="item">hello main
																																									page item</div></li>
																																							<li>
																																								<div class="item">hello main
																																									page item</div>
																																								<ul class="children">
																																									<li><div class="item">hello
																																											main page item</div></li>
																																									<li><div class="item">hello
																																											main page item</div></li>
																																									<li>
																																										<div class="item">hello main
																																											page item</div>
																																										<ul class="children">
																																											<li>
																																												<div class="item">hello main
																																													page item</div>
																																												<ul class="children">
																																													<li><div class="item">hello
																																															main page item</div></li>
																																													<li><div class="item">hello
																																															main page item</div></li>
																																													<li><div class="item">hello
																																															main page item</div></li>
																																													<li>
																																														<div class="item">hello
																																															main page item</div>
																																														<ul class="children">
																																															<li><div class="item">hello
																																																	main page item</div></li>
																																															<li><div class="item">hello
																																																	main page item</div></li>
																																															<li>
																																																<div class="item">hello
																																																	main page item</div>
																																																<ul class="children">
																																																	<li><div class="item">hello
																																																			main page item</div></li>
																																																	<li><div class="item">hello
																																																			main page item</div></li>
																																																	<li>
																																																		<div class="item">hello
																																																			main page item</div>
																																																		<ul class="children">
																																																			<li><div class="item">hello
																																																					main page item</div></li>
																																																			<li><div class="item">hello
																																																					main page item</div></li>
																																																		</ul>
																																																	</li>
																																																</ul>
																																															</li>
																																														</ul>
																																													</li>
																																													<li><div class="item">hello
																																															main page item</div></li>

																																												</ul>
																																											</li>
																																											<li><div class="item">hello
																																													main page item</div></li>
																																											<li><div class="item">hello main page item hello main page item hello main page item </div></li>
																																										</ul>
																																									</li>
																																								</ul>
																																							</li>
																																						</ul>
																																					</li>
																																					<li><div class="item">hello main
																																							page item</div></li>

																																				</ul>
																																			</li>
																																			<li><div class="item">hello main page
																																					item</div></li>
																																			<li><div class="item">hello main page
																																					item</div></li>
																																		</ul>
																																	</li>
																																</ul>
																															</li>
																														</ul>
																													</li>
																												</ul>
																											</li>
																										</ul>
																									</li>
																								</ul>
																							</li>
																							<li><div class="item">hello main page item</div></li>

																						</ul>
																					</li>
																					<li><div class="item">hello main page item</div></li>
																					<li><div class="item">hello main page item</div></li>
																				</ul>
																			</li>
																		</ul>
																	</li>
																</ul>
															</li>
															<li><div class="item">hello main page item</div></li>

														</ul>
													</li>
													<li><div class="item">hello main page item</div></li>
													<li><div class="item">hello main page item</div></li>
												</ul>
											</li>
										</ul>
									</li>
								</ul>
							</li>
							<li><div class="item">hello main page item</div></li>

						</ul>
					</li>
				</ul>

	</div>
	<div id="extra">
		<div id="clipboard" class="extra-box"></div>
		<div id="command-box" class="extra-box">hello command box</div>
	</div>
</div>


