<?php
$this->Html->less('review.less', $appVersion, array('block' => 'css'));


$this->Html->script ('pages/review.js?ver=' . $appVersion, array(
    'block' => 'script'
));
?>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<header>
    <div class="setting-toggle"><a class="fa fa-cog fa-4"></a></div>
    <div class="settings">
        <div class="row">
            <a class="button button-select selected interactive-mode">
                Interactive
            </a>
<!--            <a class="button more">-->
<!--                More-->
<!--            </a>-->
        </div>
<!--        <div class="search">-->
<!--            <input class="text" />-->
<!--            <a class="fa fa-search"></a>-->
<!--        </div>-->
<!--        <div class="search-result">-->
<!---->
<!--        </div>-->
    </div>
    <div class="more-settings">
        <ul>
            <li>
                <a class="button button-select">
                    Item 1
                </a>
            </li>
            <li>
                <a class="button button-select">
                    Item 2
                </a>
            </li>
        </ul>
    </div>
</header>
<section id="banner"><img src="/img/banners/mobile/<?php echo $bannerImage ?>"
        /><img class="placeholder" src="/img/banners/mobile/<?php echo $bannerImage ?>"
        /><span>Redefime.com</span
        ></section>
<section id="content">
    <div id="items"></div>
    <div id="item-spinner"></div>
    <div id="no-more">
        <p><span class="fa fa-thumbs-o-up"></span> Done!</p>
    </div>
</section>

<div id="hidden-zone" class="hidden">
    <div class="time-actions">
        <a class="button" data-time="4h">4h</a>
        <a class="button" data-time="8h">8h</a>
        <a class="button" data-time="1d">1d</a>
        <a class="button" data-time="2d">2d</a>
        <a class="button" data-time="4d">4d</a>
        <a class="button" data-time="1w">1w</a>
        <a class="button" data-time="2w">2w</a>
        <a class="button" data-time="1n">1n</a>
        <a class="button" data-time="2n">2n</a>
        <a class="button" data-time="4n">4n</a>
        <a class="button" data-time="8n">8n</a>
        <a class="button" data-time="1y">1y</a>
        <a class="button" data-time="2y">2y</a>
        <a class="button" data-time="4y">4y</a>
        <a class="button" data-time="8y">8y</a>
        <a class="button" data-time="16y">16y</a>
    </div>
</div>


<footer>
    <div class="expanded">
        <div class="row">
            <a class="button" data-time="1y">1y</a>
            <a class="button" data-time="2y">2y</a>
            <a class="button" data-time="4y">4y</a>
            <a class="button" data-time="8y">8y</a>
            <a class="button" data-time="16y">16y</a>
        </div>
        <div class="row">
            <a class="button" data-time="1w">1w</a>
            <a class="button" data-time="2w">2w</a>
            <a class="button" data-time="1n">1n</a>
            <a class="button" data-time="2n">2n</a>
            <a class="button" data-time="4n">4n</a>
            <a class="button" data-time="8n">8n</a>
        </div>
        <div class="row">
            <a class="button" data-time="4h">4h</a>
            <a class="button" data-time="8h">8h</a>
            <a class="button" data-time="1d">1d</a>
            <a class="button" data-time="2d">2d</a>
            <a class="button" data-time="4d">4d</a>
        </div>
    </div>
    <div class="main">
        <a class="button remove"><span class="fa fa-times"></span></a>
        <a class="button remove undo-remove"><span class="fa fa-reply"></span></a>
        <a class="button goto-list"><span class="fa fa-link"></span></a>
        <a class="button quick-review" data-time="2d"><span class="fa fa-flash"></span></a>
        <a class="button" data-time="4h">4h</a>
        <a class="button" data-time="8h">8h</a>
        <a class="button" data-time="1d">1d</a>
        <a class="button more"><span class="fa fa-bars"></span></a>
    </div>
</footer>
