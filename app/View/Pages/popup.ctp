<?php
$this->Html->script('pages/popup.js',
		array(
		'block' => 'script'
	));

$this->Html->less('popup.less', $appVersion, array('block' => 'css'));
?>
<div id="graph">
	<div class="items">
	</div>
	<div class="time-separator">
	</div>
</div>
<div class="progress">
    <div class="title">&nbsp;</div>
    <div class="text">&dash;&dash;</div>
    <div class="progress-bar task">
        <div class="complete" style="width: 100%"></div>
    </div>
    <div class="progress-bar unit-time">
        <div class="complete" style="width: 100%"></div>
    </div>
</div>
<div class="actions">
	<div class="row task-actions">
		<label>Task actions</label>
		<span class="links">
            <span class="aui-button aui-button-link finish-restart">Restart</span>
            <span class="aui-button aui-button-link finish" data-take-rest="30m">Finish</span>
<!--            going to remove the logic as well-->
<!--            <span class="aui-button aui-button-link cancel" data-take-rest="30m">Cancel</span>-->
<!--            <span class="aui-button aui-button-link cancel-restart">Cancel and restart</span>-->
		</span>
	</div>
	<div class="row quick-tasks">
		<label>Quick task</label>
		<span class="links">
			<span class="aui-button aui-button-link" data-take-rest="5m">5m</span>
			<span class="aui-button aui-button-link" data-take-rest="10m">10m</span>
			<span class="aui-button aui-button-link" data-take-rest="20m">20m</span>
			<span class="aui-button aui-button-link" data-take-rest="30m">30m</span>
			<span class="aui-button aui-button-link" data-take-rest="1h">1h</span>
			<span class="aui-button aui-button-link" data-take-rest="2h">2h</span>
			<span class="aui-button aui-button-link" data-take-rest="3h">3h</span>
			<span class="aui-button aui-button-link" data-take-rest="6h">6h</span>
		</span>
	</div>
	<div class="row extra">
		<label>Extra</label>
		<span class="links">
			<span class="aui-button aui-button-link goto-window">Goto window</span>
		</span>
	</div>
	<div class="row">
		<label>Graph</label>
		<span class="links">
			<span class="hour-graph"><input type="checkbox" /> Show hour graph</span>
		</span>
	</div>
</div>
<div class="progress progress-bottom">
    <div class="progress-bar task">
        <div class="complete" style="width: 100%"></div>
    </div>
</div>
