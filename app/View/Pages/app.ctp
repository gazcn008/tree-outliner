<?php
$this->Html->less ( 'app.less', $appVersion, array (
'block' => 'css'
) );

$this->Html->less ( 'tutorial.less', $appVersion, array (
'block' => 'css'
) );

$this->Html->script ('pages/app.js?ver=' . $appVersion, array(
'block' => 'script'
));

$loadingMaxIndex = 5;
$loadingImage = str_pad(rand(1, $loadingMaxIndex), 3, '0', STR_PAD_LEFT);

?>

<div id="sidebar" data-tab-selected="search">
	<div id="sidebar-content">
		<div class="content-scrollable">
			<div class="tabs-wrapper">
				<div class='tabs'>
					<div class="tab" data-tab="search">搜索</div>
					<div class="tab" data-tab="remind">提醒</div>
					<div class="tab" data-tab="update">最近更新</div>
					<div class="tab" data-tab="remove">最近删除</div>
				</div>
			</div>
			<div class="sidebar-content" data-tab-content="search">
				<h4>文档搜索
					<div class="actions">
						<a class="fav-doc">收藏本文档</a>
					</div>
				</h4>
				<div class="search-list text-box">
					<input type="text" placeholder='搜索 List &hellip; (快捷键: cmd + p 或 ctr + p)'/>

					<div class="list-result">
						<div class="list-item">
							<span>加载中...</span>
						</div>
					</div>
				</div>

				<h4>本文档内容搜索</h4>
				<div class="search-box text-box">
					<input type="text" placeholder='搜索 Items &hellip; (快捷键: "/")'/>
					<a class="save-search">保存</a>

					<div class="result-div">
						<ul></ul>
					</div>
				</div>

				<div class="saved-search-bottom-anchor"></div>
			</div>

			<div data-tab-content="remind">
				<div>
					<input type="text" class="search" placeholder="搜索提醒 &hellip;">
				</div>
				<div class="result-list">
					加载中...
				</div>
			</div>

			<div data-tab-content="remove">
				加载中...
			</div>

			<div data-tab-content="update">
				加载中...
			</div>
		</div>
	</div>
	<div id="splitter">
		<span class="fa fa-ellipsis-v fa-2"></span>
	</div>
</div>
<div id="content">

	<div class="header">
		<a class="primary" href="/app/list">
			树状文档 <sup>{ <span class="inner">BEFE</span> }</sup>
		</a>
		<span class="doc-toggler">
			<span class="descr">文档缩展</span>
			<span class="buttons">
				<span data-level="1">1
				</span><span data-level="2">2
				</span><span data-level="3">3
				</span><span data-level="4">4
				</span>
			</span>
			<span id="current-time">
				当前时间
				<span class="text"></span>
			</span>
		</span>
		<div class="secondary">
			<div class="share-type">
				<span class="share-type-label">文档信息加载中 &hellip;</span>
				<span class="fa fa-caret-down"></span>
				<div class="dropdown-container">
					<ul>
						<li class="selected private" data-share-type="private">
							<span class="title"><span class="fa fa-check"></span>私有文档</span>
							<span class="descr">只有创建者能访问/修改这个文档</span>
						</li>
						<li class="readonly" data-share-type="readonly">
							<span class="title"><span class="fa fa-check"></span>公开只读文档</span>
							<span class="descr">所有人都能访问这个文档 (只读)</span>
						</li>
						<li class="public" data-share-type="public">
							<span class="title"><span class="fa fa-check"></span>公开文档</span>
							<span class="descr">所有人都能访问/修改这个文档</span>
						</li>
					</ul>
				</div>
			</div>
			<a class="menu-button" id="sync">已同步</a> <a
				id="list-actions-trigger" aria-owns="list-actions"
				class="menu-button dropdown-trigger
				aui-dropdown2-trigger">操作菜单</a>
			<div id="list-actions" class="aui-dropdown2 aui-style-default">
				<div class="aui-dropdown2-section">

					<!--<li><a class="set-time-rate">Set time/complexity rate</a></li>-->

					<div class="aui-dropdown2-heading">
						<strong>基础</strong>
					</div>
					<ul>
						<li><a class="help-dialog">帮助: 键盘快捷键</a></li>
						<li><a class="all-list">文档列表</a></li>
						<li style="display: none;"><a class="remove-list">删除本文档</a></li>
						<li style="display: none;"><a class="notification-control" target="_blank"></a></li>
						<li><a class="toggle-learning-mode">关闭学习模式 (快捷键 ` )</a></li>
						<!--<li><a href="/review" target="_blank">复习收藏</a></li>-->
					</ul>

					<div class="aui-dropdown2-heading">
						<strong>子视图</strong>
					</div>
					<ul>
						<li><a class="split-doc">分割当前文档</a></li>
						<li><a class="open-sub-content">新窗口打开子文档</a></li>
						<li><a class="replace-sub-content">本窗口打开子文档</a></li>
					</ul>
					<div class="aui-dropdown2-heading">
						<strong>文档渲染</strong>
					</div>
					<ul>
						<li><a class="open-markdown" target="_blank">打开 Markdown</a></li>
						<li><a class="open-slides" target="_blank">播放演示</a></li>
						<li><a class="play-tutorial" target="_blank">播放互动教程 (实验)</a></li>
						<!--<li><a class="tutorials">Tutorials</a></li>-->
					</ul>
					</ul>
				</div>
			</div>
			<a class="menu-button" id="create">新建文档</a>
			<a class="menu-button" id="logout">注销登录</a>
		</div>
	</div>
	<h1 id="list-title">
		<span class="content"></span> <span
			class="fa fa-pencil title-edit-icon"></span>
	</h1>
	<div id="list-title-edit">
		<input class="text" type="text"/> <span class="actions"> <span
			class="save action-icon fa fa-check"></span> <span
			class="cancel action-icon fa fa-times"></span>
		</span>
	</div>
	<ul id="main-list" class="items">
	</ul>
	<div id="context-menu-wrapper">
		<span id="context-menu-trigger" class="aui-dropdown2-trigger-group">
			<span class="fa fa-pencil edit-item" title="修改 (快捷键: e)">修改</span>
			<span class="fa fa-check-square-o toggle-selection" title="选择 (快捷键: w)">选择</span>
			<span class="fa fa-times remove-item"
				  title="删除 (快捷键: `a g` 或 `fn + del` (Mac) 或 `delete` (Windows)">删除</span>
			<span class="fa fa-plus aui-dropdown2-trigger aui-dropdown2-trigger-arrowless"
				  aria-owns="context-menu-add" title="添加节点">添加</span>
			<span class="fa fa-bolt aui-dropdown2-trigger aui-dropdown2-trigger-arrowless"
				  aria-owns="context-menu-remind" title="提醒管理">提醒管理</span>
			<span class="fa fa-eye aui-dropdown2-trigger aui-dropdown2-trigger-arrowless"
				  aria-owns="context-menu-view" title="缩展功能">缩展</span>
			<span class="fa fa-arrows aui-dropdown2-trigger aui-dropdown2-trigger-arrowless"
				  aria-owns="context-menu-move" title="移动节点">移动</span>
			<span class="fa fa-paste aui-dropdown2-trigger aui-dropdown2-trigger-arrowless"
				  aria-owns="context-menu-template" title="模板功能"
			>模板</span>
			<span class="fa fa-link aui-dropdown2-trigger aui-dropdown2-trigger-arrowless"
				  aria-owns="context-menu-link" title="链接功能"
			>链接</span>
			<span class="fa fa-share share-item" title="分享 (快捷键: z)"
			>分享</span>
		</span>
		<div id="context-menu-add" class="aui-dropdown2 aui-style-default context-menu control-context-menu">
			<div class="aui-dropdown2-section">
				<div class="aui-dropdown2-heading">
					<strong>创建 Item 节点</strong>
				</div>
				<ul>
					<li><a title="Meta+D | Ctrl+D" data-action="meta+d">创建子节点</a></li>
					<li><a title="Enter" data-action="enter">同级下方创建</a></li>
					<li><a title="Shift+Enter" data-action="shift+enter">同级上方创建</a></li>
					<li><a title="Meta+S | Ctrl+S" data-action="meta+s">新建群组父节点</a></li>
				</ul>
			</div>
		</div>
		<div id="context-menu-remind" class="aui-dropdown2 aui-style-default context-menu control-context-menu">
			<div class="aui-dropdown2-section">
				<div class="aui-dropdown2-heading">
					<strong>提醒管理</strong>
				</div>
				<ul>
					<li><a title="Q" data-action="q">切换提醒状态</a></li>
					<li><a title="T" data-action="t">设置提醒时间</a></li>
				</ul>
			</div>
		</div>
		<div id="context-menu-view" class="aui-dropdown2 aui-style-default context-menu control-context-menu">
			<div class="aui-dropdown2-section">
				<div class="aui-dropdown2-heading">
					<strong>展开 / 缩放</strong>
				</div>
				<ul>
					<li><a title="F" data-action="f">当前节点</a></li>
					<li><a title="D" data-action="d">所有子节点</a></li>
					<li><a title="R" data-action="r">同级节点</a></li>
					<li><a title="S" data-action="s">父级节点</a></li>
					<li><a title="1" data-action="shift+meta+1">同级下展开 1 层</a></li>
					<li><a title="2" data-action="shift+meta+2">同级下展开 2 层</a></li>
					<li><a title="3" data-action="shift+meta+3">同级下展开 3 层</a></li>
					<li><a title="4" data-action="shift+meta+4">同级下展开 4 层</a></li>
				</ul>
			</div>
		</div>
		<div id="context-menu-move" class="aui-dropdown2 aui-style-default context-menu control-context-menu">
			<div class="aui-dropdown2-section">
				<div class="aui-dropdown2-heading">
					<strong>移动节点</strong>
				</div>
				<ul>
					<li><a title="Meta+I | Ctrl+I <br/> Meta+Up | Ctrl+Up" data-action="meta+i">向上移动</a></li>
					<li><a title="Meta+K | Ctrl+K <br/> Meta+Down | Ctrl+Down" data-action="meta+k">向下移动</a></li>
					<li><a title="Tab <br/> Meta+L | Ctrl+L" data-action="meta+l">缩进</a>
					</li>
					<li><a title="Shift+Tab <br/> Meta+J | Ctrl+J"
						   data-action="meta+j">反缩进</a></li>
				</ul>
			</div>
		</div>
		<div id="context-menu-template" class="aui-dropdown2 aui-style-default context-menu control-context-menu">
			<div class="aui-dropdown2-section">
				<div class="aui-dropdown2-heading">
					<strong>模板功能</strong>
				</div>
				<ul>
					<li><a title="C" data-action="c">复制节点内容</a></li>
					<li><a title="V" data-action="v">粘贴节点内容</a></li>
					<li><a title="A C" data-action="a c">复制选择节点</a></li>
					<li><a title="M | C M" data-action="m">移动选择节点</a></li>
				</ul>
			</div>
		</div>
		<div id="context-menu-link" class="aui-dropdown2 aui-style-default context-menu control-context-menu">
			<div class="aui-dropdown2-section">
				<div class="aui-dropdown2-heading">
					<strong>链接功能</strong>
				</div>
				<ul>
					<li><a title="A R" data-action="a r">复制节点链接</a></li>
					<li><a title="A E" data-action="a e">打开列表链接对话框</a></li>
					<li><a title="A D" data-action="a d">当前节点抽取为独立文档</a></li>
				</ul>
			</div>
		</div>
	</div>

	<!--<div id="help-box-wrapper">-->
	<!--<div id="help-box">-->
	<!--<div class="search-box text-box">-->
	<!--<span class="fa fa-bolt"></span> <input class="text" type="text"-->
	<!--placeholder='How to use (shortcut "/")'>-->
	<!--</div>-->
	<!--<div class="help-content"></div>-->
	<!--</div>-->
	<!--</div>-->

	<div id="hidden-zone">
		<div id="tag-input">
			<div class="filter">
				<input type="text" placeholder="Type to filter"/>
			</div>
			<div class="tags">
				<div class="tag-defs"></div>
				<span class="remove-tag" data-shortcut="c"><span class="shortcut">;c</span>删除标签</span>
				<!--                __m24: replace the tag-->
				<span class="replace-tag" data-shortcut="v"><span class="shortcut">;v</span>重命名标签</span>
			</div>
		</div>
	</div>

	<div id="screen-block">
		<div class="mask"></div>
		<div class="loading">
			<img src="/img/loading-gifs/<?php echo $loadingImage; ?>.gif" alt="">
			<div class="message">
				<span class="quote">“</span>
				<span class="message-content">
					<em>你的文档加载中 ...</em>
					<br>
					<em>不, 你的文档加载中</em>
				</span>
				<span class="quote">”</span>
			</div>
		</div>
	</div>

	<div id="close-sub-content">
		<i class="fa fa-times"></i>
	</div>
</div>

<iframe id="sub-content"></iframe>

<div id="markdown">
	<div class="raw">
		<textarea readonly></textarea>
	</div>
	<div class="render"></div>

	<div class="page-actions">
		<div class="switch-view action refresh" title="显示原始 markdown">
			<i class="fa fa-refresh"></i>
		</div>
		<div class="switch-view action toggle-raw" title="显示原始 markdown">
			<i class="fa fa-file-text-o"></i>
		</div>
		<a class="open-list action open" title="打开文档">
			<i class="fa fa-external-link"></i>
		</a>
	</div>
</div>

<div id="open-list">
	<div class="page-actions">
		<a class="open-list action open" title="打开文档">
			<i class="fa fa-external-link"></i>
		</a>
	</div>
</div>

<div id="file-dropping-zone">
	<div class="inner">
		<span>拖拽文件并释放上传</span>
	</div>
</div>

<div id="bookmark-scheduler" style="display: none">
	<div class="item-content">这是一个 item</div>
	<h4 class="next-review">
		<span class="label">回顾时间: </span>
		<span class="time">2017-06-18 19:00</span>
	</h4>
	<div class="is-random">
		<div class="selected" data-type="exact">精确设置</div>
		<div data-type="random" title="从现在到目标时间的后 1/3 时间中, 随机出一个时间">随机设置</div>
	</div>
	<h4>距离下次回顾 (时间间隔)</h4>
	<div class="intervals">
		<div class="interval-group">
			<div data-interval="10m">10m</div>
			<div data-interval="20m">20m</div>
			<div data-interval="30m">30m</div>
			<div data-interval="40m">40m</div>
			<div data-interval="50m">50m</div>
		</div>

		<div class="interval-group">
			<div data-interval="1h">1h</div>
			<div data-interval="1.5h">1.5h</div>
			<div data-interval="2h">2h</div>
		</div>

		<div class="interval-group">
			<div data-interval="3h">3h</div>
			<div data-interval="4h">4h</div>
			<div data-interval="8h">8h</div>
		</div>

		<div class="interval-group">
			<div data-interval="1d">1d</div>
			<div data-interval="2d">2d</div>
			<div data-interval="4d">4d</div>
			<div data-interval="1w">1w</div>
			<div data-interval="2w">2w</div>
		</div>

		<div class="interval-group">
			<div data-interval="1n">1n</div>
			<div data-interval="2n">2n</div>
			<div data-interval="4n">4n</div>
			<div data-interval="8n">8n</div>
		</div>

		<div class="interval-group">
			<div data-interval="1y">1y</div>
			<div data-interval="2y">2y</div>
			<div data-interval="4y">4y</div>
			<div data-interval="8y">8y</div>
		</div>
	</div>
	<h4>日期</h4>
	<div class="calendar">

	</div>

	<div class="times">
		<div class="times-content">
			<h4>小时</h4>
			<div class="hours">
				<div data-hour="00">00</div>
				<div data-hour="01">01</div>
				<div data-hour="02">02</div>
				<div data-hour="03">03</div>
				<div data-hour="04">04</div>
				<div data-hour="05">05</div>
				<div data-hour="06">06</div>
				<div data-hour="07">07</div>
				<div data-hour="08">08</div>
				<div data-hour="09">09</div>
				<div data-hour="10">10</div>
				<div data-hour="11">11</div>
				<div data-hour="12">12</div>
				<div data-hour="13">13</div>
				<div data-hour="14">14</div>
				<div data-hour="15">15</div>
				<div data-hour="16">16</div>
				<div data-hour="17">17</div>
				<div data-hour="18">18</div>
				<div data-hour="19">19</div>
				<div data-hour="20">20</div>
				<div data-hour="21">21</div>
				<div data-hour="22">22</div>
				<div data-hour="23">23</div>
			</div>
			<h4>分钟</h4>
			<div class="minutes">
				<div data-minute="00">00</div>
				<div data-minute="10">10</div>
				<div data-minute="20">20</div>
				<div data-minute="30">30</div>
				<div data-minute="40">40</div>
				<div data-minute="50">50</div>
			</div>
		</div>
	</div>
</div>


