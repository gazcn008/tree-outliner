<?php
$this->Html->script('pages/user.js',
array(
'block' => 'script'
));

$this->Html->less ( 'login.less', $appVersion, array (
'block' => 'css'
) );
?>
<header id="header" role="banner">
	<nav class="aui-header aui-dropdown2-trigger-group" role="navigation">
		<div class="aui-header-inner">
			<div class="aui-header-primary">
				<h1 id="logo" class="aui-header-logo">
					<a class="" href="/"> <span
							class="fa fa-th-list outliner-logo"></span><span>BEFE 树状文档</span>
					</a>
				</h1>
			</div>
		</div>
	</nav>
</header>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<section id="content" role="main">
	<div class="aui-page-panel">
		<div class="aui-page-panel-inner">
			<section class="aui-page-panel-content">
				<h2>登陆</h2>

				<form method="post" class="aui top-label">
					<div id="errors"></div>
					<div class="field-group">
						<label>用户</label> <input id="user" type="text" name="user"
												 class="text long-field"/>
					</div>
					<div class="field-group">
						<label>密码</label> <input id="pass" type="password" name="pass"
												 class="text long-field"/>
					</div>
					<!--<div class="checkbox">-->
					<!--<input class="checkbox" type="checkbox" name="cbThree"-->
					<!--id="cbThree"> <label for="cbThree">Remember me on this computer</label>-->
					<!--</div>-->
					<div class="buttons">
						<input class="aui-button aui-button-primary" type="submit" name="submit" value="登陆"/>
						<a class="aui-button aui-button-link" href="/register">注册一个新账号</a>
					</div>
				</form>
			</section>
		</div>
	</div>
	<div id="login-message">
		<span class="quote">“</span>
		<span class="message-content">
				<em>本来是不应该有这么显眼的的说明文字的</em>
				<br>
				<em>但是, 不说大伙又有很多困惑</em>
				<br>
				<em>所以, 加粗加大, 特此给大伙一个指引</em>
				<br>
				<br>
				<em>首先, 本系统还没跟 UUAP 打通</em>
				<br>
				<em>请申请一个新账号</em>
				<br>
				<br>
				<em>其次, 请把用户和密码保存到浏览器里,</em>
				<br>
				<em>这样, 每次就都不用输密码啦 !</em>
			</span>
		<span class="quote">”</span>
	</div>
</section>
<!--<footer id="footer" role="contentinfo">-->
<!--<section class="footer-body">-->
<!--<div id="footer-logo">-->
<!--<a href="http://www.atlassian.com/" target="_blank">Atlassian</a>-->
<!--</div>-->
<!--</section>-->
<!--</footer>-->
