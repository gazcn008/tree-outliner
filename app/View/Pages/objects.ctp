<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>

    <title>
        <?php echo $title_for_layout; ?>
    </title>
    <link href="/css/font-awesome-4.1.0/css/font-awesome.css" rel="stylesheet">
    <script src="/js/require.js" type="text/javascript"></script>

    <script>
        require.config({
            urlArgs : "bust=v" + 15
            /** <DEBUG do not touch this kind of comments* */
                + (new Date()).getTime()
            /** DEBUG>* */
        });
    </script>
    <?php
        echo $this->Html->css('/js/lib/jquery-ui-1.11.2.custom/jquery-ui.css');
        echo $this->Html->less ('objects.less', $appVersion);
        echo $this->Html->script ('pages/objects.js?ver=' . $appVersion);
    ?>
</head>
<body>
<div id="page">
</div>
<script type="text/javascript">
    var pv = <?php if (isset($pv)) { echo json_encode($pv); } else { echo '{}'; }?>;
</script>
</body>
</html>
